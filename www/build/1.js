webpackJsonp([1],{

/***/ 670:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltermodalPageModule", function() { return FiltermodalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__filtermodal__ = __webpack_require__(674);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FiltermodalPageModule = (function () {
    function FiltermodalPageModule() {
    }
    FiltermodalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__filtermodal__["a" /* FiltermodalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__filtermodal__["a" /* FiltermodalPage */]),
            ],
        })
    ], FiltermodalPageModule);
    return FiltermodalPageModule;
}());

//# sourceMappingURL=filtermodal.module.js.map

/***/ }),

/***/ 674:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiltermodalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FiltermodalPage = (function () {
    function FiltermodalPage(navCtrl, navParams, viewCtrl, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.global = global;
    }
    FiltermodalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    FiltermodalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-filtermodal',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\filtermodal\filtermodal.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons start>\n      <button ion-button clear icon-only>\n          <ion-icon name="funnel"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>מסנן</ion-title>\n    <ion-buttons end>\n        <button ion-button icon-only (click)="dismiss()">\n            <ion-icon name="close"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content  padding>\n    <ion-item-group  *ngFor="let item of global.filter">\n      <ion-item-divider no-lines no-padding>{{item.category}}</ion-item-divider>\n      <ion-item *ngFor="let object of item.filter" no-lines>\n<ion-label stacked>{{object.title}}</ion-label>\n          <ion-checkbox (ionChange)="global.filterData(object)" [(ngModel)]="object.check" color="Darkgraycolor"></ion-checkbox>\n      </ion-item>\n    </ion-item-group>\n    <!--<button (click)="dismiss()" color="light" float-right ion-button clear>\n       Done\n    </button>-->\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\filtermodal\filtermodal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global__["a" /* global */]])
    ], FiltermodalPage);
    return FiltermodalPage;
}());

//# sourceMappingURL=filtermodal.js.map

/***/ })

});
//# sourceMappingURL=1.js.map