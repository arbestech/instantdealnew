webpackJsonp([5],{

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogoutProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LogoutProvider = (function () {
    function LogoutProvider(app, loadingProvider, dataProvider) {
        this.app = app;
        this.loadingProvider = loadingProvider;
        this.dataProvider = dataProvider;
        console.log("Initializing Logout Provider");
    }
    // Hooks the app to this provider, this is needed to clear the navigation views when logging out.
    LogoutProvider.prototype.setApp = function (app) {
        this.app = app;
    };
    // Logs the user out on Firebase, and clear navigation stacks.
    // It's important to call setApp(app) on the constructor of the controller that calls this function.
    LogoutProvider.prototype.logout = function () {
        var _this = this;
        this.loadingProvider.show();
        // Sign the user out on Firebase
        __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().signOut().then(function (success) {
            // Clear navigation stacks
            _this.app.getRootNav().popToRoot().then(function () {
                _this.loadingProvider.hide();
                _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
                // Restart the entire app
                // let page;
                // if(window.location.protocol != "file:"){
                //   page = window.location.protocol+"//"+window.location.hostname+":"+window.location.port;
                // } else{
                //   page = 'index.html';
                // }
                // console.log(page);
                // document.location.href = page;
            });
        });
    };
    LogoutProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_2__loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_3__data__["a" /* DataProvider */]])
    ], LogoutProvider);
    return LogoutProvider;
}());

//# sourceMappingURL=logout.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_login__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__validator__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__settings__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = (function () {
    function LoginPage(navCtrl, loginProvider, formBuilder, modalCtrl) {
        this.navCtrl = navCtrl;
        this.loginProvider = loginProvider;
        this.formBuilder = formBuilder;
        this.modalCtrl = modalCtrl;
        // It's important to hook the navController to our loginProvider.
        this.loginProvider.setNavController(this.navCtrl);
        // Create our forms and their validators based on validators set on validator.ts.
        this.emailPasswordForm = formBuilder.group({
            email: __WEBPACK_IMPORTED_MODULE_4__validator__["a" /* Validator */].emailValidator,
            password: __WEBPACK_IMPORTED_MODULE_4__validator__["a" /* Validator */].passwordValidator
        });
        this.emailForm = formBuilder.group({
            email: __WEBPACK_IMPORTED_MODULE_4__validator__["a" /* Validator */].emailValidator
        });
        this.facebookLoginEnabled = __WEBPACK_IMPORTED_MODULE_5__settings__["a" /* Settings */].facebookLoginEnabled;
        this.googleLoginEnabled = __WEBPACK_IMPORTED_MODULE_5__settings__["a" /* Settings */].googleLoginEnabled;
        this.phoneLoginEnabled = __WEBPACK_IMPORTED_MODULE_5__settings__["a" /* Settings */].phoneLoginEnabled;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        // Set view mode to main.
        this.mode = 'main';
    };
    // Call loginProvider and login the user with email and password.
    // You may be wondering where the login function for Facebook and Google are.
    // They are called directly from the html markup via loginProvider.facebookLogin() and loginProvider.googleLogin().
    LoginPage.prototype.login = function () {
        this.loginProvider.emailLogin(this.emailPasswordForm.value["email"], this.emailPasswordForm.value["password"]);
    };
    // Call loginProvider and send a password reset email.
    LoginPage.prototype.forgotPassword = function () {
        this.loginProvider.sendPasswordReset(this.emailForm.value["email"]);
        this.clearForms();
    };
    // Clear the forms.
    LoginPage.prototype.clearForms = function () {
        this.emailPasswordForm.reset();
        this.emailForm.reset();
    };
    LoginPage.prototype.registerModel = function () {
        this.modalCtrl.create("RegisterPage").present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\login\login.html"*/'<!-- <ion-content class="no-scroll">\n  <div padding>\n    <div style="padding-top:10%">\n  -->\n\n<!-- <div class="home_banner" padding-bottom>\n  <img src="assets/img/InstantDealLogo1.png">\n</div>\n      </div>\n      <ion-list no-lines>\n        <form [formGroup]="emailPasswordForm">\n          <ion-item>\n            <ion-input type="text" formControlName="email" placeholder="אימייל"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-input type="password" formControlName="password" placeholder="סיסמא"></ion-input>\n            <br>\n\n          </ion-item>\n<button item-right ion-button clear color="gray" (click)="forgotPassword()">שכחתי סיסמא ?</button>\n          <div style="padding: 10px;">\n            <button ion-button color="dark" block tappable (click)="login()" [disabled]="!emailPasswordForm.valid">התחבר</button>\n          </div>\n        </form>\n      </ion-list>\n    </div>\n    <div style="text-align:center; margin-top: 2rem;">\n      <p style="font-weight: 500; color:#444">התחבר באמצעות</p>\n      <button *ngIf="facebookLoginEnabled==true" ion-button color="facebook" tappable (click)="loginProvider.facebookLogin()">\n        <ion-icon name="logo-facebook"></ion-icon>\n      </button>\n      <button *ngIf="googleLoginEnabled==true" ion-button color="google" tappable (click)="loginProvider.googleLogin()">\n        <ion-icon name="logo-google"></ion-icon>\n      </button>\n      <button ion-button color="dark" tappable (click)="registerModel()">\n        <ion-icon name="ios-mail-outline"></ion-icon>\n      </button>\n      <button *ngIf="phoneLoginEnabled==true" ion-button color="dark" tappable (click)="loginProvider.phoneLogin()">\n        <ion-icon name="phone-portrait"></ion-icon>\n      </button>\n    </div> -->\n\n<ion-content class="background">\n<br><br><br><br>\n<br><br>\n\n  <ion-card>\n    <ion-card-header>\n<button ion-button block full disabled=true color=primary >התחברות</button>\n    </ion-card-header>\n    <ion-card-content>\n<form [formGroup]="emailPasswordForm">\n      <ion-list no-line>\n        <ion-item>\n<ion-icon name="person" color="primary" item-end></ion-icon>\n<ion-input type="text" formControlName="email" placeholder=" אימייל">\n\n</ion-input>\n        </ion-item>\n        <ion-item>\n<ion-icon name="lock" color="primary" item-end></ion-icon>\n<ion-input type="password" formControlName="password" placeholder=" סיסמא">\n\n</ion-input>\n        </ion-item>\n        <a>שכחת סיסמא? <b>קבל עזרה לשחזר סיסמא</b></a>\n<button ion-button block outline color="light" (click)="login()" [disabled]="!emailPasswordForm.valid">כניסה</button>\n        <p>או</p>\n<button ion-button block color="Google+" icon-start *ngIf="facebookLoginEnabled==true" ion-button tappable\n  (click)="loginProvider.facebookLogin()">\nהתחבר באמצעות Facebook<ion-icon name="logo-facebook"></ion-icon>\n        </button>\n<button ion-button block color="Google+" icon-start *ngIf="googleLoginEnabled==true" ion-button tappable (click)="loginProvider.googleLogin()">\nהתחבר באמצעות Google<ion-icon name="logo-google"></ion-icon>\n</button>\n<button ion-button block color="Google+" icon-start *ngIf="phoneLoginEnabled==true" ion-button tappable (click)="loginProvider.phoneLogin()">\nהתחבר באמצעות SMS <ion-icon name="text"></ion-icon>\n</button>\n<button ion-button clear full color="light" (click)="registerModel()">אין לך עדין חשבון ? הירשם</button>\n      </ion-list>\n      </form>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_login__["a" /* LoginProvider */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirebaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alert__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take__ = __webpack_require__(637);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { Observable } from 'rxjs/Rx';

var FirebaseProvider = (function () {
    // Firebase Provider
    // This is the provider class for most of the Firebase updates in the app.
    function FirebaseProvider(angularfire, loadingProvider, alertProvider, dataProvider) {
        this.angularfire = angularfire;
        this.loadingProvider = loadingProvider;
        this.alertProvider = alertProvider;
        this.dataProvider = dataProvider;
        console.log("Initializing Firebase Provider");
    }
    // Send friend request to userId.
    FirebaseProvider.prototype.sendFriendRequest = function (userId) {
        var _this = this;
        var loggedInUserId = __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid;
        this.loadingProvider.show();
        var requestsSent;
        // Use take(1) so that subscription will only trigger once.
        this.dataProvider.getRequests(loggedInUserId).snapshotChanges().take(1).subscribe(function (requests) {
            console.log(requests.payload.val());
            if (requests.payload.val() != null && requests.payload.val().requestsSent != null)
                requestsSent = requests.payload.val().requestsSent;
            if (requestsSent == null || requestsSent == undefined) {
                requestsSent = [userId];
            }
            else {
                if (requestsSent.indexOf(userId) == -1)
                    requestsSent.push(userId);
            }
            // Add requestsSent information.
            _this.angularfire.object('/requests/' + loggedInUserId).update({
                requestsSent: requestsSent
            }).then(function (success) {
                var friendRequests;
                _this.dataProvider.getRequests(userId).snapshotChanges().take(1).subscribe(function (requests) {
                    if (requests.payload.val() != null && requests.payload.val().friendRequests != null)
                        friendRequests = requests.payload.val().friendRequests;
                    if (friendRequests == null) {
                        friendRequests = [loggedInUserId];
                    }
                    else {
                        if (friendRequests.indexOf(userId) == -1)
                            friendRequests.push(loggedInUserId);
                    }
                    // Add friendRequest information.
                    _this.angularfire.object('/requests/' + userId).update({
                        friendRequests: friendRequests
                    }).then(function (success) {
                        _this.loadingProvider.hide();
                        _this.alertProvider.showFriendRequestSent();
                    }).catch(function (error) {
                        _this.loadingProvider.hide();
                    });
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    // Cancel friend request sent to userId.
    FirebaseProvider.prototype.cancelFriendRequest = function (userId) {
        var _this = this;
        var loggedInUserId = __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid;
        this.loadingProvider.show();
        var requestsSent;
        this.dataProvider.getRequests(loggedInUserId).snapshotChanges().take(1).subscribe(function (requests) {
            requestsSent = requests.payload.val().requestsSent;
            requestsSent.splice(requestsSent.indexOf(userId), 1);
            // Update requestSent information.
            _this.angularfire.object('/requests/' + loggedInUserId).update({
                requestsSent: requestsSent
            }).then(function (success) {
                var friendRequests;
                _this.dataProvider.getRequests(userId).snapshotChanges().take(1).subscribe(function (requests) {
                    friendRequests = requests.payload.val().friendRequests;
                    console.log(friendRequests);
                    friendRequests.splice(friendRequests.indexOf(loggedInUserId), 1);
                    // Update friendRequests information.
                    _this.angularfire.object('/requests/' + userId).update({
                        friendRequests: friendRequests
                    }).then(function (success) {
                        _this.loadingProvider.hide();
                        _this.alertProvider.showFriendRequestRemoved();
                    }).catch(function (error) {
                        _this.loadingProvider.hide();
                    });
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    // Delete friend request.
    FirebaseProvider.prototype.deleteFriendRequest = function (userId) {
        var _this = this;
        var loggedInUserId = __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid;
        this.loadingProvider.show();
        var friendRequests;
        this.dataProvider.getRequests(loggedInUserId).snapshotChanges().take(1).subscribe(function (requests) {
            friendRequests = requests.payload.val().friendRequests;
            console.log(friendRequests);
            friendRequests.splice(friendRequests.indexOf(userId), 1);
            // Update friendRequests information.
            _this.angularfire.object('/requests/' + loggedInUserId).update({
                friendRequests: friendRequests
            }).then(function (success) {
                var requestsSent;
                _this.dataProvider.getRequests(userId).snapshotChanges().take(1).subscribe(function (requests) {
                    requestsSent = requests.payload.val().requestsSent;
                    requestsSent.splice(requestsSent.indexOf(loggedInUserId), 1);
                    // Update requestsSent information.
                    _this.angularfire.object('/requests/' + userId).update({
                        requestsSent: requestsSent
                    }).then(function (success) {
                        _this.loadingProvider.hide();
                    }).catch(function (error) {
                        _this.loadingProvider.hide();
                    });
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
                //TODO ERROR
            });
        });
    };
    // Accept friend request.
    FirebaseProvider.prototype.acceptFriendRequest = function (userId) {
        var _this = this;
        var loggedInUserId = __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid;
        // Delete friend request.
        this.deleteFriendRequest(userId);
        this.loadingProvider.show();
        this.dataProvider.getUser(loggedInUserId).snapshotChanges().take(1).subscribe(function (account) {
            var friends = account.payload.val().friends;
            if (!friends) {
                friends = [userId];
            }
            else {
                friends.push(userId);
            }
            // Add both users as friends.
            _this.dataProvider.getUser(loggedInUserId).update({
                friends: friends
            }).then(function (success) {
                _this.dataProvider.getUser(userId).snapshotChanges().take(1).subscribe(function (account) {
                    var friends = account.payload.val().friends;
                    if (!friends) {
                        friends = [loggedInUserId];
                    }
                    else {
                        friends.push(loggedInUserId);
                    }
                    _this.dataProvider.getUser(userId).update({
                        friends: friends
                    }).then(function (success) {
                        _this.loadingProvider.hide();
                    }).catch(function (error) {
                        _this.loadingProvider.hide();
                    });
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    FirebaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_2__loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_3__alert__["a" /* AlertProvider */], __WEBPACK_IMPORTED_MODULE_4__data__["a" /* DataProvider */]])
    ], FirebaseProvider);
    return FirebaseProvider;
}());

//# sourceMappingURL=firebase.js.map

/***/ }),

/***/ 16:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoadingProvider = (function () {
    function LoadingProvider(loadingController) {
        this.loadingController = loadingController;
        this.spinner = { spinner: 'circles' };
        console.log("Initializing Loading Provider");
    }
    //Show loading
    LoadingProvider.prototype.show = function () {
        if (!this.loading) {
            this.loading = this.loadingController.create(this.spinner);
            this.loading.present();
        }
    };
    //Hide loading
    LoadingProvider.prototype.hide = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    LoadingProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], LoadingProvider);
    return LoadingProvider;
}());

//# sourceMappingURL=loading.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_logout__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_alert__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_image__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__validator__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_camera__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_firebase__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_http__ = __webpack_require__(143);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var HomePage = (function () {
    // HomePage
    // This is the page where the user is directed after successful login and email is confirmed.
    // A couple of profile management function is available for the user in this page such as:
    // Change name, profile pic, email, and password
    // The user can also opt for the deletion of their account, and finally logout.
    function HomePage(navCtrl, alertCtrl, navParams, app, logoutProvider, loadingProvider, imageProvider, angularfire, alertProvider, dataProvider, camera, platform, fcm, toast, modal, http) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.app = app;
        this.logoutProvider = logoutProvider;
        this.loadingProvider = loadingProvider;
        this.imageProvider = imageProvider;
        this.angularfire = angularfire;
        this.alertProvider = alertProvider;
        this.dataProvider = dataProvider;
        this.camera = camera;
        this.platform = platform;
        this.fcm = fcm;
        this.toast = toast;
        this.modal = modal;
        this.http = http;
        this.user = {
            gender: 'male',
            dob: new Date(),
            location: '',
            publicVisibility: true,
        };
        this.showOnline = false;
        this.isPushEnabled = false;
        this.isBrowser = false;
        this.places = [];
        this.logoutProvider.setApp(this.app);
        if (this.platform.is('core'))
            this.isBrowser = true;
        if (localStorage.getItem('isPushEnabled') == 'true')
            this.isPushEnabled = true;
        else
            this.isPushEnabled = false;
        if (localStorage.getItem('showOnline') == 'true')
            this.showOnline = true;
        else
            this.showOnline = false;
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // Observe the userData on database to be used by our markup html.
        // Whenever the userData on the database is updated, it will automatically reflect on our user variable.
        this.loadingProvider.show();
        this.dataProvider.getCurrentUser().snapshotChanges().subscribe(function (user) {
            _this.loadingProvider.hide();
            _this.user = user.payload.val();
            console.log(_this.user);
        });
    };
    HomePage.prototype.search = function () {
        var _this = this;
        console.log(this.user.location);
        var textbox = document.getElementById('txtHome').getElementsByTagName('input')[0];
        var autocomplete = new google.maps.places.Autocomplete(textbox, { types: ['geocode'] });
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            // retrieve the place object for your use
            var place = autocomplete.getPlace();
            _this.user.location = place.formatted_address;
            console.log(_this.user.location);
        });
    };
    HomePage.prototype.changeStatus = function () {
        console.log(this.showOnline);
        localStorage.setItem('showOnline', this.showOnline);
        this.angularfire.object('accounts/' + this.user.userId).update({
            online: this.showOnline
        });
    };
    HomePage.prototype.changeVisibility = function () {
        this.angularfire.object('accounts/' + this.user.userId).update({
            publicVisibility: this.user.publicVisibility
        });
    };
    HomePage.prototype.save = function () {
        var _this = this;
        var dob = (this.user.dob).split("-");
        console.log(dob);
        var currentYear = new Date().getFullYear();
        this.user.age = parseInt(currentYear) - dob[0];
        this.angularfire.object('accounts/' + this.user.userId).update(this.user).then(function () { return _this.toast.create({ message: 'Updated Successfully', duration: 2000 }).present(); });
    };
    HomePage.prototype.showBlockedList = function () {
        this.modal.create("BlockedlistPage").present();
    };
    HomePage.prototype.changeNotification = function () {
        var _this = this;
        console.log(this.isPushEnabled);
        if (this.isPushEnabled == true) {
            //Registering for push notification
            this.fcm.hasPermission().then(function (data) {
                if (data.isEnabled != true) {
                    _this.fcm.grantPermission().then(function (data) {
                        console.log(data);
                    });
                }
                else {
                    _this.fcm.getToken().then(function (token) {
                        console.log(token);
                        _this.angularfire.object('/accounts/' + __WEBPACK_IMPORTED_MODULE_9_firebase__["auth"]().currentUser.uid).update({ pushToken: token });
                        localStorage.setItem('isPushEnabled', 'true');
                        _this.isPushEnabled = true;
                    }).catch(function (err) {
                        console.log(err);
                    });
                    _this.fcm.onTokenRefresh().subscribe(function (token) {
                        console.log(token);
                        _this.angularfire.object('/accounts/' + __WEBPACK_IMPORTED_MODULE_9_firebase__["auth"]().currentUser.uid).update({ pushToken: token });
                    });
                }
            });
            this.fcm.onNotificationOpen().subscribe(function (data) {
                console.log(data);
            });
        }
        else {
            this.isPushEnabled == false;
            this.angularfire.object('/accounts/' + __WEBPACK_IMPORTED_MODULE_9_firebase__["auth"]().currentUser.uid).update({ pushToken: '' });
            localStorage.setItem('isPushEnabled', 'false');
        }
    };
    // Change user's profile photo. Uses imageProvider to process image and upload on Firebase and update userData.
    HomePage.prototype.setPhoto = function () {
        var _this = this;
        // Ask if the user wants to take a photo or choose from photo gallery.
        this.alert = this.alertCtrl.create({
            title: 'Set Profile Photo',
            message: 'Do you want to take a photo or choose from your photo gallery?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Choose from Gallery',
                    handler: function () {
                        // Call imageProvider to process, upload, and update user photo.
                        _this.imageProvider.setProfilePhoto(_this.user, _this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Take Photo',
                    handler: function () {
                        // Call imageProvider to process, upload, and update user photo.
                        _this.imageProvider.setProfilePhoto(_this.user, _this.camera.PictureSourceType.CAMERA);
                    }
                }
            ]
        }).present();
    };
    // Change user's password, this option only shows up for users registered via Firebase.
    // The currentPassword is first checked, after which the new password should be entered twice.
    // Uses password validator from Validator.ts.
    HomePage.prototype.setPassword = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Change Password',
            message: "Please enter a new password.",
            inputs: [
                {
                    name: 'currentPassword',
                    placeholder: 'Current Password',
                    type: 'password'
                },
                {
                    name: 'password',
                    placeholder: 'New Password',
                    type: 'password'
                },
                {
                    name: 'confirmPassword',
                    placeholder: 'Confirm Password',
                    type: 'password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        var currentPassword = data["currentPassword"];
                        var credential = __WEBPACK_IMPORTED_MODULE_9_firebase__["auth"].EmailAuthProvider.credential(_this.user.email, currentPassword);
                        // Check if currentPassword entered is correct
                        _this.loadingProvider.show();
                        __WEBPACK_IMPORTED_MODULE_9_firebase__["auth"]().currentUser.reauthenticateWithCredential(credential)
                            .then(function (success) {
                            var password = data["password"];
                            // Check if entered password is not the same as the currentPassword
                            if (password != currentPassword) {
                                if (password.length >= __WEBPACK_IMPORTED_MODULE_8__validator__["a" /* Validator */].profilePasswordValidator.minLength) {
                                    if (__WEBPACK_IMPORTED_MODULE_8__validator__["a" /* Validator */].profilePasswordValidator.pattern.test(password)) {
                                        if (password == data["confirmPassword"]) {
                                            // Update password on Firebase.
                                            __WEBPACK_IMPORTED_MODULE_9_firebase__["auth"]().currentUser.updatePassword(password)
                                                .then(function (success) {
                                                _this.loadingProvider.hide();
                                                __WEBPACK_IMPORTED_MODULE_8__validator__["a" /* Validator */].profilePasswordValidator.pattern.test(password);
                                                _this.alertProvider.showPasswordChangedMessage();
                                            })
                                                .catch(function (error) {
                                                _this.loadingProvider.hide();
                                                var code = error["code"];
                                                _this.alertProvider.showErrorMessage(code);
                                                if (code == 'auth/requires-recent-login') {
                                                    _this.logoutProvider.logout();
                                                }
                                            });
                                        }
                                        else {
                                            _this.alertProvider.showErrorMessage('profile/passwords-do-not-match');
                                        }
                                    }
                                    else {
                                        _this.alertProvider.showErrorMessage('profile/invalid-chars-password');
                                    }
                                }
                                else {
                                    _this.alertProvider.showErrorMessage('profile/password-too-short');
                                }
                            }
                        })
                            .catch(function (error) {
                            //Show error
                            _this.loadingProvider.hide();
                            var code = error["code"];
                            _this.alertProvider.showErrorMessage(code);
                        });
                    }
                }
            ]
        }).present();
    };
    // Delete the user account. After deleting the Firebase user, the userData along with their profile pic uploaded on the storage will be deleted as well.
    // If you added some other info or traces for the account, make sure to account for them when deleting the account.
    HomePage.prototype.deleteAccount = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to delete your account? This cannot be undone.',
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Delete',
                    handler: function (data) {
                        _this.loadingProvider.show();
                        // Delete Firebase user
                        __WEBPACK_IMPORTED_MODULE_9_firebase__["auth"]().currentUser.delete()
                            .then(function (success) {
                            // Delete profilePic of user on Firebase storage
                            _this.imageProvider.deleteUserImageFile(_this.user);
                            // Delete user data on Database
                            _this.angularfire.object('/accounts/' + _this.user.userId).remove().then(function () {
                                _this.loadingProvider.hide();
                                _this.alertProvider.showAccountDeletedMessage();
                                _this.logoutProvider.logout();
                            });
                        })
                            .catch(function (error) {
                            _this.loadingProvider.hide();
                            var code = error["code"];
                            _this.alertProvider.showErrorMessage(code);
                            if (code == 'auth/requires-recent-login') {
                                _this.logoutProvider.logout();
                            }
                        });
                    }
                }
            ]
        }).present();
    };
    // Log the user out.
    HomePage.prototype.logout = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Confirm Logout',
            message: 'Are you sure you want to logout?',
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Logout',
                    handler: function (data) { _this.logoutProvider.logout(); }
                }
            ]
        }).present();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\home\home.html"*/'<ion-header>\n<ion-navbar hideBackButton color=primary>\n    <ion-title>Profile</ion-title>\n\n  <button ion-button icon-only menuToggle>\n    <ion-icon name="menu"></ion-icon>\n  </button>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div *ngIf="user">\n    <div class="center">\n      <img src="{{user.img}}" style="border-radius: 100%;" tappable (click)="setPhoto()" onError="this.src=\'./assets/img/default.png\'">\n    </div>\n\n    <ion-list padding no-lines style="background: transparent">\n      <ion-item>\n        <ion-label stacked>שם מלא</ion-label>\n        <ion-input type="text" [(ngModel)]="user.name"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>שם משתמש</ion-label>\n        <ion-input type="text" [(ngModel)]="user.username"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>פרטים</ion-label>\n        <ion-textarea [(ngModel)]="user.description"></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>אימייל</ion-label>\n        <ion-input type="text" [(ngModel)]="user.email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>מין</ion-label>\n        <ion-select [(ngModel)]="user.gender">\n          <ion-option value="male">Male</ion-option>\n          <ion-option value="female">Female</ion-option>\n          <ion-option value="transgender">TransGender</ion-option>\n          <ion-option value="none">None</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>יום הולדת</ion-label>\n        <ion-datetime displayFormat="DD MMM YYYY" pickerFormat="DD MMM YYYY" [(ngModel)]="user.dob"></ion-datetime>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>מיקום</ion-label>\n        <ion-input type="text" id="txtHome" [(ngModel)]="user.location" (input)="search($event)"></ion-input>\n      </ion-item>\n      <div style="padding:10px">\n        <button ion-button block (click)="save()">שמור הגדרות</button>\n      </div>\n    </ion-list>\n    <ion-item-divider>הגדרות אחרות</ion-item-divider>\n    <ion-list no-lines padding>\n\n      <ion-item>\n        <ion-label>אפשר ניראות ציבורית</ion-label>\n        <ion-toggle item-right [(ngModel)]="user.publicVisibility" (ionChange)="changeVisibility()"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-label>סטאטוס ניראה</ion-label>\n        <ion-toggle item-right [(ngModel)]="showOnline" (ionChange)="changeStatus()"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-label>אפשר קבלת הודעות מערכת</ion-label>\n        <ion-toggle item-right [(ngModel)]="isPushEnabled" [disabled]="isBrowser" (ionChange)="changeNotification()"></ion-toggle>\n      </ion-item>\n      <ion-item (click)="showBlockedList()">\n        <h2>רשימה שחורה</h2>\n        <ion-icon item-right name="arrow-forward"></ion-icon>\n      </ion-item>\n      <ion-item tappable (click)="setPassword()" *ngIf="user && user.provider == \'Email\'">\n        עדכן סיסמא\n      </ion-item>\n      <ion-item tappable (click)="logout()">התנתק</ion-item>\n      <ion-item tappable (click)="deleteAccount()" style="color:#F44336">\n        מחק חשבון\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__providers_logout__["a" /* LogoutProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_image__["a" /* ImageProvider */],
            __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_4__providers_alert__["a" /* AlertProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_data__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_11__ionic_native_firebase__["a" /* Firebase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_12__angular_http__["a" /* Http */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__message_message__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_alert__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_firebase__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__user_info_user_info__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__friends_filter_friends_filter__ = __webpack_require__(335);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var FriendsPage = (function () {
    // FriendsPage
    // This is the page where the user can search, view, and initiate a chat with their friends.
    function FriendsPage(navCtrl, navParams, app, dataProvider, loadingProvider, alertProvider, alertCtrl, firebaseProvider, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.dataProvider = dataProvider;
        this.loadingProvider = loadingProvider;
        this.alertProvider = alertProvider;
        this.alertCtrl = alertCtrl;
        this.firebaseProvider = firebaseProvider;
        this.modalCtrl = modalCtrl;
        this.friendRequests = [];
        this.requestsSent = [];
        this.friendRequestCount = 0;
        this.accounts = [];
        this.excludedIds = [];
    }
    FriendsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.tab = "friends";
        this.title = "Friends";
        this.searchFriend = '';
        this.dataProvider.getRequests(__WEBPACK_IMPORTED_MODULE_7_firebase__["auth"]().currentUser.uid).snapshotChanges().subscribe(function (requestsRes) {
            var requests = requestsRes.payload.val();
            console.log(requests);
            if (requests != null) {
                if (requests.friendRequests != null && requests.friendRequests != undefined)
                    _this.friendRequestCount = requests.friendRequests.length;
                else
                    _this.friendRequestCount = 0;
            }
            else
                _this.friendRequestCount = 0;
            console.log(_this.friendRequestCount);
        });
        this.getFriends();
    };
    FriendsPage.prototype.segmentChanged = function ($event) {
        if (this.tab == 'friends') {
            this.title = "Friends";
            this.getFriends();
        }
        else if (this.tab == 'requests') {
            this.title = "Friend Requests";
            this.getFriendRequests();
        }
        else if (this.tab == 'search') {
            this.title = "Find New Friends";
            this.findNewFriends();
        }
    };
    FriendsPage.prototype.openFilter = function () {
        var _this = this;
        this.findNewFriends();
        var friendModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__friends_filter_friends_filter__["a" /* FriendsFilterPage */]);
        friendModal.present();
        friendModal.onDidDismiss(function (data) {
            console.log(data);
            if (data != undefined) {
                _this.accounts = _this.accounts.filter(function (acc) {
                    if ((acc.age >= data.ageStart) && (acc.age <= data.ageEnd) && acc.location == data.location)
                        return true;
                    return false;
                });
            }
        });
    };
    FriendsPage.prototype.getFriends = function () {
        var _this = this;
        this.loadingProvider.show();
        this.friends = [];
        // Get user data on database and get list of friends.
        this.dataProvider.getCurrentUser().snapshotChanges().subscribe(function (account) {
            if (account.payload.val() != null && account.payload.val().friends != null) {
                for (var i = 0; i < account.payload.val().friends.length; i++) {
                    _this.dataProvider.getUser(account.payload.val().friends[i]).snapshotChanges().subscribe(function (friend) {
                        if (friend.key != null) {
                            var friendData = __assign({ $key: friend.key }, friend.payload.val());
                            _this.addOrUpdateFriend(friendData);
                        }
                    });
                }
            }
            else {
                _this.friends = [];
            }
            _this.loadingProvider.hide();
        });
    };
    // Add or update friend data for real-time sync.
    FriendsPage.prototype.addOrUpdateFriend = function (friend) {
        console.log(friend);
        if (!this.friends) {
            this.friends = [friend];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.friends.length; i++) {
                if (this.friends[i].$key == friend.$key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.friends[index] = friend;
            }
            else {
                this.friends.push(friend);
            }
        }
        console.log(this.friends);
    };
    // Proceed to userInfo page.
    FriendsPage.prototype.viewUser = function (userId) {
        console.log(userId);
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_8__user_info_user_info__["a" /* UserInfoPage */], { userId: userId });
    };
    // Proceed to chat page.
    FriendsPage.prototype.message = function (userId) {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_2__message_message__["a" /* MessagePage */], { userId: userId });
    };
    // Manageing Friend Requests
    FriendsPage.prototype.getFriendRequests = function () {
        var _this = this;
        this.friendRequests = [];
        this.requestsSent = [];
        this.loadingProvider.show();
        // Get user info
        this.dataProvider.getCurrentUser().snapshotChanges().subscribe(function (account) {
            _this.account = account.payload.val();
            console.log(_this.account);
            // Get friendRequests and requestsSent of the user.
            _this.dataProvider.getRequests(_this.account.userId).snapshotChanges().subscribe(function (requestsRes) {
                // friendRequests.
                var requests = requestsRes.payload.val();
                if (requests != null) {
                    if (requests.friendRequests != null && requests.friendRequests != undefined) {
                        _this.friendRequests = [];
                        _this.friendRequestCount = requests.friendRequests.length;
                        requests.friendRequests.forEach(function (userId) {
                            _this.dataProvider.getUser(userId).snapshotChanges().subscribe(function (sender) {
                                sender = __assign({ $key: sender.key }, sender.payload.val());
                                _this.addOrUpdateFriendRequest(sender);
                            });
                        });
                    }
                    else {
                        _this.friendRequests = [];
                    }
                    // requestsSent.
                    if (requests.requestsSent != null && requests.requestsSent != undefined) {
                        _this.requestsSent = [];
                        requests.requestsSent.forEach(function (userId) {
                            _this.dataProvider.getUser(userId).snapshotChanges().subscribe(function (receiver) {
                                receiver = __assign({ $key: receiver.key }, receiver.payload.val());
                                _this.addOrUpdateRequestSent(receiver);
                            });
                        });
                    }
                    else {
                        _this.requestsSent = [];
                    }
                }
                _this.loadingProvider.hide();
            });
        });
    };
    // Add or update friend request only if not yet friends.
    FriendsPage.prototype.addOrUpdateFriendRequest = function (sender) {
        if (!this.friendRequests) {
            this.friendRequests = [sender];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.friendRequests.length; i++) {
                if (this.friendRequests[i].$key == sender.$key) {
                    index = i;
                }
            }
            if (index > -1) {
                if (!this.isFriends(sender.$key))
                    this.friendRequests[index] = sender;
            }
            else {
                if (!this.isFriends(sender.$key))
                    this.friendRequests.push(sender);
            }
        }
    };
    // Add or update requests sent only if the user is not yet a friend.
    FriendsPage.prototype.addOrUpdateRequestSent = function (receiver) {
        if (!this.requestsSent) {
            this.requestsSent = [receiver];
        }
        else {
            var index = -1;
            for (var j = 0; j < this.requestsSent.length; j++) {
                if (this.requestsSent[j].$key == receiver.$key) {
                    index = j;
                }
            }
            if (index > -1) {
                if (!this.isFriends(receiver.$key))
                    this.requestsSent[index] = receiver;
            }
            else {
                if (!this.isFriends(receiver.$key))
                    this.requestsSent.push(receiver);
            }
        }
    };
    FriendsPage.prototype.findNewFriends = function () {
        var _this = this;
        this.requestsSent = [];
        this.friendRequests = [];
        // Initialize
        this.loadingProvider.show();
        this.searchUser = '';
        // Get all users.
        this.dataProvider.getUsers().snapshotChanges().subscribe(function (accounts) {
            _this.loadingProvider.hide();
            // applying Filters
            var acc = accounts.filter(function (c) {
                if (c.key == null && c.key == undefined && c.payload.val() == null)
                    return false;
                if (c.payload.val().name == '' || c.payload.val().name == ' ' || c.payload.val().name == undefined)
                    return false;
                if (c.payload.val().publicVisibility == false)
                    return false;
                return true;
            });
            _this.accounts = acc.map(function (c) {
                return __assign({ $key: c.key }, c.payload.val());
            });
            _this.dataProvider.getCurrentUser().snapshotChanges().subscribe(function (account) {
                // Add own userId as exludedIds.
                // console.log(account.payload.val());
                _this.excludedIds = [];
                _this.account = account.payload.val();
                if (_this.excludedIds.indexOf(account.key) == -1) {
                    _this.excludedIds.push(account.key);
                }
                // Get friends which will be filtered out from the list using searchFilter pipe pipes/search.ts.
                if (account.payload.val() != null) {
                    // console.log(account.payload.val().friends);
                    if (account.payload.val().friends != null) {
                        account.payload.val().friends.forEach(function (friend) {
                            if (_this.excludedIds.indexOf(friend) == -1) {
                                _this.excludedIds.push(friend);
                            }
                        });
                    }
                }
                // Get requests of the currentUser.
                _this.dataProvider.getRequests(account.key).snapshotChanges().subscribe(function (requests) {
                    if (requests.payload.val() != null) {
                        _this.requestsSent = requests.payload.val().requestsSent;
                        _this.friendRequests = requests.payload.val().friendRequests;
                    }
                });
            });
        });
    };
    // Send friend request.
    FriendsPage.prototype.sendFriendRequest = function (user) {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Send Friend Request',
            message: 'Do you want to send friend request to <b>' + user.name + '</b>?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Send',
                    handler: function () {
                        _this.firebaseProvider.sendFriendRequest(user.$key);
                    }
                }
            ]
        }).present();
    };
    // Accept Friend Request.
    FriendsPage.prototype.acceptFriendRequest = function (user) {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Confirm Friend Request',
            message: 'Do you want to accept <b>' + user.name + '</b> as your friend?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Reject Request',
                    handler: function () {
                        _this.firebaseProvider.deleteFriendRequest(user.$key);
                        _this.getFriendRequests();
                    }
                },
                {
                    text: 'Accept Request',
                    handler: function () {
                        _this.firebaseProvider.acceptFriendRequest(user.$key);
                        _this.getFriendRequests();
                    }
                }
            ]
        }).present();
    };
    // Cancel Friend Request sent.
    FriendsPage.prototype.cancelFriendRequest = function (user) {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Friend Request Pending',
            message: 'Do you want to delete your friend request to <b>' + user.name + '</b>?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Delete',
                    handler: function () {
                        _this.firebaseProvider.cancelFriendRequest(user.$key);
                        _this.getFriendRequests();
                    }
                }
            ]
        }).present();
    };
    // Checks if user is already friends with this user.
    FriendsPage.prototype.isFriends = function (userId) {
        if (this.account.friends) {
            if (this.account.friends.indexOf(userId) == -1) {
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    };
    // Get the status of the user in relation to the logged in user.
    FriendsPage.prototype.getStatus = function (user) {
        // Returns:
        // 0 when user can be requested as friend.
        // 1 when a friend request was already sent to this user.
        // 2 when this user has a pending friend request.
        if (this.requestsSent) {
            for (var i = 0; i < this.requestsSent.length; i++) {
                if (this.requestsSent[i] == user.$key) {
                    return 1;
                }
            }
        }
        if (this.friendRequests) {
            for (var j = 0; j < this.friendRequests.length; j++) {
                if (this.friendRequests[j] == user.$key) {
                    return 2;
                }
            }
        }
        return 0;
    };
    FriendsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-friends',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\friends\friends.html"*/'<ion-header>\n  <ion-navbar color="primary">\n<button ion-button icon-only menuToggle>\n  <ion-icon name="menu"></ion-icon>\n</button>\n    <ion-title>{{title}}</ion-title>\n    <ion-buttons end>\n      <button ion-button *ngIf="this.tab == \'search\'" (click)="openFilter()"><ion-icon name="funnel"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-segment padding [(ngModel)]="tab" (ionChange)="segmentChanged($event)">\n    <ion-segment-button value="friends">\n      FRIENDS\n    </ion-segment-button>\n    <ion-segment-button value="requests">\n      REQUESTS\n      <span *ngIf="friendRequestCount != 0">({{friendRequestCount}})</span>\n    </ion-segment-button>\n    <ion-segment-button value="search">\n      FIND NEW\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]="tab">\n    <div *ngSwitchCase="\'friends\'">\n      <!-- No friends to show -->\n      <div class="empty-list" *ngIf="friends && friends.length == 0">\n        <h1>\n          <ion-icon name="contacts"></ion-icon>\n        </h1>\n        <p>You don\'t have new friend yet</p>\n      </div>\n      <!-- Show list of friends -->\n      <ion-list class="avatar-list" *ngIf="friends && friends.length > 0">\n        <!-- <ion-searchbar [(ngModel)]="searchFriend" placeholder="Search for friend or username" showCancelButton="true" cancelButtonText="Done"></ion-searchbar> -->\n        <ion-item *ngFor="let friend of friends | friendFilter:searchFriend" no-lines (click)="message(friend.$key); $event.stopPropagation();">\n          <ion-avatar item-left>\n            <img src="{{friend.img}}" onError="this.src=\'./assets/images/default-dp.png\'">\n          </ion-avatar>\n          <ion-note item-right *ngIf="friend.online == true">online</ion-note>\n          <h2>{{friend.name}}</h2>\n          <p>{{friend.description}}</p>\n        </ion-item>\n      </ion-list>\n    </div>\n\n\n\n    <div *ngSwitchCase="\'requests\'">\n      <!-- No friend requests sent or received. -->\n      <div class="empty-list" *ngIf="(friendRequests && friendRequests.length == 0) && (requestsSent && requestsSent.length == 0)">\n        <h1>\n          <ion-icon name="md-filing"></ion-icon>\n        </h1>\n        <p>No New Requests</p>\n      </div>\n      <!-- Show friend requests received. -->\n      <ion-list class="avatar-list" *ngIf="friendRequests && friendRequests.length > 0">\n        <ion-item *ngFor="let friendRequest of friendRequests" no-lines tappable (click)="viewUser(friendRequest.$key)">\n          <button item-right color="acceptrequest" ion-button outline tappable (click)="acceptFriendRequest(friendRequest); $event.stopPropagation();">\n            Accept\n          </button>\n          <ion-avatar item-left>\n            <img src="{{friendRequest.img}}" onError="this.src=\'./assets/images/default-dp.png\'">\n          </ion-avatar>\n          <h2>{{friendRequest.name}}</h2>\n          <p>has sent you a friend request.</p>\n        </ion-item>\n      </ion-list>\n      <!-- Show friend requests sent. -->\n      <ion-list class="avatar-list" *ngIf="requestsSent && requestsSent.length > 0">\n        <ion-item *ngFor="let requestSent of requestsSent" no-lines tappable (click)="viewUser(requestSent.$key)">\n          <button item-right color="cancelrequest" ion-button outline tappable (click)="cancelFriendRequest(requestSent); $event.stopPropagation();">\n            Cancel\n          </button>\n          <ion-avatar item-left>\n            <img src="{{requestSent.img}}" onError="this.src=\'./assets/images/default-dp.png\'">\n          </ion-avatar>\n          <h2>{{requestSent.name}}</h2>\n          <p>friend request sent.</p>\n        </ion-item>\n      </ion-list>\n    </div>\n\n    <div *ngSwitchCase="\'search\'">\n      <!-- No other users to send friend request right now. -->\n      <div class="empty-list" *ngIf="accounts && (accounts.length == 0 || (accounts.length == excludedIds.length))">\n        <h1>\n          <ion-icon name="md-search"></ion-icon>\n        </h1>\n        <p>We can\'t find new users right now</p>\n      </div>\n      <!-- Show other users excluding yourself, and friends with the help of searchFilter pipe. -->\n      <ion-list class="avatar-list" *ngIf="accounts && accounts.length > 0">\n        <ion-searchbar *ngIf="accounts.length != excludedIds.length" [(ngModel)]="searchUser" placeholder="Search for name or username"\n          showCancelButton="true" cancelButtonText="Done"></ion-searchbar>\n        <ion-item *ngFor="let account of accounts | searchFilter: [excludedIds, searchUser]" no-lines tappable (click)="viewUser(account.$key)">\n          <div item-right>\n            <!-- Show appropriate buttons depending on the status of this user in relation to the current user. -->\n            <!-- // Returns:\n                // 0 when user can be requested as friend.\n                // 1 when a friend request was already sent to this user.\n                // 2 when this user has a pending friend request. -->\n            <button color="sendrequest" ion-button outline tappable (click)="sendFriendRequest(account); $event.stopPropagation();" *ngIf="getStatus(account) == 0">\n              <!-- <ion-icon name="md-add-circle" class="success"></ion-icon> -->\n              add\n            </button>\n            <button color="cancelrequest" ion-button outline tappable (click)="cancelFriendRequest(account); $event.stopPropagation();"\n              *ngIf="getStatus(account) == 1">\n              <!-- <ion-icon name="md-close-circle" class="danger"></ion-icon> -->\n              cancel\n            </button>\n            <button color="acceptrequest" ion-button outline tappable (click)="acceptFriendRequest(account); $event.stopPropagation();"\n              *ngIf="getStatus(account) == 2">\n              <!-- <ion-icon name="md-checkmark-circle" class="success"></ion-icon> -->\n              accept\n            </button>\n          </div>\n          <ion-avatar item-left>\n            <img src="{{account.img}}" onError="this.src=\'./assets/images/default-dp.png\'">\n          </ion-avatar>\n          <h2>{{account.name}}</h2>\n          <p>@{{account.username}}</p>\n        </ion-item>\n      </ion-list>\n    </div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\friends\friends.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_3__providers_data__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_alert__["a" /* AlertProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_6__providers_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], FriendsPage);
    return FriendsPage;
}());

//# sourceMappingURL=friends.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_image__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_contacts__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_keyboard__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__group_info_group_info__ = __webpack_require__(339);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var GroupPage = (function () {
    // GroupPage
    // This is the page where the user can chat with other group members and view group info.
    function GroupPage(navCtrl, navParams, dataProvider, modalCtrl, angularfire, alertCtrl, imageProvider, loadingProvider, camera, keyboard, actionSheet, contacts, geolocation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataProvider = dataProvider;
        this.modalCtrl = modalCtrl;
        this.angularfire = angularfire;
        this.alertCtrl = alertCtrl;
        this.imageProvider = imageProvider;
        this.loadingProvider = loadingProvider;
        this.camera = camera;
        this.keyboard = keyboard;
        this.actionSheet = actionSheet;
        this.contacts = contacts;
        this.geolocation = geolocation;
        this.startIndex = -1;
        this.scrollDirection = 'bottom';
        // Set number of messages to show.
        this.numberOfMessages = 10;
    }
    GroupPage_1 = GroupPage;
    GroupPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // Get group details
        this.groupId = this.navParams.get('groupId');
        this.subscription = this.dataProvider.getGroup(this.groupId).snapshotChanges().subscribe(function (group) {
            if (group.payload.exists()) {
                _this.title = group.payload.val().name;
                // Get group messages
                _this.dataProvider.getGroupMessages(group.key).snapshotChanges().subscribe(function (messagesRes) {
                    var messages = messagesRes.payload.val();
                    if (messages == null || messages == undefined)
                        messages = [];
                    console.log(_this.messages);
                    if (_this.messages != null && _this.messages != undefined) {
                        // Just append newly added messages to the bottom of the view.
                        if (messages.length > _this.messages.length) {
                            var message_1 = messages[messages.length - 1];
                            _this.dataProvider.getUser(message_1.sender).snapshotChanges().subscribe(function (user) {
                                message_1.avatar = user.payload.val().img;
                            });
                            _this.messages.push(message_1);
                            // Also append to messagesToShow.
                            _this.messagesToShow.push(message_1);
                            // Reset scrollDirection to bottom.
                            _this.scrollDirection = 'bottom';
                        }
                    }
                    else {
                        // Get all messages, this will be used as reference object for messagesToShow.
                        _this.messages = [];
                        messages.forEach(function (message) {
                            console.log(message);
                            _this.dataProvider.getUser(message.sender).snapshotChanges().subscribe(function (user) {
                                if (user.key != null) {
                                    message.avatar = user.payload.val().img;
                                }
                            });
                            _this.messages.push(message);
                        });
                        // Load messages in relation to numOfMessages.
                        if (_this.startIndex == -1) {
                            // Get initial index for numberOfMessages to show.
                            if ((_this.messages.length - _this.numberOfMessages) > 0) {
                                _this.startIndex = _this.messages.length - _this.numberOfMessages;
                            }
                            else {
                                _this.startIndex = 0;
                            }
                        }
                        if (!_this.messagesToShow) {
                            _this.messagesToShow = [];
                        }
                        // Set messagesToShow
                        for (var i = _this.startIndex; i < _this.messages.length; i++) {
                            _this.messagesToShow.push(_this.messages[i]);
                        }
                        _this.loadingProvider.hide();
                    }
                });
            }
        });
        // Update messages' date time elapsed every minute based on Moment.js.
        var that = this;
        if (!that.updateDateTime) {
            that.updateDateTime = setInterval(function () {
                if (that.messages) {
                    that.messages.forEach(function (message) {
                        var date = message.date;
                        message.date = new Date(date);
                    });
                }
            }, 60000);
        }
    };
    // Load previous messages in relation to numberOfMessages.
    GroupPage.prototype.loadPreviousMessages = function () {
        var that = this;
        // Show loading.
        this.loadingProvider.show();
        setTimeout(function () {
            // Set startIndex to load more messages.
            if (that.startIndex - that.numberOfMessages > -1) {
                that.startIndex -= that.numberOfMessages;
            }
            else {
                that.startIndex = 0;
            }
            // Refresh our messages list.
            that.messages = null;
            that.messagesToShow = null;
            // Set scroll direction to top.
            that.scrollDirection = 'top';
            // Populate list again.
            that.ionViewDidLoad();
        }, 1000);
    };
    // Update messagesRead when user lefts this page.
    GroupPage.prototype.ionViewWillLeave = function () {
        if (this.messages)
            this.setMessagesRead(this.messages);
    };
    // Check if currentPage is active, then update user's messagesRead.
    GroupPage.prototype.setMessagesRead = function (messages) {
        if (this.navCtrl.getActive().instance instanceof GroupPage_1) {
            // Update user's messagesRead on database.
            this.angularfire.object('/accounts/' + __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid + '/groups/' + this.groupId).update({
                messagesRead: this.messages.length
            });
        }
    };
    // Check if 'return' button is pressed and send the message.
    GroupPage.prototype.onType = function (keyCode) {
        if (keyCode == 13) {
            // this.keyboard.close();
            // this.send();
        }
    };
    // Scroll to bottom of page after a short delay.
    GroupPage.prototype.scrollBottom = function () {
        var that = this;
        setTimeout(function () {
            that.content.scrollToBottom();
        }, 300);
    };
    // Scroll to top of the page after a short delay.
    GroupPage.prototype.scrollTop = function () {
        var that = this;
        setTimeout(function () {
            that.content.scrollToTop();
        }, 300);
    };
    // Scroll depending on the direction.
    GroupPage.prototype.doScroll = function () {
        if (this.scrollDirection == 'bottom') {
            this.scrollBottom();
        }
        else if (this.scrollDirection == 'top') {
            this.scrollTop();
        }
    };
    // Check if the user is the sender of the message.
    GroupPage.prototype.isSender = function (message) {
        if (message.sender == __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid) {
            return true;
        }
        else {
            return false;
        }
    };
    // Check if the message is a system message.
    GroupPage.prototype.isSystemMessage = function (message) {
        if (message.type == 'system') {
            return true;
        }
        else {
            return false;
        }
    };
    // View user info
    GroupPage.prototype.viewUser = function (userId) {
        this.navCtrl.push("UserInfoPage", { userId: userId });
    };
    // Send text message to the group.
    GroupPage.prototype.send = function (type) {
        // Clone an instance of messages object so it will not directly be updated.
        // The messages object should be updated by our observer declared on ionViewDidLoad.
        var messages = JSON.parse(JSON.stringify(this.messages));
        messages.push({
            date: new Date().toString(),
            sender: __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid,
            type: type,
            message: this.message
        });
        // Update group messages.
        this.dataProvider.getGroup(this.groupId).update({
            messages: messages
        });
        // Clear messagebox.
        this.message = '';
    };
    // Enlarge image messages.
    GroupPage.prototype.enlargeImage = function (img) {
        var imageModal = this.modalCtrl.create("ImageModalPage", { img: img });
        imageModal.present();
    };
    GroupPage.prototype.attach = function () {
        var _this = this;
        var action = this.actionSheet.create({
            title: 'Choose attachments',
            buttons: [{
                    text: 'Camera',
                    handler: function () {
                        console.log("take photo");
                        _this.imageProvider.uploadGroupPhotoMessage(_this.groupId, _this.camera.PictureSourceType.CAMERA).then(function (url) {
                            // Process image message.
                            _this.sendPhotoMessage(url);
                        });
                    }
                }, {
                    text: 'Photo Library',
                    handler: function () {
                        console.log("Access gallery");
                        _this.imageProvider.uploadGroupPhotoMessage(_this.groupId, _this.camera.PictureSourceType.PHOTOLIBRARY).then(function (url) {
                            // Process image message.
                            _this.sendPhotoMessage(url);
                        });
                    }
                }, {
                    text: 'Video',
                    handler: function () {
                        console.log("Video");
                        _this.imageProvider.uploadGroupVideoMessage(_this.groupId).then(function (url) {
                            _this.sendVideoMessage(url);
                        });
                    }
                }, {
                    text: 'Location',
                    handler: function () {
                        console.log("Location");
                        _this.geolocation.getCurrentPosition({
                            timeout: 2000
                        }).then(function (res) {
                            var locationMessage = "current location: lat:" + res.coords.latitude + " lng:" + res.coords.longitude;
                            var mapUrl = "<a href='https://www.google.com/maps/search/" + res.coords.latitude + "," + res.coords.longitude + "'>View on Map</a>";
                            var confirm = _this.alertCtrl.create({
                                title: 'Your Location',
                                message: locationMessage,
                                buttons: [{
                                        text: 'cancel',
                                        handler: function () {
                                            console.log("canceled");
                                        }
                                    }, {
                                        text: 'Share',
                                        handler: function () {
                                            console.log("share");
                                            _this.message = locationMessage + "<br>" + mapUrl;
                                            _this.send('location');
                                        }
                                    }]
                            });
                            confirm.present();
                        }, function (locationErr) {
                            console.log("Location Error" + JSON.stringify(locationErr));
                        });
                    }
                }, {
                    text: 'Contact',
                    handler: function () {
                        console.log("Share contact");
                        _this.contacts.pickContact().then(function (data) {
                            console.log(data.displayName);
                            console.log(data.phoneNumbers[0].value);
                            _this.message = "<b>Name:</b> " + data.displayName + "<br><b>Mobile:</b> <a href='tel:" + data.phoneNumbers[0].value + "'>" + data.phoneNumbers[0].value + "</a>";
                            _this.send('contact');
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }, {
                    text: 'cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log("cancelled");
                    }
                }]
        });
        action.present();
    };
    GroupPage.prototype.takePhoto = function () {
        var _this = this;
        this.imageProvider.uploadGroupPhotoMessage(this.groupId, this.camera.PictureSourceType.CAMERA).then(function (url) {
            // Process image message.
            _this.sendPhotoMessage(url);
        });
    };
    // Process photoMessage on database.
    GroupPage.prototype.sendPhotoMessage = function (url) {
        var messages = JSON.parse(JSON.stringify(this.messages));
        messages.push({
            date: new Date().toString(),
            sender: __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid,
            type: 'image',
            url: url
        });
        this.dataProvider.getGroup(this.groupId).update({
            messages: messages
        });
        this.message = '';
    };
    GroupPage.prototype.sendVideoMessage = function (url) {
        var messages = JSON.parse(JSON.stringify(this.messages));
        messages.push({
            date: new Date().toString(),
            sender: __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid,
            type: 'video',
            url: url
        });
        this.dataProvider.getGroup(this.groupId).update({
            messages: messages
        });
        this.message = '';
    };
    // View group info.
    GroupPage.prototype.groupInfo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__group_info_group_info__["a" /* GroupInfoPage */], { groupId: this.groupId });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], GroupPage.prototype, "content", void 0);
    GroupPage = GroupPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-group',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\group\group.html"*/'<ion-header>\n  <ion-navbar color="white">\n    <ion-title tappable (click)="groupInfo()">{{title}}</ion-title>\n    <!-- View Group Info -->\n    <ion-buttons end>\n      <button ion-button icon-only tappable (click)="groupInfo()"><ion-icon name="ios-more"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content has-footer padding>\n  <!-- Messages -->\n  <div class="messages">\n    <p class="center" *ngIf="startIndex > 0"><span tappable (click)="loadPreviousMessages()">Load previous messages</span></p>\n    <div *ngFor="let message of messagesToShow">\n      <!--  System Message -->\n      <div *ngIf="isSystemMessage(message)" style="text-align:center; float: left; color:#ccc">\n        <p>\n          <ion-icon name="{{message.icon}}"></ion-icon>\n          {{message.message}} {{message.date | DateFormat}}\n        </p>\n      </div>\n      <!--  Message -->\n      <div *ngIf="isSender(message) && !isSystemMessage(message)" class="chatbox right">\n\n        <div *ngIf="isSender(message) && !isSystemMessage(message)">\n            <div class="right" *ngIf="message.type == \'text\'">\n              <p>{{message.message}}</p>\n              <span>{{message.date | DateFormat}}</span>\n            </div>\n          <div class="right" *ngIf="message.type == \'location\'" [innerHtml]="message.message"></div>\n          <div class="right" *ngIf="message.type == \'contact\'" [innerHtml]="message.message"></div>\n            <div class="right" *ngIf="message.type == \'image\'">\n              <img tappable (click)="enlargeImage(message.url)" src="{{message.url}}" (load)="doScroll()" onError="this.src=\'./assets/images/default-dp.png\'"/>\n              <span>{{message.date | DateFormat}}</span>\n            </div>\n            <div *ngIf="message.type == \'video\'">\n              <video controls width="100%">\n                 <source src="{{message.message}}" type="video/mp4">\n              </video>\n            </div>\n        </div>\n      </div>\n\n      <div *ngIf="!isSender(message) && !isSystemMessage(message)" class="chatbox left">\n        <img src="{{message.avatar}}" tappable (click)="viewUser(message.sender)" (load)="doScroll()" onError="this.src=\'./assets/images/default-dp.png\'" style="height:30px; border-radius: 100%;float:left;"/>\n        <div class="left" *ngIf="message.type == \'text\'">\n          <p>{{message.message}}</p>\n          <span>{{message.date | DateFormat}}</span>\n        </div>\n        <div class="left" *ngIf="message.type == \'location\'" [innerHtml]="message.message"></div>\n        <div class="left" *ngIf="message.type == \'contact\'" [innerHtml]="message.message"></div>\n        <div class="left" *ngIf="message.type == \'image\'">\n          <img tappable (click)="enlargeImage(message.url)" src="{{message.url}}" onError="this.src=\'./assets/images/default-dp.png\'" (load)="doScroll()"/>\n          <span>{{message.date | DateFormat}}</span>\n        </div>\n        <div *ngIf="message.type == \'video\'">\n          <video controls width="100%">\n             <source src="{{message.message}}" type="video/mp4">\n          </video>\n        </div>\n      </div>\n      \n    </div>\n  </div>\n</ion-content>\n<!-- Message Box -->\n<ion-footer>\n  <ion-item class="bottom_bar">\n    <button item-left ion-button clear (click)="attach()"><ion-icon name="md-attach"></ion-icon></button>\n    <ion-textarea type="text" rows="0" placeholder="Type your message" [(ngModel)]="message" (ionFocus)="scrollBottom()" (keypress)="onType($event.keyCode)"></ion-textarea>\n    <button item-right ion-button clear (click)="takePhoto()"><ion-icon name="md-camera"></ion-icon></button>\n    <button item-right ion-button clear (click)="send(\'text\')" [disabled]="!message"><ion-icon name="md-send"></ion-icon></button>\n  </ion-item>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\group\group.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_data__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_image__["a" /* ImageProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_keyboard__["a" /* Keyboard */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_contacts__["a" /* Contacts */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__["a" /* Geolocation */]])
    ], GroupPage);
    return GroupPage;
    var GroupPage_1;
}());

//# sourceMappingURL=group.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__settings__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__alert__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_plus__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_message_message__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var LoginProvider = (function () {
    function LoginProvider(loadingProvider, alertProvider, zone, googleplus, platform, afAuth, http, toastCtrl, facebook, alert) {
        var _this = this;
        this.loadingProvider = loadingProvider;
        this.alertProvider = alertProvider;
        this.zone = zone;
        this.googleplus = googleplus;
        this.platform = platform;
        this.afAuth = afAuth;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.facebook = facebook;
        this.alert = alert;
        console.log("got in login provider");
        __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().onAuthStateChanged(function (user) {
            if (user) {
                _this.zone.run(function () {
                    // this.navCtrl.setRoot(Settings.homePage, { animate: false });
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_message_message__["a" /* MessagePage */], { userId: user.uid });
                });
            }
        });
    }
    LoginProvider.prototype.setNavController = function (navCtrl) {
        this.navCtrl = navCtrl;
    };
    LoginProvider.prototype.facebookLogin = function () {
        var _this = this;
        if (this.platform.is('core')) {
            // let fbCredential;
            this.afAuth.auth.signInWithPopup(new __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"].FacebookAuthProvider()).then(function (fbRes) {
                console.log(fbRes);
                var fbcredential = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"].FacebookAuthProvider.credential(fbRes.credential.accessToken);
                console.log(fbcredential);
                _this.loadingProvider.show();
                __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().signInWithCredential(fbcredential).then(function (success) {
                    var data = fbRes.additionalUserInfo.profile;
                    var uid = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid;
                    console.log(uid);
                    if (fbRes.additionalUserInfo.isNewUser == true)
                        _this.createNewUser(uid, data.first_name, uid, data.email, 'I am available', 'Facebook', data.picture.data.url, data.gender);
                    _this.loadingProvider.hide();
                })
                    .catch(function (error) {
                    console.log(error);
                    _this.loadingProvider.hide();
                    _this.alertProvider.showErrorMessage(error["code"]);
                });
            }).catch(function (error) {
                console.log(error);
            });
        }
        else {
            this.facebook.login(['public_profile', 'email']).then(function (res) {
                console.log(res);
                var credential = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"].FacebookAuthProvider.credential(res.authResponse.accessToken);
                _this.loadingProvider.show();
                __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().signInWithCredential(credential)
                    .then(function (success) {
                    console.log(success);
                    _this.facebook.api("me/?fields=id,email,first_name,picture,gender", ["public_profile", "email"])
                        .then(function (data) {
                        console.log(data);
                        var uid = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid;
                        _this.createNewUser(uid, data.first_name, uid, data.email, 'I am available', 'Facebook', data.picture.data.url, data.gender);
                    })
                        .catch(function (err) {
                        console.log(err);
                        _this.loadingProvider.hide();
                    });
                })
                    .catch(function (error) {
                    _this.loadingProvider.hide();
                    _this.alertProvider.showErrorMessage(error["code"]);
                });
            }).catch(function (err) { return console.log(err); });
        }
    };
    LoginProvider.prototype.googleLogin = function () {
        var _this = this;
        if (this.platform.is('core')) {
            this.afAuth.auth.signInWithPopup(new __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"].GoogleAuthProvider()).then(function (gpRes) {
                console.log(gpRes);
                var credential = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"].GoogleAuthProvider.credential(gpRes.credential.idToken, gpRes.credential.accessToken);
                __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().signInWithCredential(credential)
                    .then(function (success) {
                    console.log(success);
                    var uid = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid;
                    var data = gpRes.additionalUserInfo.profile;
                    if (gpRes.additionalUserInfo.isNewUser == true)
                        _this.createNewUser(uid, data.name, uid, data.email, 'I am available', 'Google', data.picture, data.gender);
                    _this.loadingProvider.hide();
                })
                    .catch(function (error) {
                    _this.loadingProvider.hide();
                    _this.alertProvider.showErrorMessage(error["code"]);
                });
            }).catch(function (err) {
                console.log(err);
            });
        }
        else {
            this.loadingProvider.show();
            this.googleplus.login({
                'webClientId': __WEBPACK_IMPORTED_MODULE_1__settings__["a" /* Settings */].googleClientId
            }).then(function (success) {
                console.log(success);
                var credential = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"].GoogleAuthProvider.credential(success['idToken'], null);
                __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().signInWithCredential(credential)
                    .then(function (data) {
                    console.log(data);
                    _this.loadingProvider.hide();
                    var uid = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid;
                    _this.createNewUser(uid, data.displayName, uid, data.email, 'I am available', 'Google', data.imageUrl);
                })
                    .catch(function (error) {
                    _this.loadingProvider.hide();
                    _this.alertProvider.showErrorMessage(error["code"]);
                });
            }, function (error) {
                console.log(error);
                _this.loadingProvider.hide();
            });
        }
    };
    // Login on Firebase given the email and password.
    LoginProvider.prototype.emailLogin = function (email, password) {
        var _this = this;
        this.loadingProvider.show();
        __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().signInWithEmailAndPassword(email, password).then(function (success) {
            _this.loadingProvider.hide();
        }).catch(function (error) {
            _this.loadingProvider.hide();
            _this.alertProvider.showErrorMessage(error["code"]);
        });
    };
    LoginProvider.prototype.phoneLogin = function () {
        var _this = this;
        if (this.platform.is('core'))
            this.toastCtrl.create({ message: 'AccountKit only works on device', duration: 3000 }).present();
        else {
            window.AccountKitPlugin.loginWithPhoneNumber({
                useAccessToken: true,
                defaultCountryCode: "IN",
                facebookNotificationsEnabled: true,
            }, function (data) {
                window.AccountKitPlugin.getAccount(function (info) {
                    var phoneNumber = info.phoneNumber;
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_1__settings__["a" /* Settings */].customTokenUrl + "?access_token=" + info.token).subscribe(function (data) {
                        var token = data['_body'];
                        _this.loadingProvider.show();
                        __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().signInWithCustomToken(token).then(function (data) {
                            var uid = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid;
                            _this.createNewUser(uid, phoneNumber, uid, null, 'I am available', 'Phone', './assets/images/default-dp.png');
                            _this.loadingProvider.hide();
                        }).catch(function (err) {
                            _this.loadingProvider.hide();
                            console.log(err);
                        });
                    }, function (err) {
                        console.log(err);
                    });
                }, function (err) { return console.log(err); });
            });
        }
    };
    // Register user on Firebase given the email and password.
    LoginProvider.prototype.register = function (name, username, email, password, img) {
        var _this = this;
        this.loadingProvider.show();
        __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().createUserWithEmailAndPassword(email, password).then(function (success) {
            var user = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser;
            _this.createNewUser(user.uid, name, username, user.email, "I am available", "Firebase", img);
            _this.loadingProvider.hide();
        }).catch(function (error) {
            _this.loadingProvider.hide();
            _this.alertProvider.showErrorMessage(error["code"]);
        });
    };
    // Send Password Reset Email to the user.
    LoginProvider.prototype.sendPasswordReset = function (email) {
        var _this = this;
        console.log(email);
        if (email != null || email != undefined || email != "") {
            this.loadingProvider.show();
            __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().sendPasswordResetEmail(email).then(function (success) {
                _this.loadingProvider.hide();
                _this.alertProvider.showPasswordResetMessage(email);
            }).catch(function (error) {
                _this.loadingProvider.hide();
                _this.alertProvider.showErrorMessage(error["code"]);
            });
        }
    };
    // Creating new user after signed up
    LoginProvider.prototype.createNewUser = function (userId, name, username, email, description, provider, img, gender) {
        if (description === void 0) { description = "I'm available"; }
        if (img === void 0) { img = "./assets/images/default-dp.png"; }
        if (gender === void 0) { gender = 'none'; }
        var dateCreated = new Date();
        __WEBPACK_IMPORTED_MODULE_6_firebase__["database"]().ref('accounts/' + userId).update({ dateCreated: dateCreated, username: username, name: name, userId: userId, email: email, description: description, provider: provider, img: img, gender: gender });
    };
    LoginProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_4__alert__["a" /* AlertProvider */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_plus__["a" /* GooglePlus */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ToastController */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__["a" /* Facebook */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */]])
    ], LoginProvider);
    return LoginProvider;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DataProvider = (function () {
    // Data Provider
    // This is the provider class for most of the Firebase observables in the app.
    function DataProvider(angularfire) {
        this.angularfire = angularfire;
        console.log("Initializing Data Provider");
    }
    // Get all users
    DataProvider.prototype.getUsers = function () {
        return this.angularfire.list('/accounts', function (ref) { return ref.orderByChild('name'); });
    };
    // Get user with username
    DataProvider.prototype.getUserWithUsername = function (username) {
        return this.angularfire.list('/accounts', function (ref) { return ref.orderByChild('username').equalTo(username); });
    };
    // Get logged in user data
    DataProvider.prototype.getCurrentUser = function () {
        return this.angularfire.object('/accounts/' + __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid);
    };
    // Get user by their userId
    DataProvider.prototype.getUser = function (userId) {
        return this.angularfire.object('/accounts/' + userId);
    };
    // Get requests given the userId.
    DataProvider.prototype.getRequests = function (userId) {
        return this.angularfire.object('/requests/' + userId);
    };
    // Get friend requests given the userId.
    DataProvider.prototype.getFriendRequests = function (userId) {
        return this.angularfire.list('/requests', function (ref) { return ref.orderByChild('receiver').equalTo(userId); });
    };
    // Get conversation given the conversationId.
    DataProvider.prototype.getConversation = function (conversationId) {
        return this.angularfire.object('/conversations/' + conversationId);
    };
    // Get conversations of the current logged in user.
    DataProvider.prototype.getConversations = function () {
        return this.angularfire.list('/accounts/' + __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid + '/conversations');
    };
    // Get messages of the conversation given the Id.
    DataProvider.prototype.getConversationMessages = function (conversationId) {
        return this.angularfire.object('/conversations/' + conversationId + '/messages');
    };
    // Get messages of the group given the Id.
    DataProvider.prototype.getGroupMessages = function (groupId) {
        return this.angularfire.object('/groups/' + groupId + '/messages');
    };
    // Get groups of the logged in user.
    DataProvider.prototype.getGroups = function () {
        return this.angularfire.list('/accounts/' + __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid + '/groups');
    };
    // Get group info given the groupId.
    DataProvider.prototype.getGroup = function (groupId) {
        return this.angularfire.object('/groups/' + groupId);
    };
    DataProvider.prototype.getBlockedLists = function () {
        return this.angularfire.list('/accounts/' + __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid + '/conversations', function (ref) { return ref.orderByChild('blocked').equalTo(true); });
    };
    DataProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], DataProvider);
    return DataProvider;
}());

//# sourceMappingURL=data.js.map

/***/ }),

/***/ 183:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 183;

/***/ }),

/***/ 226:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/blockedlist/blockedlist.module": [
		668,
		2
	],
	"../pages/details/details.module": [
		669,
		4
	],
	"../pages/filtermodal/filtermodal.module": [
		670,
		1
	],
	"../pages/image-modal/image-modal.module": [
		671,
		3
	],
	"../pages/register/register.module": [
		672,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 226;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__validator__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__logout__ = __webpack_require__(141);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var errorMessages = {
    // Alert Provider
    // This is the provider class for most of the success and error messages in the app.
    // If you added your own messages don't forget to make a function for them or add them in the showErrorMessage switch block.
    // Firebase Error Messages
    accountExistsWithDifferentCredential: { title: 'Account Exists!', subTitle: 'An account with the same credential already exists.' },
    invalidCredential: { title: 'Invalid Credential!', subTitle: 'An error occured logging in with this credential.' },
    operationNotAllowed: { title: 'Login Failed!', subTitle: 'Logging in with this provider is not allowed! Please contact support.' },
    userDisabled: { title: 'Account Disabled!', subTitle: 'Sorry! But this account has been suspended! Please contact support.' },
    userNotFound: { title: 'Account Not Found!', subTitle: 'Sorry, but an account with this credential could not be found.' },
    wrongPassword: { title: 'Incorrect Password!', subTitle: 'Sorry, but the password you have entered is incorrect.' },
    invalidEmail: { title: 'Invalid Email!', subTitle: 'Sorry, but you have entered an invalid email address.' },
    emailAlreadyInUse: { title: 'Email Not Available!', subTitle: 'Sorry, but this email is already in use.' },
    weakPassword: { title: 'Weak Password!', subTitle: 'Sorry, but you have entered a weak password.' },
    requiresRecentLogin: { title: 'Credential Expired!', subTitle: 'Sorry, but this credential has expired! Please login again.' },
    userMismatch: { title: 'User Mismatch!', subTitle: 'Sorry, but this credential is for another user!' },
    providerAlreadyLinked: { title: 'Already Linked!', subTitle: 'Sorry, but your account is already linked to this credential.' },
    credentialAlreadyInUse: { title: 'Credential Not Available!', subTitle: 'Sorry, but this credential is already used by another user.' },
    // Profile Error Messages
    changeName: { title: 'Change Name Failed!', subTitle: 'Sorry, but we\'ve encountered an error changing your name.' },
    invalidCharsName: __WEBPACK_IMPORTED_MODULE_2__validator__["a" /* Validator */].profileNameValidator.patternError,
    nameTooShort: __WEBPACK_IMPORTED_MODULE_2__validator__["a" /* Validator */].profileNameValidator.lengthError,
    changeEmail: { title: 'Change Email Failed!', subTitle: 'Sorry, but we\'ve encountered an error changing your email address.' },
    invalidProfileEmail: __WEBPACK_IMPORTED_MODULE_2__validator__["a" /* Validator */].profileEmailValidator.patternError,
    changePhoto: { title: 'Change Photo Failed!', subTitle: 'Sorry, but we\'ve encountered an error changing your photo.' },
    passwordTooShort: __WEBPACK_IMPORTED_MODULE_2__validator__["a" /* Validator */].profilePasswordValidator.lengthError,
    invalidCharsPassword: __WEBPACK_IMPORTED_MODULE_2__validator__["a" /* Validator */].profilePasswordValidator.patternError,
    passwordsDoNotMatch: { title: 'Change Password Failed!', subTitle: 'Sorry, but the passwords you entered do not match.' },
    updateProfile: { title: 'Update Profile Failed', subTitle: 'Sorry, but we\'ve encountered an error updating your profile.' },
    usernameExists: { title: 'Username Already Exists!', subTitle: 'Sorry, but this username is already taken by another user.' },
    // Image Error Messages
    imageUpload: { title: 'Image Upload Failed!', subTitle: 'Sorry but we\'ve encountered an error uploading selected image.' },
    // Group Error Messages
    groupUpdate: { title: 'Update Group Failed!', subTitle: 'Sorry, but we\'ve encountered an error updating this group.' },
    groupLeave: { title: 'Leave Group Failed!', subTitle: 'Sorry, but you\'ve encountered an error leaving this group.' },
    groupDelete: { title: 'Delete Group Failed!', subTitle: 'Sorry, but we\'ve encountered an error deleting this group.' }
};
var successMessages = {
    passwordResetSent: { title: 'Password Reset Sent!', subTitle: 'A password reset email has been sent to: ' },
    profileUpdated: { title: 'Profile Updated!', subTitle: 'Your profile has been successfully updated!' },
    emailVerified: { title: 'Email Confirmed!', subTitle: 'Congratulations! Your email has been confirmed!' },
    emailVerificationSent: { title: 'Email Confirmation Sent!', subTitle: 'An email confirmation has been sent to: ' },
    accountDeleted: { title: 'Account Deleted!', subTitle: 'Your account has been successfully deleted.' },
    passwordChanged: { title: 'Password Changed!', subTitle: 'Your password has been successfully changed.' },
    friendRequestSent: { title: 'Friend Request Sent!', subTitle: 'Your friend request has been successfully sent!' },
    friendRequestRemoved: { title: 'Friend Request Deleted!', subTitle: 'Your friend request has been successfully deleted.' },
    groupUpdated: { title: 'Group Updated!', subTitle: 'This group has been successfully updated!' },
    groupLeft: { title: 'Leave Group', subTitle: 'You have successfully left this group.' }
};
var AlertProvider = (function () {
    function AlertProvider(alertCtrl, logoutProvider) {
        this.alertCtrl = alertCtrl;
        this.logoutProvider = logoutProvider;
        console.log("Initializing Alert Provider");
    }
    // Show profile updated
    AlertProvider.prototype.showProfileUpdatedMessage = function () {
        this.alert = this.alertCtrl.create({
            title: successMessages.profileUpdated["title"],
            subTitle: successMessages.profileUpdated["subTitle"],
            buttons: ['OK']
        }).present();
    };
    // Show password reset sent
    AlertProvider.prototype.showPasswordResetMessage = function (email) {
        this.alert = this.alertCtrl.create({
            title: successMessages.passwordResetSent["title"],
            subTitle: successMessages.passwordResetSent["subTitle"] + email,
            buttons: ['OK']
        }).present();
    };
    // Show email verified and redirect to homePage
    AlertProvider.prototype.showEmailVerifiedMessageAndRedirect = function (navCtrl) {
        this.alert = this.alertCtrl.create({
            title: successMessages.emailVerified["title"],
            subTitle: successMessages.emailVerified["subTitle"],
            buttons: [{
                    text: 'OK',
                    handler: function () {
                        navCtrl.setRoot(Test);
                    }
                }]
        }).present();
    };
    // Show email verification sent
    AlertProvider.prototype.showEmailVerificationSentMessage = function (email) {
        this.alert = this.alertCtrl.create({
            title: successMessages.emailVerificationSent["title"],
            subTitle: successMessages.emailVerificationSent["subTitle"] + email,
            buttons: ['OK']
        }).present();
    };
    // Show account deleted
    AlertProvider.prototype.showAccountDeletedMessage = function () {
        this.alert = this.alertCtrl.create({
            title: successMessages.accountDeleted["title"],
            subTitle: successMessages.accountDeleted["subTitle"],
            buttons: ['OK']
        }).present();
    };
    // Show password changed
    AlertProvider.prototype.showPasswordChangedMessage = function () {
        this.alert = this.alertCtrl.create({
            title: successMessages.passwordChanged["title"],
            subTitle: successMessages.passwordChanged["subTitle"],
            buttons: ['OK']
        }).present();
    };
    // Show friend request sent
    AlertProvider.prototype.showFriendRequestSent = function () {
        this.alert = this.alertCtrl.create({
            title: successMessages.friendRequestSent["title"],
            subTitle: successMessages.friendRequestSent["subTitle"],
            buttons: ['OK']
        }).present();
    };
    // Show friend request removed
    AlertProvider.prototype.showFriendRequestRemoved = function () {
        this.alert = this.alertCtrl.create({
            title: successMessages.friendRequestRemoved["title"],
            subTitle: successMessages.friendRequestRemoved["subTitle"],
            buttons: ['OK']
        }).present();
    };
    // Show group updated.
    AlertProvider.prototype.showGroupUpdatedMessage = function () {
        this.alert = this.alertCtrl.create({
            title: successMessages.groupUpdated["title"],
            subTitle: successMessages.groupUpdated["subTitle"],
            buttons: ['OK']
        }).present();
    };
    // Show error messages depending on the code
    // If you added custom error codes on top, make sure to add a case block for it.
    AlertProvider.prototype.showErrorMessage = function (code) {
        switch (code) {
            // Firebase Error Messages
            case 'auth/account-exists-with-different-credential':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.accountExistsWithDifferentCredential["title"],
                    subTitle: errorMessages.accountExistsWithDifferentCredential["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/invalid-credential':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.invalidCredential["title"],
                    subTitle: errorMessages.invalidCredential["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/operation-not-allowed':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.operationNotAllowed["title"],
                    subTitle: errorMessages.operationNotAllowed["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/popup-closed-by-user':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.operationNotAllowed["title"],
                    subTitle: errorMessages.operationNotAllowed["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/user-disabled':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.userDisabled["title"],
                    subTitle: errorMessages.userDisabled["subTitle"],
                    buttons: ['OK']
                });
                this.alert.present();
                break;
            case 'auth/user-not-found':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.userNotFound["title"],
                    subTitle: errorMessages.userNotFound["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/wrong-password':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.wrongPassword["title"],
                    subTitle: errorMessages.wrongPassword["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/invalid-email':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.invalidEmail["title"],
                    subTitle: errorMessages.invalidEmail["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/email-already-in-use':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.emailAlreadyInUse["title"],
                    subTitle: errorMessages.emailAlreadyInUse["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/weak-password':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.weakPassword["title"],
                    subTitle: errorMessages.weakPassword["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/requires-recent-login':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.requiresRecentLogin["title"],
                    subTitle: errorMessages.requiresRecentLogin["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/user-mismatch':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.userMismatch["title"],
                    subTitle: errorMessages.userMismatch["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/provider-already-linked':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.providerAlreadyLinked["title"],
                    subTitle: errorMessages.providerAlreadyLinked["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'auth/credential-already-in-use':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.credentialAlreadyInUse["title"],
                    subTitle: errorMessages.credentialAlreadyInUse["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            // Profile Error Messages
            case 'profile/error-change-name':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.changeName["title"],
                    subTitle: errorMessages.changeName["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/invalid-chars-name':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.invalidCharsName["title"],
                    subTitle: errorMessages.invalidCharsName["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/name-too-short':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.nameTooShort["title"],
                    subTitle: errorMessages.nameTooShort["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/error-change-email':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.changeEmail["title"],
                    subTitle: errorMessages.changeEmail["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/invalid-email':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.invalidProfileEmail["title"],
                    subTitle: errorMessages.invalidProfileEmail["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/error-change-photo':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.changePhoto["title"],
                    subTitle: errorMessages.changePhoto["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/password-too-short':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.passwordTooShort["title"],
                    subTitle: errorMessages.passwordTooShort["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/invalid-chars-password':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.invalidCharsPassword["title"],
                    subTitle: errorMessages.invalidCharsPassword["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/passwords-do-not-match':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.passwordsDoNotMatch["title"],
                    subTitle: errorMessages.passwordsDoNotMatch["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/error-update-profile':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.updateProfile["title"],
                    subTitle: errorMessages.updateProfile["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'profile/error-same-username':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.usernameExists["title"],
                    subTitle: errorMessages.usernameExists["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            //Image Error Messages
            case 'image/error-image-upload':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.imageUpload["title"],
                    subTitle: errorMessages.imageUpload["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            // Group Error MEssages
            case 'group/error-update-group':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.groupUpdate["title"],
                    subTitle: errorMessages.groupUpdate["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'group/error-leave-group':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.groupLeave["title"],
                    subTitle: errorMessages.groupLeave["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
            case 'group/error-delete-group':
                this.alert = this.alertCtrl.create({
                    title: errorMessages.groupDelete["title"],
                    subTitle: errorMessages.groupDelete["subTitle"],
                    buttons: ['OK']
                }).present();
                break;
        }
    };
    AlertProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__logout__["a" /* LogoutProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__logout__["a" /* LogoutProvider */]) === "function" && _b || Object])
    ], AlertProvider);
    return AlertProvider;
    var _a, _b;
}());

//# sourceMappingURL=alert.js.map

/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationsPage = (function () {
    function NotificationsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.notifications = [{ img: "assets/img/01.png", title: "SAVE $0.55", date: "1h" },
            { img: "assets/img/02.png", title: "SAVE $0.10", date: "3h" },
            { img: "assets/img/03.png", title: "SAVE $0.55", date: "1d" }];
    }
    NotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notifications',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\notifications\notifications.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>notifications</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-list class="main_list" no-margin>\n      <ion-item *ngFor="let item of notifications" no-lines>\n        <ion-thumbnail item-start>\n          <img src="{{item.img}}">\n        </ion-thumbnail>\n\n        <h2 class="main_title" text-uppercase>{{item.title}}<span float-right>{{item.date}}</span></h2>\n        <h2 text-uppercase>ESThe &reg;</h2>\n        <p class="short_note" no-margin>on any one (1) ESThe Iced Tea 18.5oz, 23oz or multipack</p>\n      </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\notifications\notifications.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], NotificationsPage);
    return NotificationsPage;
}());

//# sourceMappingURL=notifications.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SettingPage = (function () {
    function SettingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-setting',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\setting\setting.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>setting</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-item>\n      <ion-label>Language</ion-label>\n      <ion-select>\n          <ion-option value="english" selected>English</ion-option>\n          <ion-option value="french">French</ion-option>\n      </ion-select>\n  </ion-item>\n  <ion-list class="notif_setting" radio-group>\n      <ion-list-header no-lines>\n       Notification\n      </ion-list-header>\n      <ion-item>\n          <ion-label>Your Account<p>Get notified account alerts</p></ion-label>\n          <ion-toggle></ion-toggle>\n      </ion-item>\n      <ion-item>\n          <ion-label>Your Shipments<p>Find out When package ship & arrive</p></ion-label>\n          <ion-toggle></ion-toggle>\n      </ion-item>\n  </ion-list>\n  <ion-item  [navPush]="\'LoginPage\'">\n    Logout\n    <ion-icon color="primary" name="log-out" item-end></ion-icon>\n  </ion-item>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\setting\setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendsFilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FriendsFilterPage = (function () {
    function FriendsFilterPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.age = {
            lower: 0,
            upper: 100
        };
        this.area = '';
        this.places = [];
    }
    FriendsFilterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FriendsFilterPage');
    };
    FriendsFilterPage.prototype.search = function () {
        var _this = this;
        var textbox = document.getElementById('searchBox').getElementsByTagName('input')[0];
        var autocomplete = new google.maps.places.Autocomplete(textbox, { types: ['geocode'] });
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            // retrieve the place object for your use
            var place = autocomplete.getPlace();
            _this.area = place.formatted_address;
            console.log(_this.area);
        });
    };
    FriendsFilterPage.prototype.apply = function () {
        this.viewCtrl.dismiss({
            ageStart: this.age.lower,
            ageEnd: this.age.upper,
            location: this.area
        });
    };
    FriendsFilterPage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    FriendsFilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-friends-filter',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\friends-filter\friends-filter.html"*/'<ion-header>\n\n  <ion-navbar color="white">\n    <ion-buttons start>\n      <button ion-button (click)="cancel()">Cancel</button>\n    </ion-buttons>\n    <ion-title>Filter</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)="apply()">Apply</button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <ion-list no-lines>\n    <ion-item>\n      <ion-label stacked>Location (Area)</ion-label>\n      <ion-input type="text" id="searchBox"  [(ngModel)]="area" (input)="search($event)"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label stacked>\n        Age Range ({{age.lower}} - {{age.upper}})\n      </ion-label>\n      <ion-range [(ngModel)]="age" dualKnobs="true" min="0" max="100">\n        <ion-label range-left>0</ion-label>\n        <ion-label range-right>100</ion-label>\n      </ion-range>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\friends-filter\friends-filter.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], FriendsFilterPage);
    return FriendsFilterPage;
}());

//# sourceMappingURL=friends-filter.js.map

/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__message_message__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global__ = __webpack_require__(74);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TestPage = (function () {
    function TestPage(global, navCtrl, navParams, angularfire, loadingProvider, app, dataProvider) {
        this.global = global;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.angularfire = angularfire;
        this.loadingProvider = loadingProvider;
        this.app = app;
        this.dataProvider = dataProvider;
        this.searchFriend = '';
        this.items = global.items;
    }
    TestPage.prototype.ionViewDidLoad = function () {
        // this.loadingProvider.show();
        var _this = this;
        // Get info of conversations of current logged in user.
        this.dataProvider.getConversations().snapshotChanges().subscribe(function (conversationsInfoRes) {
            var conversations = [];
            conversations = conversationsInfoRes.map(function (c) { return (__assign({ key: c.key }, c.payload.val())); });
            console.log(conversations);
            if (conversations.length > 0) {
                conversations.forEach(function (conversation) {
                    console.log(conversation);
                    if (conversation) {
                        // Get conversation partner info.
                        _this.dataProvider.getUser(conversation.key).snapshotChanges().subscribe(function (user) {
                            conversation.friend = user.payload.val();
                            // Get conversation info.
                            _this.dataProvider.getConversation(conversation.conversationId).snapshotChanges().subscribe(function (obj) {
                                // Get last message of conversation.
                                console.log(obj.payload.val());
                                if (obj.payload.val() != null) {
                                    var lastMessage = obj.payload.val().messages[obj.payload.val().messages.length - 1];
                                    conversation.date = lastMessage.date;
                                    conversation.sender = lastMessage.sender;
                                    // Set unreadMessagesCount
                                    conversation.unreadMessagesCount = obj.payload.val().messages.length - conversation.messagesRead;
                                    console.log(obj.payload.val().messages.length + "-" + conversation.messagesRead);
                                    console.log(conversation.unreadMessagesCount);
                                    // Process last message depending on messageType.
                                    if (lastMessage.type == 'text') {
                                        if (lastMessage.sender == __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid) {
                                            conversation.message = 'You: ' + lastMessage.message;
                                        }
                                        else {
                                            conversation.message = lastMessage.message;
                                        }
                                    }
                                    else {
                                        if (lastMessage.sender == __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid) {
                                            conversation.message = 'You sent a photo message.';
                                        }
                                        else {
                                            conversation.message = 'has sent you a photo message.';
                                        }
                                    }
                                    // Add or update conversation.
                                    _this.addOrUpdateConversation(conversation);
                                }
                            });
                        });
                    }
                });
                // this.loadingProvider.hide();
            }
            else {
                _this.conversations = [];
                // this.loadingProvider.hide();
            }
        });
        // Update conversations' last active date time elapsed every minute based on Moment.js.
        var that = this;
        if (!that.updateDateTime) {
            that.updateDateTime = setInterval(function () {
                if (that.conversations) {
                    that.conversations.forEach(function (conversation) {
                        var date = conversation.date;
                        conversation.date = new Date(date);
                    });
                }
            }, 60000);
        }
    };
    // Add or update conversation for real-time sync based on our observer, sort by active date.
    TestPage.prototype.addOrUpdateConversation = function (conversation) {
        if (!this.conversations) {
            this.conversations = [conversation];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.conversations.length; i++) {
                if (this.conversations[i].key == conversation.key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.conversations[index] = conversation;
            }
            else {
                this.conversations.push(conversation);
            }
            // Sort by last active date.
            this.conversations.sort(function (a, b) {
                var date1 = new Date(a.date);
                var date2 = new Date(b.date);
                if (date1 > date2) {
                    return -1;
                }
                else if (date1 < date2) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
    };
    // Open chat with friend.
    TestPage.prototype.message = function (userId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__message_message__["a" /* MessagePage */], { userId: userId });
    };
    // Return class based if conversation has unreadMessages or not.
    TestPage.prototype.hasUnreadMessages = function (conversation) {
        if (conversation.unreadMessagesCount > 0) {
            return 'bold';
        }
        else
            return '';
    };
    TestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'test-page',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\test-page\test.html"*/'<ion-header>\n\n    <ion-navbar hideBackButton color=primary>\n\n        <button ion-button icon-only menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>בית</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="global.presentfilterModal()">\n\n                <ion-icon name="funnel"></ion-icon>\n\n            </button>\n\n\n\n            <button ion-button icon-only (click)="searchInput=!searchInput">\n\n                <ion-icon name="search"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n    <ion-toolbar>\n\n        <ion-searchbar placeholder=\'חיפוש\' no-margin></ion-searchbar>\n\n    </ion-toolbar>\n\n</ion-header>\n\n<ion-content class="background_primary">\n\n\n\n    <div class="home_banner" padding-bottom>\n\n        <img src="assets/img/InstantDealLogo2.png">\n\n    </div>\n\n    <div>\n\n\n\n        <div class="filter_data" *ngIf="global.filterresults.length">\n\n            <button color="light" (click)="global.deleteFilter(item)" *ngFor="let item of global.filterresults"\n\n                ion-button icon-right round small style="float: right">\n\n                {{item.title}}\n\n                <ion-icon name="ios-close"></ion-icon>\n\n            </button>\n\n        </div>\n\n\n\n        <ion-card *ngFor="let item of items" (click)="message(item.userId); $event.stopPropagation();">\n\n\n\n            <ion-item>\n\n                <ion-avatar item-start margin (click)="message(item.userId); $event.stopPropagation();">\n\n                    <img src={{item.logo}}>\n\n                </ion-avatar>\n\n                <h2>{{item.title}}</h2>\n\n                <p>{{item.subtitle}}</p>\n\n            </ion-item>\n\n            <br>\n\n\n\n            <img width="100%" height="100" src={{item.img}}>\n\n\n\n            <ion-card-content>\n\n                {{item.content}}\n\n            </ion-card-content>\n\n\n\n            <ion-row>\n\n                <ion-col>\n\n                    <button ion-button clear small>\n\n                        <ion-icon name=""></ion-icon>\n\n                        <div>{{item.quantity}} נשארו</div>\n\n                    </button>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <button ion-button small clear icon-left>\n\n                        <ion-icon name="text">\n\n                        </ion-icon>\n\n                        תגובות\n\n                    </button>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <button ion-button clear small icon-left>\n\n                        <ion-icon name="clock"></ion-icon>\n\n                        {{item.duration}}\n\n                    </button>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-card>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\test-page\test.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__providers_global__["a" /* global */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_3__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_4__providers_data__["a" /* DataProvider */]])
    ], TestPage);
    return TestPage;
}());

//# sourceMappingURL=test.js.map

/***/ }),

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__message_message__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MessagesPage = (function () {
    function MessagesPage(navCtrl, navParams, angularfire, loadingProvider, app, dataProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.angularfire = angularfire;
        this.loadingProvider = loadingProvider;
        this.app = app;
        this.dataProvider = dataProvider;
        this.searchFriend = '';
    }
    MessagesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadingProvider.show();
        // Get info of conversations of current logged in user.
        this.dataProvider.getConversations().snapshotChanges().subscribe(function (conversationsInfoRes) {
            var conversations = [];
            conversations = conversationsInfoRes.map(function (c) { return (__assign({ key: c.key }, c.payload.val())); });
            console.log(conversations);
            if (conversations.length > 0) {
                conversations.forEach(function (conversation) {
                    console.log(conversation);
                    if (conversation) {
                        // Get conversation partner info.
                        _this.dataProvider.getUser(conversation.key).snapshotChanges().subscribe(function (user) {
                            conversation.friend = user.payload.val();
                            // Get conversation info.
                            _this.dataProvider.getConversation(conversation.conversationId).snapshotChanges().subscribe(function (obj) {
                                // Get last message of conversation.
                                console.log(obj.payload.val());
                                if (obj.payload.val() != null) {
                                    var lastMessage = obj.payload.val().messages[obj.payload.val().messages.length - 1];
                                    conversation.date = lastMessage.date;
                                    conversation.sender = lastMessage.sender;
                                    // Set unreadMessagesCount
                                    conversation.unreadMessagesCount = obj.payload.val().messages.length - conversation.messagesRead;
                                    console.log(obj.payload.val().messages.length + "-" + conversation.messagesRead);
                                    console.log(conversation.unreadMessagesCount);
                                    // Process last message depending on messageType.
                                    if (lastMessage.type == 'text') {
                                        if (lastMessage.sender == __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid) {
                                            conversation.message = 'You: ' + lastMessage.message;
                                        }
                                        else {
                                            conversation.message = lastMessage.message;
                                        }
                                    }
                                    else {
                                        if (lastMessage.sender == __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid) {
                                            conversation.message = 'You sent a photo message.';
                                        }
                                        else {
                                            conversation.message = 'has sent you a photo message.';
                                        }
                                    }
                                    // Add or update conversation.
                                    _this.addOrUpdateConversation(conversation);
                                }
                            });
                        });
                    }
                });
                _this.loadingProvider.hide();
            }
            else {
                _this.conversations = [];
                _this.loadingProvider.hide();
            }
        });
        // Update conversations' last active date time elapsed every minute based on Moment.js.
        var that = this;
        if (!that.updateDateTime) {
            that.updateDateTime = setInterval(function () {
                if (that.conversations) {
                    that.conversations.forEach(function (conversation) {
                        var date = conversation.date;
                        conversation.date = new Date(date);
                    });
                }
            }, 60000);
        }
    };
    // Add or update conversation for real-time sync based on our observer, sort by active date.
    MessagesPage.prototype.addOrUpdateConversation = function (conversation) {
        if (!this.conversations) {
            this.conversations = [conversation];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.conversations.length; i++) {
                if (this.conversations[i].key == conversation.key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.conversations[index] = conversation;
            }
            else {
                this.conversations.push(conversation);
            }
            // Sort by last active date.
            this.conversations.sort(function (a, b) {
                var date1 = new Date(a.date);
                var date2 = new Date(b.date);
                if (date1 > date2) {
                    return -1;
                }
                else if (date1 < date2) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
    };
    // Open chat with friend.
    MessagesPage.prototype.message = function (userId) {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_5__message_message__["a" /* MessagePage */], { userId: userId });
        console.log(userId);
        console.log(typeof (userId));
    };
    // Return class based if conversation has unreadMessages or not.
    MessagesPage.prototype.hasUnreadMessages = function (conversation) {
        if (conversation.unreadMessagesCount > 0) {
            return 'bold';
        }
        else
            return '';
    };
    MessagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-messages',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\messages\messages.html"*/'<ion-header>\n  <ion-navbar color="white">\n    <ion-title>Recent Chats</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <!-- No conversations to show -->\n  <div class="empty-list" *ngIf="conversations && conversations.length <= 0">\n    <h1><ion-icon name="text"></ion-icon></h1>\n    <p>No new conversation yet.</p>\n  </div>\n  <!-- Show conversations -->\n  <ion-list class="avatar-list" *ngIf="conversations && conversations.length > 0">\n    <ion-searchbar [(ngModel)]="searchFriend" placeholder="Search for friend or username" showCancelButton="true" cancelButtonText="Done"></ion-searchbar>\n    <div *ngFor="let conversation of conversations | conversationFilter:searchFriend">\n    <ion-item  *ngIf="conversation.blocked != true" no-lines tappable (click)="message(conversation.key)">\n      \n      <ion-avatar item-left *ngIf="conversation.friend">\n        <img src="{{conversation.friend.img}}" onError="this.src=\'./assets/images/default-dp.png\'">\n      </ion-avatar>\n      <div [ngClass]=hasUnreadMessages(conversation)>\n        <h2 *ngIf="conversation.friend">{{conversation.friend.name}}</h2>\n        <ion-badge color="danger" *ngIf="conversation.unreadMessagesCount > 0">{{conversation.unreadMessagesCount}}</ion-badge>\n        <p>{{conversation.message}}</p>\n        <span>{{conversation.date | DateFormat}}</span>\n      </div>\n    \n    </ion-item>\n    </div>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\messages\messages.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_3__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_4__providers_data__["a" /* DataProvider */]])
    ], MessagesPage);
    return MessagesPage;
}());

//# sourceMappingURL=messages.js.map

/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__group_group__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__new_group_new_group__ = __webpack_require__(341);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GroupsPage = (function () {
    // GroupsPage
    // This is the page where the user can add, view and search for groups.
    function GroupsPage(navCtrl, navParams, app, dataProvider, loadingProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.dataProvider = dataProvider;
        this.loadingProvider = loadingProvider;
    }
    GroupsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // Initialize
        this.searchGroup = '';
        this.loadingProvider.show();
        // Get groups
        this.dataProvider.getGroups().snapshotChanges().subscribe(function (groupIdsRes) {
            var groupIds = [];
            groupIds = groupIdsRes.map(function (c) { return (__assign({ key: c.key }, c.payload.val())); });
            console.log(groupIds);
            if (groupIds.length > 0) {
                if (_this.groups && _this.groups.length > groupIds.length) {
                    // User left/deleted a group, clear the list and add or update each group again.
                    _this.groups = [];
                }
                groupIds.forEach(function (groupId) {
                    console.log(groupId);
                    _this.dataProvider.getGroup(groupId.key).snapshotChanges().subscribe(function (groupRes) {
                        var group = __assign({ key: groupRes.key }, groupRes.payload.val());
                        console.log(group);
                        if (group.key != null) {
                            // Get group's unreadMessagesCount
                            group.unreadMessagesCount = group.messages.length - groupId.messagesRead;
                            // Get group's last active date
                            group.date = group.messages[group.messages.length - 1].date;
                            _this.addOrUpdateGroup(group);
                        }
                    });
                });
                _this.loadingProvider.hide();
            }
            else {
                _this.groups = [];
                _this.loadingProvider.hide();
            }
        });
        // Update groups' last active date time elapsed every minute based on Moment.js.
        var that = this;
        if (!that.updateDateTime) {
            that.updateDateTime = setInterval(function () {
                if (that.groups) {
                    that.groups.forEach(function (group) {
                        var date = group.date;
                        group.date = new Date(date);
                    });
                }
            }, 60000);
        }
    };
    // Add or update group for real-time sync based on our observer.
    GroupsPage.prototype.addOrUpdateGroup = function (group) {
        if (!this.groups) {
            this.groups = [group];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.groups.length; i++) {
                if (this.groups[i].key == group.key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.groups[index] = group;
            }
            else {
                this.groups.push(group);
            }
        }
    };
    // New Group.
    GroupsPage.prototype.newGroup = function () {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_5__new_group_new_group__["a" /* NewGroupPage */]);
    };
    // Open Group Chat.
    GroupsPage.prototype.viewGroup = function (groupId) {
        console.log(groupId);
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_4__group_group__["a" /* GroupPage */], { groupId: groupId });
    };
    // Return class based if group has unreadMessages or not.
    GroupsPage.prototype.hasUnreadMessages = function (group) {
        if (group.unreadMessagesCount > 0) {
            return 'group bold';
        }
        else
            return 'group';
    };
    GroupsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groups',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\groups\groups.html"*/'<ion-header>\n  <ion-navbar color="white">\n    <ion-title>Groups</ion-title>\n    <ion-buttons end>\n      <button ion-button color="primary" (click)="newGroup()">Create</button>\n  </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <!-- No groups to show -->\n  <div class="empty-list" *ngIf="groups && groups.length <= 0">\n    <h1><ion-icon name="md-chatbubbles"></ion-icon></h1>\n    <p>No new groups yet.</p>\n  </div>\n  <!-- Show groups -->\n  <div *ngIf="groups && groups.length > 0">\n    <ion-searchbar [(ngModel)]="searchGroup" placeholder="Search for group" showCancelButton="true" cancelButtonText="Done"></ion-searchbar>\n    <ion-list no-lines>\n      <ion-item *ngFor="let group of groups | groupFilter: searchGroup" (click)="viewGroup(group.key)">\n        <ion-thumbnail item-start>\n          <img src="{{group.img}}" onError="this.src=\'./assets/images/default-group.png\'">\n        </ion-thumbnail>\n        <h2>{{group.name}}</h2>\n        <p>{{group.date | DateFormat}}</p>\n        <ion-badge item-right color="danger" *ngIf="group.unreadMessagesCount > 0">{{group.unreadMessagesCount}}</ion-badge>\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\groups\groups.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_2__providers_data__["a" /* DataProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_loading__["a" /* LoadingProvider */]])
    ], GroupsPage);
    return GroupsPage;
}());

//# sourceMappingURL=groups.js.map

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_image__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_alert__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_members_add_members__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__user_info_user_info__ = __webpack_require__(94);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var GroupInfoPage = (function () {
    // GroupInfoPage
    // This is the page where the user can view group information, change group information, add members, and leave/delete group.
    function GroupInfoPage(navCtrl, navParams, dataProvider, loadingProvider, modalCtrl, alertCtrl, alertProvider, angularfire, imageProvider, camera) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataProvider = dataProvider;
        this.loadingProvider = loadingProvider;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.alertProvider = alertProvider;
        this.angularfire = angularfire;
        this.imageProvider = imageProvider;
        this.camera = camera;
    }
    GroupInfoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // Initialize
        this.groupId = this.navParams.get('groupId');
        console.log(this.groupId);
        // Get group details.
        this.subscription = this.dataProvider.getGroup(this.groupId).snapshotChanges().subscribe(function (groupRes) {
            var group = __assign({ $key: groupRes.key }, groupRes.payload.val());
            console.log(group);
            if (group != null) {
                _this.loadingProvider.show();
                _this.group = group;
                if (group.members) {
                    group.members.forEach(function (memberId) {
                        _this.dataProvider.getUser(memberId).snapshotChanges().subscribe(function (member) {
                            if (member.key != null) {
                                member = __assign({ $key: member.key }, member.payload.val());
                                _this.addUpdateOrRemoveMember(member);
                            }
                        });
                    });
                }
                _this.loadingProvider.hide();
            }
            else {
                // Group is deleted, go back.
                _this.navCtrl.popToRoot();
            }
        });
        // Get user details.
        this.dataProvider.getCurrentUser().snapshotChanges().subscribe(function (user) {
            _this.user = __assign({ $key: user.key }, user.payload.val());
        });
    };
    // Delete subscription.
    // ionViewDidLeave() {
    //   if(this.deleteSubscription)
    //
    // }
    // Check if user exists in the group then add/update user.
    // If the user has already left the group, remove user from the list.
    GroupInfoPage.prototype.addUpdateOrRemoveMember = function (member) {
        console.log(member);
        if (this.group) {
            if (this.group.members.indexOf(member.$key) > -1) {
                // User exists in the group.
                if (!this.groupMembers) {
                    this.groupMembers = [member];
                }
                else {
                    var index = -1;
                    for (var i = 0; i < this.groupMembers.length; i++) {
                        if (this.groupMembers[i].$key == member.$key) {
                            index = i;
                        }
                    }
                    // Add/Update User.
                    if (index > -1) {
                        this.groupMembers[index] = member;
                    }
                    else {
                        this.groupMembers.push(member);
                    }
                }
            }
            else {
                // User already left the group, remove member from list.
                var index1 = -1;
                for (var j = 0; j < this.groupMembers.length; j++) {
                    if (this.groupMembers[j].$key == member.$key) {
                        index1 = j;
                    }
                }
                if (index1 > -1) {
                    this.groupMembers.splice(index1, 1);
                }
            }
        }
    };
    // View user info.
    GroupInfoPage.prototype.viewUser = function (userId) {
        if (__WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid != userId)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__user_info_user_info__["a" /* UserInfoPage */], { userId: userId });
    };
    // Enlarge group image.
    GroupInfoPage.prototype.enlargeImage = function (img) {
        var imageModal = this.modalCtrl.create("ImageModalPage", { img: img });
        imageModal.present();
    };
    // Change group name.
    GroupInfoPage.prototype.setName = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Change Group Name',
            message: "Please enter a new group name.",
            inputs: [
                {
                    name: 'name',
                    placeholder: 'Group Name',
                    value: this.group.name
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        var name = data["name"];
                        if (_this.group.name != name) {
                            _this.loadingProvider.show();
                            // Add system message.
                            _this.group.messages.push({
                                date: new Date().toString(),
                                sender: _this.user.$key,
                                type: 'system',
                                message: _this.user.name + ' has changed the group name to: ' + name + '.',
                                icon: 'md-create'
                            });
                            // Update group on database.
                            _this.dataProvider.getGroup(_this.groupId).update({
                                name: name,
                                messages: _this.group.messages
                            }).then(function (success) {
                                _this.loadingProvider.hide();
                                _this.alertProvider.showGroupUpdatedMessage();
                            }).catch(function (error) {
                                _this.loadingProvider.hide();
                                _this.alertProvider.showErrorMessage('group/error-update-group');
                            });
                        }
                    }
                }
            ]
        }).present();
    };
    // Change group image, the user is asked if they want to take a photo or choose from gallery.
    GroupInfoPage.prototype.setPhoto = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Set Group Photo',
            message: 'Do you want to take a photo or choose from your photo gallery?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Choose from Gallery',
                    handler: function () {
                        _this.loadingProvider.show();
                        // Upload photo and set to group photo, afterwards, return the group object as promise.
                        _this.imageProvider.setGroupPhotoPromise(_this.group, _this.camera.PictureSourceType.PHOTOLIBRARY).then(function (group) {
                            // Add system message.
                            _this.group.messages.push({
                                date: new Date().toString(),
                                sender: _this.user.$key,
                                type: 'system',
                                message: _this.user.name + ' has changed the group photo.',
                                icon: 'ios-camera'
                            });
                            // Update group image on database.
                            _this.dataProvider.getGroup(_this.groupId).update({
                                img: group.img,
                                messages: _this.group.messages
                            }).then(function (success) {
                                _this.loadingProvider.hide();
                                _this.alertProvider.showGroupUpdatedMessage();
                            }).catch(function (error) {
                                _this.loadingProvider.hide();
                                _this.alertProvider.showErrorMessage('group/error-update-group');
                            });
                        });
                    }
                },
                {
                    text: 'Take Photo',
                    handler: function () {
                        _this.loadingProvider.show();
                        // Upload photo and set to group photo, afterwwards, return the group object as promise.
                        _this.imageProvider.setGroupPhotoPromise(_this.group, _this.camera.PictureSourceType.CAMERA).then(function (group) {
                            // Add system message.
                            _this.group.messages.push({
                                date: new Date().toString(),
                                sender: _this.user.$key,
                                type: 'system',
                                message: _this.user.name + ' has changed the group photo.',
                                icon: 'ios-camera'
                            });
                            // Update group image on database.
                            _this.dataProvider.getGroup(_this.groupId).update({
                                img: group.img,
                                messages: _this.group.messages
                            }).then(function (success) {
                                _this.loadingProvider.hide();
                                _this.alertProvider.showGroupUpdatedMessage();
                            }).catch(function (error) {
                                _this.loadingProvider.hide();
                                _this.alertProvider.showErrorMessage('group/error-update-group');
                            });
                        });
                    }
                }
            ]
        }).present();
    };
    // Change group description.
    GroupInfoPage.prototype.setDescription = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Change Group Description',
            message: "Please enter a new group description.",
            inputs: [
                {
                    name: 'description',
                    placeholder: 'Group Description',
                    value: this.group.description
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        var description = data["description"];
                        if (_this.group.description != description) {
                            _this.loadingProvider.show();
                            // Add system message.
                            _this.group.messages.push({
                                date: new Date().toString(),
                                sender: _this.user.$key,
                                type: 'system',
                                message: _this.user.name + ' has changed the group description.',
                                icon: 'md-clipboard'
                            });
                            // Update group on database.
                            _this.dataProvider.getGroup(_this.groupId).update({
                                description: description,
                                messages: _this.group.messages
                            }).then(function (success) {
                                _this.loadingProvider.hide();
                                _this.alertProvider.showGroupUpdatedMessage();
                            }).catch(function (error) {
                                _this.loadingProvider.hide();
                                _this.alertProvider.showErrorMessage('group/error-update-group');
                            });
                        }
                    }
                }
            ]
        }).present();
    };
    // Leave group.
    GroupInfoPage.prototype.leaveGroup = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Confirm Leave',
            message: 'Are you sure you want to leave this group?',
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Leave',
                    handler: function (data) {
                        _this.loadingProvider.show();
                        // Remove member from group.
                        _this.group.members.splice(_this.group.members.indexOf(_this.user.$key), 1);
                        // Add system message.
                        _this.group.messages.push({
                            date: new Date().toString(),
                            sender: _this.user.$key,
                            type: 'system',
                            message: _this.user.name + ' has left this group.',
                            icon: 'md-log-out'
                        });
                        // Update group on database.
                        _this.dataProvider.getGroup(_this.groupId).update({
                            members: _this.group.members,
                            messages: _this.group.messages
                        }).then(function (success) {
                            // Remove group from user's group list.
                            _this.angularfire.object('/accounts/' + __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid + '/groups/' + _this.groupId).remove().then(function () {
                                // Pop this view because user already has left this group.
                                _this.group = null;
                                setTimeout(function () {
                                    _this.loadingProvider.hide();
                                    _this.navCtrl.popToRoot();
                                }, 300);
                            });
                        }).catch(function (error) {
                            _this.alertProvider.showErrorMessage('group/error-leave-group');
                        });
                    }
                }
            ]
        }).present();
    };
    // Delete group.
    GroupInfoPage.prototype.deleteGroup = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to delete this group?',
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Delete',
                    handler: function (data) {
                        var group = JSON.parse(JSON.stringify(_this.group));
                        console.log(group);
                        // Delete all images of image messages.
                        group.messages.forEach(function (message) {
                            if (message.type == 'image') {
                                console.log("Delete: " + message.url + " of " + group.$key);
                                _this.imageProvider.deleteGroupImageFile(group.$key, message.url);
                            }
                        });
                        _this.angularfire.object('/accounts/' + __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid + '/groups/' + group.$key).remove().then(function () {
                            _this.dataProvider.getGroup(group.$key).remove();
                        });
                        // Delete group image.
                        console.log("Delete: " + group.img);
                        _this.imageProvider.deleteImageFile(group.img);
                        _this.navCtrl.popToRoot();
                    }
                }
            ]
        }).present();
    };
    // Add members.
    GroupInfoPage.prototype.addMembers = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__add_members_add_members__["a" /* AddMembersPage */], { groupId: this.groupId });
    };
    GroupInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-group-info',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\group-info\group-info.html"*/'<ion-header>\n  <ion-navbar color="white">\n    <ion-title>Group Info</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <!-- Group Info -->\n  <div *ngIf="group">\n    <ion-row  style="background:#f3f3f3" padding>\n      <ion-col col-md-8>\n        <h4 tappable (click)="setName()">{{group.name}}</h4>\n        <p tappable style="color:#aaa" (click)="setDescription()">{{group.description}}</p>\n      </ion-col>\n      <ion-col col-md-4 class="center">\n          <img src="{{group.img}}" style="border-radius: 100%; width: 80px; height: 80px;" tappable (click)="setPhoto()" onError="this.src=\'./assets/images/default-group.png\'">\n      </ion-col>\n    </ion-row>\n    <ion-list *ngIf="groupMembers">\n      <ion-list-header>\n        Group Members ({{groupMembers.length}})\n      </ion-list-header>\n      <ion-item (click)="addMembers()">\n        <ion-icon name="add" item-left></ion-icon>\n        <h2>Add Members</h2>\n      </ion-item>\n      <ion-item *ngFor="let member of groupMembers" (click)="viewUser(member.$key)">\n        <ion-avatar item-left>\n          <img src="{{member.img}}" onError="this.src=\'./assets/images/default-dp.png\'"/>\n        </ion-avatar>\n        <h2>{{member.name}}</h2>\n        <p>{{member.description}}</p>\n      </ion-item>\n    </ion-list>\n    <ion-list-header>\n        More\n    </ion-list-header>\n    <ion-list style="text-align: center;">  \n      <ion-item no-lines tappable (click)="leaveGroup()" *ngIf="groupMembers && groupMembers.length > 1">\n        Leave Group\n      </ion-item>\n      <!-- When there\'s only one member left, allow deleting of group. -->\n      <ion-item color="danger" no-lines tappable (click)="deleteGroup()" *ngIf="groupMembers && groupMembers.length <= 1">\n        Delete Group\n      </ion-item>\n    </ion-list>\n    <p padding style="color:#aaa">Started {{group.dateCreated | DateFormat}}</p>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\group-info\group-info.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_data__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_alert__["a" /* AlertProvider */], __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_4__providers_image__["a" /* ImageProvider */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */]])
    ], GroupInfoPage);
    return GroupInfoPage;
}());

//# sourceMappingURL=group-info.js.map

/***/ }),

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddMembersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_alert__ = __webpack_require__(33);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddMembersPage = (function () {
    // AddMemberPage
    // This is the page where the user can add their friends to an existing group.
    // The user can only add their friends to the group.
    function AddMembersPage(navCtrl, navParams, dataProvider, loadingProvider, angularfire, alertCtrl, alertProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataProvider = dataProvider;
        this.loadingProvider = loadingProvider;
        this.angularfire = angularfire;
        this.alertCtrl = alertCtrl;
        this.alertProvider = alertProvider;
    }
    AddMembersPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // Initialize
        this.groupId = this.navParams.get('groupId');
        this.searchFriend = '';
        this.toAdd = [];
        this.loadingProvider.show();
        // Get user information for system message sent to the group when a member was added.
        this.dataProvider.getCurrentUser().snapshotChanges().subscribe(function (user) {
            _this.user = user.payload.val();
        });
        // Get group information
        this.dataProvider.getGroup(this.groupId).snapshotChanges().subscribe(function (group) {
            _this.group = group.payload.val();
            _this.groupMembers = null;
            // Get group members
            if (group.payload.val().members) {
                group.payload.val().members.forEach(function (memberId) {
                    _this.dataProvider.getUser(memberId).snapshotChanges().subscribe(function (member) {
                        _this.addOrUpdateMember(member);
                    });
                });
                // Get user's friends to add
                _this.dataProvider.getCurrentUser().snapshotChanges().subscribe(function (account) {
                    if (account.payload.val().friends) {
                        for (var i = 0; i < account.payload.val().friends.length; i++) {
                            _this.dataProvider.getUser(account.payload.val().friends[i]).snapshotChanges().subscribe(function (friendRes) {
                                // Only friends that are not yet a member of this group can be added.
                                var friend = __assign({ $key: friendRes.key }, friendRes.payload.val());
                                console.log(friend);
                                if (!_this.isMember(friend))
                                    _this.addOrUpdateFriend(friend);
                            });
                        }
                        if (!_this.friends) {
                            _this.friends = [];
                        }
                    }
                    else {
                        _this.friends = [];
                    }
                });
            }
            console.log(_this.friends);
            _this.loadingProvider.hide();
        });
    };
    // Check if friend is a member of the group or not.
    AddMembersPage.prototype.isMember = function (friend) {
        if (this.groupMembers) {
            for (var i = 0; i < this.groupMembers.length; i++) {
                if (this.groupMembers[i].$key == friend.$key) {
                    return true;
                }
            }
        }
        return false;
    };
    // Check if friend is already on the list of members to be added.
    AddMembersPage.prototype.isAdded = function (friend) {
        if (this.toAdd) {
            for (var i = 0; i < this.toAdd.length; i++) {
                if (this.toAdd[i].$key == friend.$key) {
                    return true;
                }
            }
        }
        return false;
    };
    // Toggle for adding/removing friend on the list of members to be added.
    AddMembersPage.prototype.addOrRemove = function (friend) {
        if (this.isAdded(friend)) {
            this.remove(friend);
        }
        else {
            this.add(friend);
        }
    };
    // Add or update friend information for real-time sync.
    AddMembersPage.prototype.addOrUpdateFriend = function (friend) {
        if (!this.friends) {
            this.friends = [friend];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.friends.length; i++) {
                if (this.friends[i].$key == friend.$key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.friends[index] = friend;
            }
            else {
                this.friends.push(friend);
            }
        }
    };
    // Add or update member information for real-time sync.
    AddMembersPage.prototype.addOrUpdateMember = function (member) {
        if (!this.groupMembers) {
            this.groupMembers = [member];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.groupMembers.length; i++) {
                if (this.groupMembers[i].$key == member.$key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.groupMembers[index] = member;
            }
            else {
                this.groupMembers.push(member);
            }
        }
    };
    // Add friend to the list of to be added.
    AddMembersPage.prototype.add = function (friend) {
        this.toAdd.push(friend);
    };
    // Remove friend from the list of to be added.
    AddMembersPage.prototype.remove = function (friend) {
        this.toAdd.splice(this.toAdd.indexOf(friend), 1);
    };
    // Back
    AddMembersPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    // Get names of the members to be added to the group.
    AddMembersPage.prototype.getNames = function () {
        var names = '';
        this.toAdd.forEach(function (friend) {
            names += friend.name + ', ';
        });
        return names.substring(0, names.length - 2);
    };
    // Confirm adding of new members, afterwards add the members.
    AddMembersPage.prototype.done = function () {
        var _this = this;
        this.alertCtrl.create({
            title: 'Add Members',
            message: 'Are you sure you want to add <b>' + this.getNames() + '</b> to the group?',
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Add',
                    handler: function (data) {
                        // Proceed
                        _this.loadingProvider.show();
                        _this.toAdd.forEach(function (friend) {
                            // Add groupInfo to each friend added to the group.
                            _this.angularfire.object('/accounts/' + friend.$key + '/groups/' + _this.groupId).update({
                                messagesRead: 0
                            });
                            // Add friend as members of the group.
                            console.log(friend.$key);
                            console.log(_this.group.members);
                            _this.group.members.push(friend.$key);
                            // Add system message that the members are added to the group.
                            _this.group.messages.push({
                                date: new Date().toString(),
                                sender: _this.user.userId,
                                type: 'system',
                                message: _this.user.name + ' has added ' + _this.getNames() + ' to the group.',
                                icon: 'md-contacts'
                            });
                        });
                        // Update group data on the database.
                        _this.dataProvider.getGroup(_this.groupId).update({
                            members: _this.group.members,
                            messages: _this.group.messages
                        }).then(function () {
                            // Back.
                            _this.loadingProvider.hide();
                            _this.navCtrl.pop();
                        });
                    }
                }
            ]
        }).present();
    };
    AddMembersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-members',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\add-members\add-members.html"*/'<ion-header>\n  <ion-navbar color="white">\n    <ion-title>Add Members</ion-title>\n    <!-- Only enable button when user is adding atleast one member to the group -->\n    <ion-buttons end>\n      <button ion-button tappable (click)="done()" [disabled]="toAdd && toAdd.length < 1">Done</button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <!-- All friends already in the group. -->\n  <div class="empty-list" *ngIf="friends && friends.length == 0">\n    <h1><ion-icon name="md-contacts"></ion-icon></h1>\n    <p>Uh-oh! Sorry but your friends are already a member of this group.</p>\n    <button ion-button icon-left tappable (click)="back()"><ion-icon name="md-arrow-round-back"></ion-icon>Go Back</button>\n  </div>\n  <!-- Add/Cancel Add friends to the group. -->\n  <ion-list class="avatar-list" *ngIf="friends && friends.length > 0">\n    <ion-searchbar [(ngModel)]="searchFriend" placeholder="Search for friend or username" showCancelButton="true" cancelButtonText="Done"></ion-searchbar>\n    <ion-item *ngFor="let friend of friends | friendFilter:searchFriend" no-lines tappable (click)="addOrRemove(friend)">\n     \n      <ion-avatar item-left>\n        <img src="{{friend.img}}" onError="this.src=\'./assets/images/default-dp.png\'">\n      </ion-avatar>\n      <h2>{{friend.name}}</h2>\n      <p>@{{friend.username}}</p>\n      \n      <button item-right ion-button outline color="acceptrequest" tappable (click)="add(friend); $event.stopPropagation();" *ngIf="!isAdded(friend)">Add</button>\n      <button item-right ion-button outline color="cancelrequest" tappable (click)="remove(friend); $event.stopPropagation();" *ngIf="isAdded(friend)">Remove</button>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\add-members\add-members.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_data__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_alert__["a" /* AlertProvider */]])
    ], AddMembersPage);
    return AddMembersPage;
}());

//# sourceMappingURL=add-members.js.map

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewGroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_image__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_alert__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__validator__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__group_group__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_firebase__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var NewGroupPage = (function () {
    // NewGroupPage
    // This is the page where the user can start a new group chat with their friends.
    function NewGroupPage(navCtrl, navParams, imageProvider, dataProvider, formBuilder, alertProvider, alertCtrl, angularfire, app, loadingProvider, camera) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imageProvider = imageProvider;
        this.dataProvider = dataProvider;
        this.formBuilder = formBuilder;
        this.alertProvider = alertProvider;
        this.alertCtrl = alertCtrl;
        this.angularfire = angularfire;
        this.app = app;
        this.loadingProvider = loadingProvider;
        this.camera = camera;
        // Create our groupForm based on Validator.ts
        this.groupForm = formBuilder.group({
            name: __WEBPACK_IMPORTED_MODULE_7__validator__["a" /* Validator */].groupNameValidator,
            description: __WEBPACK_IMPORTED_MODULE_7__validator__["a" /* Validator */].groupDescriptionValidator
        });
    }
    NewGroupPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // Initialize
        this.group = {
            img: './assets/images/default-group.png'
        };
        this.searchFriend = '';
        // Get user's friends to add to the group.
        this.dataProvider.getCurrentUser().snapshotChanges().subscribe(function (accountRes) {
            var account = __assign({ $key: accountRes.key }, accountRes.payload.val());
            if (!_this.groupMembers) {
                _this.groupMembers = [account];
            }
            if (account.friends) {
                for (var i = 0; i < account.friends.length; i++) {
                    _this.dataProvider.getUser(account.friends[i]).snapshotChanges().subscribe(function (friendRes) {
                        if (friendRes.key != null) {
                            var friend = __assign({ $key: friendRes.key }, friendRes.payload.val());
                            _this.addOrUpdateFriend(friend);
                        }
                    });
                }
            }
            else {
                _this.friends = [];
            }
        });
    };
    // Add or update friend for real-time sync.
    NewGroupPage.prototype.addOrUpdateFriend = function (friend) {
        if (!this.friends) {
            this.friends = [friend];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.friends.length; i++) {
                if (this.friends[i].$key == friend.$key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.friends[index] = friend;
            }
            else {
                this.friends.push(friend);
            }
        }
    };
    // Back
    NewGroupPage.prototype.back = function () {
        if (this.group)
            this.imageProvider.deleteImageFile(this.group.img);
        this.navCtrl.pop();
    };
    // Proceed with group creation.
    NewGroupPage.prototype.done = function () {
        var _this = this;
        this.loadingProvider.show();
        var messages = [];
        // Add system message that group is created.
        messages.push({
            date: new Date().toString(),
            sender: __WEBPACK_IMPORTED_MODULE_11_firebase__["auth"]().currentUser.uid,
            type: 'system',
            message: 'This group has been created.',
            icon: 'md-chatbubbles'
        });
        // Add members of the group.
        var members = [];
        for (var i = 0; i < this.groupMembers.length; i++) {
            members.push(this.groupMembers[i].$key);
        }
        // Add group info and date.
        this.group.dateCreated = new Date().toString();
        this.group.messages = messages;
        this.group.members = members;
        this.group.name = this.groupForm.value["name"];
        this.group.description = this.groupForm.value["description"];
        // Add group to database.
        this.angularfire.list('groups').push(this.group).then(function (success) {
            var groupId = success.key;
            // Add group reference to users.
            _this.angularfire.object('/accounts/' + _this.groupMembers[0].$key + '/groups/' + groupId).update({
                messagesRead: 1
            });
            for (var i = 1; i < _this.groupMembers.length; i++) {
                _this.angularfire.object('/accounts/' + _this.groupMembers[i].$key + '/groups/' + groupId).update({
                    messagesRead: 0
                });
            }
            // Open the group chat of the just created group.
            _this.navCtrl.popToRoot().then(function () {
                _this.loadingProvider.hide();
                _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_10__group_group__["a" /* GroupPage */], { groupId: groupId });
            });
        });
    };
    // Add friend to members of group.
    NewGroupPage.prototype.addToGroup = function (friend) {
        this.groupMembers.push(friend);
    };
    // Remove friend from members of group.
    NewGroupPage.prototype.removeFromGroup = function (friend) {
        var index = -1;
        for (var i = 1; i < this.groupMembers.length; i++) {
            if (this.groupMembers[i].$key == friend.$key) {
                index = i;
            }
        }
        if (index > -1) {
            this.groupMembers.splice(index, 1);
        }
    };
    // Check if friend is already added to the group or not.
    NewGroupPage.prototype.inGroup = function (friend) {
        for (var i = 0; i < this.groupMembers.length; i++) {
            if (this.groupMembers[i].$key == friend.$key) {
                return true;
            }
        }
        return false;
    };
    // Toggle to add/remove friend from the group.
    NewGroupPage.prototype.addOrRemoveFromGroup = function (friend) {
        if (this.inGroup(friend)) {
            this.removeFromGroup(friend);
        }
        else {
            this.addToGroup(friend);
        }
    };
    // Set group photo.
    NewGroupPage.prototype.setGroupPhoto = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Set Group Photo',
            message: 'Do you want to take a photo or choose from your photo gallery?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Choose from Gallery',
                    handler: function () {
                        _this.imageProvider.setGroupPhoto(_this.group, _this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Take Photo',
                    handler: function () {
                        _this.imageProvider.setGroupPhoto(_this.group, _this.camera.PictureSourceType.CAMERA);
                    }
                }
            ]
        }).present();
    };
    NewGroupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-new-group',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\new-group\new-group.html"*/'<ion-header>\n  <ion-navbar color="white">\n    <ion-title>New Group</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable (click)="done()" [disabled]="!groupForm.valid || group.img == \'\' || groupMembers.length <= 1">Done</button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div *ngIf="group">\n      <form [formGroup]="groupForm">\n      \n      <ion-row style="background: #f3f3f3">\n        <ion-col col-3 style="text-align:center; margin-top: 4rem;">\n          <img src="{{group.img}}" *ngIf="group.img != \'\'" tappable (click)="setGroupPhoto()" onError="this.src=\'./assets/images/default-group.png\'" />\n          <img style="border-radius: 100%; width: 100px; height: 100px;" src="{{group.img}}" onError="this.src=\'./assets/images/default-group.png\'" *ngIf="group.img == \'\'" tappable (click)="setGroupPhoto()" />\n        </ion-col>\n        <ion-col col-9>\n          <ion-list no-lines style="margin:0;">\n            <ion-item>\n              <ion-label stacked>Group Name</ion-label>\n              <ion-input type="text" formControlName="name" placeholder="Name of Group"></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-label stacked>Description</ion-label>\n              <ion-textarea rows="3" formControlName="description" placeholder="Describe this Group"></ion-textarea>\n            </ion-item>\n          </ion-list>\n        </ion-col>\n      </ion-row>\n      <div *ngIf="groupMembers" >\n        <ion-list-header>\n          Group Members ({{groupMembers.length}})\n        </ion-list-header>\n        <ion-list no-lines>\n        <ion-item  *ngFor="let member of groupMembers">\n          <ion-avatar item-left>\n            <img src="{{member.img}}" onError="this.src=\'./assets/images/default-dp.png\'"/>\n          </ion-avatar>\n          <h2>{{member.name}}</h2>\n          <button color="cancelrequest" ion-button outline item-right (click)="removeFromGroup(member)">Remove</button>\n        </ion-item>\n      </ion-list>\n      </div>\n      </form>\n\n    <ion-list-header>\n      Add New Members\n    </ion-list-header>\n    <div class="form">\n      <!-- No friends to create a group. -->\n      <div class="empty" *ngIf="friends && friends.length == 0">\n        <p>You have no friends right now to start a group conversation.</p>\n      </div>\n      <!-- Show friends to add/remove to group. -->\n      <ion-list class="avatar-list" *ngIf="friends && friends.length > 0">\n        <ion-searchbar [(ngModel)]="searchFriend" placeholder="Search for friend or username" showCancelButton="true" cancelButtonText="Done"></ion-searchbar>\n        <ion-item *ngFor="let friend of friends | friendFilter:searchFriend" no-lines tappable (click)="addOrRemoveFromGroup(friend)">\n          \n          <button ion-button outline color="acceptrequest" item-right tappable (click)="addToGroup(friend); $event.stopPropagation();" *ngIf="!inGroup(friend)">Add</button>\n          <button ion-button outline color="cancelrequest" item-right tappable (click)="removeFromGroup(friend); $event.stopPropagation();" *ngIf="inGroup(friend)">Remove</button>\n          \n          <ion-avatar item-left>\n            <img src="{{friend.img}}" onError="this.src=\'./assets/images/default-dp.png\'">\n          </ion-avatar>\n          <h2>{{friend.name}}</h2>\n          <p>@{{friend.username}}</p>\n        </ion-item>\n      </ion-list>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\new-group\new-group.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_image__["a" /* ImageProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_data__["a" /* DataProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_6__providers_alert__["a" /* AlertProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_9_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_4__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */]])
    ], NewGroupPage);
    return NewGroupPage;
}());

//# sourceMappingURL=new-group.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__ = __webpack_require__(625);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DetailsPage = (function () {
    function DetailsPage(navCtrl, navParams, global, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.global = global;
        this.socialSharing = socialSharing;
        this.comments = [{ img: "assets/img/person.png", name: "WendyVerdades", date: "3 days ago" },
            { img: "assets/img/person2.png", name: "john wick", date: "1 hour" }];
    }
    DetailsPage.prototype.shareSheetShare = function () {
        this.socialSharing.share("Share message", "Share subject", "URL to file or image", "A URL to share").then(function () {
            console.log("shareSheetShare: Success");
        }).catch(function () {
            console.error("shareSheetShare: failed");
        });
    };
    DetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-details',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\details\details.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>details</ion-title>\n     <ion-buttons end>\n        <button ion-button icon-only>\n            <ion-icon name="logo-facebook"></ion-icon>\n        </button>\n        <button (click)="shareSheetShare()" ion-button icon-only>\n          <ion-icon name="share"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <!-- details card -->\n  <ion-card margin-bottom>\n    <div class="card_img">\n      <img src="assets/img/details.png"/>\n      <button class="float_btn" color="primary" [navPush]="\'CouponsPage\'" ion-button icon-only no-margin>\n          <ion-icon name="add"></ion-icon>\n      </button>\n    </div>\n    <ion-card-content>\n      <ion-card-title>\n        SAVE $0.55<span text-uppercase>ESThe &reg;</span>\n      </ion-card-title>\n      <p>on any one (1) ESThe Iced Tea 18.5oz, 23oz or multipack</p>\n      <p><span>00.10$</span>free shipping at amazon</p>\n      <button ion-button color="primary" (click)="global.getCode($event)" text-capitalize no-margin>\n        <span *ngIf="!global.codenumber">Coupon Code</span>\n        <span *ngIf="global.codenumber">0486Fd25</span>\n      </button>\n    </ion-card-content>\n  </ion-card>\n  \n  <!-- comments -->\n  <div  class="comments">\n    <ion-list no-margin>\n      <ion-list-header>\n      Comment (2)\n      </ion-list-header>\n      <ion-item  class="comment_item" *ngFor="let item of comments"  no-padding>\n        <ion-avatar item-start>\n          <img src="{{item.img}}">\n        </ion-avatar>\n        <h2>{{item.name}}<span>{{item.date}}</span></h2>\n        <p>Order quickly before price change</p>\n      </ion-item>\n    </ion-list>\n    <ion-textarea rows="1" placeholder="write comment" margin-top></ion-textarea>\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\details\details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_global__["a" /* global */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], DetailsPage);
    return DetailsPage;
}());

//# sourceMappingURL=details.js.map

/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ImageModalPage = (function () {
    function ImageModalPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ImageModalPage.prototype.ionViewDidLoad = function () {
        this.image = this.navParams.get('img');
    };
    ImageModalPage.prototype.close = function () {
        this.navCtrl.pop();
    };
    ImageModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-image-modal',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\image-modal\image-modal.html"*/'<ion-content>\n  <div class="content" (click)="close()" tappable>\n    <img src={{image}}/>\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\image-modal\image-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], ImageModalPage);
    return ImageModalPage;
}());

//# sourceMappingURL=image-modal.js.map

/***/ }),

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(488);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 488:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__(658);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__ = __webpack_require__(659);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_messages_messages__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_groups_groups__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_friends_friends__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_message_message__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_group_group__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_group_info_group_info__ = __webpack_require__(339);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_new_group_new_group__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_add_members_add_members__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_user_info_user_info__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_login__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_logout__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_alert__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_image__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_firebase__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_22_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_angularfire2__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_angularfire2_auth__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__settings__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pipes_friend__ = __webpack_require__(660);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pipes_search__ = __webpack_require__(661);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pipes_conversation__ = __webpack_require__(662);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pipes_date__ = __webpack_require__(663);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pipes_group__ = __webpack_require__(666);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_global__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_splash_screen__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_status_bar__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_google_plus__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_camera__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_keyboard__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_contacts__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ionic_native_media_capture__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ionic_native_file__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ionic_native_geolocation__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_firebase__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__ionic_native_facebook__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__angular_http__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_friends_filter_friends_filter__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_setting_setting__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_coupons_coupons__ = __webpack_require__(667);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_notifications_notifications__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_details_details__ = __webpack_require__(465);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_test_page_test__ = __webpack_require__(336);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















































__WEBPACK_IMPORTED_MODULE_22_firebase__["initializeApp"](__WEBPACK_IMPORTED_MODULE_26__settings__["a" /* Settings */].firebaseConfig);
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_51__pages_test_page_test__["a" /* TestPage */],
                __WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_coupons_coupons__["a" /* CouponsPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_notifications_notifications__["a" /* NotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_details_details__["a" /* DetailsPage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_groups_groups__["a" /* GroupsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_group_info_group_info__["a" /* GroupInfoPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_friends_friends__["a" /* FriendsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_message_message__["a" /* MessagePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_group_group__["a" /* GroupPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_new_group_new_group__["a" /* NewGroupPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_add_members_add_members__["a" /* AddMembersPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_user_info_user_info__["a" /* UserInfoPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_friends_filter_friends_filter__["a" /* FriendsFilterPage */],
                __WEBPACK_IMPORTED_MODULE_27__pipes_friend__["a" /* FriendPipe */],
                __WEBPACK_IMPORTED_MODULE_29__pipes_conversation__["a" /* ConversationPipe */],
                __WEBPACK_IMPORTED_MODULE_28__pipes_search__["a" /* SearchPipe */],
                __WEBPACK_IMPORTED_MODULE_30__pipes_date__["a" /* DateFormatPipe */],
                __WEBPACK_IMPORTED_MODULE_31__pipes_group__["a" /* GroupPipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */], {
                    scrollAssist: false,
                    autoFocusAssist: false,
                    mode: 'ios',
                    tabsPlacement: 'top'
                }, {
                    links: [
                        { loadChildren: '../pages/blockedlist/blockedlist.module#BlockedlistPageModule', name: 'BlockedlistPage', segment: 'blockedlist', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/details/details.module#DetailsPageModule', name: 'DetailsPage', segment: 'details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filtermodal/filtermodal.module#FiltermodalPageModule', name: 'FiltermodalPage', segment: 'filtermodal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/image-modal/image-modal.module#ImageModalPageModule', name: 'ImageModalPage', segment: 'image-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_44__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_23_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_26__settings__["a" /* Settings */].firebaseConfig, 'ionic3chat'),
                __WEBPACK_IMPORTED_MODULE_24_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_25_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_45__angular_http__["b" /* HttpModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_groups_groups__["a" /* GroupsPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_test_page_test__["a" /* TestPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_coupons_coupons__["a" /* CouponsPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_notifications_notifications__["a" /* NotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_details_details__["a" /* DetailsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_friends_friends__["a" /* FriendsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_message_message__["a" /* MessagePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_group_group__["a" /* GroupPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_group_info_group_info__["a" /* GroupInfoPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_new_group_new_group__["a" /* NewGroupPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_add_members_add_members__["a" /* AddMembersPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_user_info_user_info__["a" /* UserInfoPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_friends_filter_friends_filter__["a" /* FriendsFilterPage */]
            ],
            providers: [{ provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_33__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_35__ionic_native_google_plus__["a" /* GooglePlus */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_37__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_contacts__["a" /* Contacts */],
                __WEBPACK_IMPORTED_MODULE_39__ionic_native_media_capture__["a" /* MediaCapture */],
                __WEBPACK_IMPORTED_MODULE_40__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_41__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_42__ionic_native_firebase__["a" /* Firebase */],
                __WEBPACK_IMPORTED_MODULE_43__ionic_native_facebook__["a" /* Facebook */],
                __WEBPACK_IMPORTED_MODULE_15__providers_login__["a" /* LoginProvider */],
                __WEBPACK_IMPORTED_MODULE_16__providers_logout__["a" /* LogoutProvider */],
                __WEBPACK_IMPORTED_MODULE_17__providers_loading__["a" /* LoadingProvider */],
                __WEBPACK_IMPORTED_MODULE_18__providers_alert__["a" /* AlertProvider */],
                __WEBPACK_IMPORTED_MODULE_19__providers_image__["a" /* ImageProvider */],
                __WEBPACK_IMPORTED_MODULE_20__providers_data__["a" /* DataProvider */],
                __WEBPACK_IMPORTED_MODULE_32__providers_global__["a" /* global */],
                __WEBPACK_IMPORTED_MODULE_21__providers_firebase__["a" /* FirebaseProvider */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__alert__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_media_capture__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file__ = __webpack_require__(290);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ImageProvider = (function () {
    // All files to be uploaded on Firebase must have DATA_URL as the destination type.
    // This will return the imageURI which can then be processed and uploaded to Firebase.
    // For the list of cameraOptions, please refer to: https://github.com/apache/cordova-plugin-camera#module_camera.CameraOptions
    function ImageProvider(angularfire, alertProvider, loadingProvider, camera, mediaCapture, file) {
        this.angularfire = angularfire;
        this.alertProvider = alertProvider;
        this.loadingProvider = loadingProvider;
        this.camera = camera;
        this.mediaCapture = mediaCapture;
        this.file = file;
        // Image Provider
        // This is the provider class for most of the image processing including uploading images to Firebase.
        // Take note that the default function here uploads the file in .jpg. If you plan to use other encoding types, make sure to
        // set the encodingType before uploading the image on Firebase.
        // Example for .png:
        // data:image/jpeg;base64 -> data:image/png;base64
        // generateFilename to return .png
        this.profilePhotoOptions = {
            quality: 50,
            targetWidth: 384,
            targetHeight: 384,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        };
        this.photoMessageOptions = {
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true,
            allowEdit: true
        };
        this.groupPhotoOptions = {
            quality: 50,
            targetWidth: 384,
            targetHeight: 384,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        };
        console.log("Initializing Image Provider");
    }
    // Function to convert dataURI to Blob needed by Firebase
    ImageProvider.prototype.imgURItoBlob = function (dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };
    // Generate a random filename of length for the image to be uploaded
    ImageProvider.prototype.generateFilename = function () {
        var length = 8;
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text + ".jpg";
    };
    // Set ProfilePhoto given the user and the cameraSourceType.
    // This function processes the imageURI returned and uploads the file on Firebase,
    // Finally the user data on the database is updated.
    ImageProvider.prototype.setProfilePhoto = function (user, sourceType) {
        var _this = this;
        this.profilePhotoOptions.sourceType = sourceType;
        this.loadingProvider.show();
        // Get picture from camera or gallery.
        this.camera.getPicture(this.profilePhotoOptions).then(function (imageData) {
            // Process the returned imageURI.
            var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
            var metadata = {
                'contentType': imgBlob.type
            };
            // Generate filename and upload to Firebase Storage.
            __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref().child('images/' + user.userId + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                // Delete previous profile photo on Storage if it exists.
                _this.deleteImageFile(user.img);
                // URL of the uploaded image!
                var url = snapshot.metadata.downloadURLs[0];
                var profile = {
                    displayName: user.name,
                    photoURL: url
                };
                // Update Firebase User.
                __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().currentUser.updateProfile(profile)
                    .then(function (success) {
                    // Update User Data on Database.
                    _this.angularfire.object('/accounts/' + user.userId).update({
                        img: url
                    }).then(function (success) {
                        _this.loadingProvider.hide();
                        _this.alertProvider.showProfileUpdatedMessage();
                    }).catch(function (error) {
                        _this.loadingProvider.hide();
                        _this.alertProvider.showErrorMessage('profile/error-change-photo');
                    });
                })
                    .catch(function (error) {
                    _this.loadingProvider.hide();
                    _this.alertProvider.showErrorMessage('profile/error-change-photo');
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
                _this.alertProvider.showErrorMessage('image/error-image-upload');
            });
        }).catch(function (error) {
            _this.loadingProvider.hide();
        });
    };
    // Upload and set the group object's image.
    ImageProvider.prototype.setGroupPhoto = function (group, sourceType) {
        var _this = this;
        this.groupPhotoOptions.sourceType = sourceType;
        this.loadingProvider.show();
        // Get picture from camera or gallery.
        this.camera.getPicture(this.groupPhotoOptions).then(function (imageData) {
            // Process the returned imageURI.
            var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
            var metadata = {
                'contentType': imgBlob.type
            };
            __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref().child('images/' + __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().currentUser.uid + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                _this.deleteImageFile(group.img);
                // URL of the uploaded image!
                var url = snapshot.metadata.downloadURLs[0];
                group.img = url;
                _this.loadingProvider.hide();
            }).catch(function (error) {
                _this.loadingProvider.hide();
                _this.alertProvider.showErrorMessage('image/error-image-upload');
            });
        }).catch(function (error) {
            _this.loadingProvider.hide();
        });
    };
    // Set group photo and return the group object as promise.
    ImageProvider.prototype.setGroupPhotoPromise = function (group, sourceType) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.groupPhotoOptions.sourceType = sourceType;
            _this.loadingProvider.show();
            // Get picture from camera or gallery.
            _this.camera.getPicture(_this.groupPhotoOptions).then(function (imageData) {
                // Process the returned imageURI.
                var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
                var metadata = {
                    'contentType': imgBlob.type
                };
                __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref().child('images/' + __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().currentUser.uid + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                    _this.deleteImageFile(group.img);
                    // URL of the uploaded image!
                    var url = snapshot.metadata.downloadURLs[0];
                    group.img = url;
                    _this.loadingProvider.hide();
                    resolve(group);
                }).catch(function (error) {
                    _this.loadingProvider.hide();
                    _this.alertProvider.showErrorMessage('image/error-image-upload');
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    //Delete the image given the url.
    ImageProvider.prototype.deleteImageFile = function (path) {
        var fileName = path.substring(path.lastIndexOf('%2F') + 3, path.lastIndexOf('?'));
        __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref().child('images/' + __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().currentUser.uid + '/' + fileName).delete().then(function () { }).catch(function (error) { });
    };
    //Delete the user.img given the user.
    ImageProvider.prototype.deleteUserImageFile = function (user) {
        var fileName = user.img.substring(user.img.lastIndexOf('%2F') + 3, user.img.lastIndexOf('?'));
        __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref().child('images/' + user.userId + '/' + fileName).delete().then(function () { }).catch(function (error) { });
    };
    // Delete group image file on group storage reference.
    ImageProvider.prototype.deleteGroupImageFile = function (groupId, path) {
        var fileName = path.substring(path.lastIndexOf('%2F') + 3, path.lastIndexOf('?'));
        __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref().child('images/' + groupId + '/' + fileName).delete().then(function () { }).catch(function (error) { });
    };
    // Upload photo message and return the url as promise.
    ImageProvider.prototype.uploadPhotoMessage = function (conversationId, sourceType) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.photoMessageOptions.sourceType = sourceType;
            _this.loadingProvider.show();
            // Get picture from camera or gallery.
            _this.camera.getPicture(_this.photoMessageOptions).then(function (imageData) {
                // Process the returned imageURI.
                var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
                var metadata = {
                    'contentType': imgBlob.type
                };
                // Generate filename and upload to Firebase Storage.
                __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref().child('images/' + conversationId + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                    // URL of the uploaded image!
                    var url = snapshot.metadata.downloadURLs[0];
                    _this.loadingProvider.hide();
                    resolve(url);
                }).catch(function (error) {
                    _this.loadingProvider.hide();
                    _this.alertProvider.showErrorMessage('image/error-image-upload');
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    // Upload group photo message and return a promise as url.
    ImageProvider.prototype.uploadGroupPhotoMessage = function (groupId, sourceType) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.photoMessageOptions.sourceType = sourceType;
            _this.loadingProvider.show();
            // Get picture from camera or gallery.
            _this.camera.getPicture(_this.photoMessageOptions).then(function (imageData) {
                // Process the returned imageURI.
                var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
                var metadata = {
                    'contentType': imgBlob.type
                };
                // Generate filename and upload to Firebase Storage.
                __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref().child('images/' + groupId + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                    // URL of the uploaded image!
                    var url = snapshot.metadata.downloadURLs[0];
                    _this.loadingProvider.hide();
                    resolve(url);
                }).catch(function (error) {
                    _this.loadingProvider.hide();
                    _this.alertProvider.showErrorMessage('image/error-image-upload');
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    ImageProvider.prototype.uploadGroupVideoMessage = function (groupId) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.loadingProvider.show();
            _this.mediaCapture.captureVideo().then(function (data) {
                var videoUrl = data[0].fullPath;
                console.log("video path: " + videoUrl);
                var x = videoUrl.split("/");
                var filepath = videoUrl.substring(0, videoUrl.lastIndexOf("/"));
                var name = x[x.length - 1];
                console.log(filepath + " - " + name);
                _this.file.readAsArrayBuffer(filepath, name).then(function (success) {
                    console.log(success);
                    var blob = new Blob([success], { type: "video/mp4" });
                    console.log(blob);
                    var upload = __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref().child('videos/' + groupId + "/" + name).put(blob);
                    upload.then(function (res) {
                        var process = res.bytesTransferred / res.totalBytes * 100;
                        console.log(process);
                        _this.loadingProvider.hide();
                        resolve(res.downloadURL);
                    }, function (err) {
                        _this.loadingProvider.hide();
                        console.log("Failed");
                    });
                });
            }, function (err) {
                _this.loadingProvider.hide();
                console.log("Media Err = " + err);
            });
        });
    };
    ImageProvider.prototype.uploadVideoMessage = function (conversationId) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.loadingProvider.show();
            _this.mediaCapture.captureVideo().then(function (data) {
                var videoUrl = data[0].fullPath;
                console.log("video path: " + videoUrl);
                var x = videoUrl.split("/");
                var filepath = videoUrl.substring(0, videoUrl.lastIndexOf("/"));
                var name = x[x.length - 1];
                console.log(filepath + " - " + name);
                _this.file.readAsArrayBuffer(filepath, name).then(function (success) {
                    console.log(success);
                    var blob = new Blob([success], { type: "video/mp4" });
                    console.log(blob);
                    // let timestamp = (Math.floor(Date.now() / 1000)).toString();
                    var storageRef = __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref();
                    var upload = storageRef.child('videos/' + name).put(blob);
                    upload.then(function (res) {
                        var process = res.bytesTransferred / res.totalBytes * 100;
                        console.log(process);
                        _this.loadingProvider.hide();
                        resolve(res.downloadURL);
                    }, function (err) {
                        _this.loadingProvider.hide();
                        console.log("Failed");
                    });
                });
            }, function (err) {
                _this.loadingProvider.hide();
                console.log("Media Err = " + err);
            });
        });
    };
    ImageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1__alert__["a" /* AlertProvider */], __WEBPACK_IMPORTED_MODULE_2__loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_media_capture__["a" /* MediaCapture */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_file__["a" /* File */]])
    ], ImageProvider);
    return ImageProvider;
}());

//# sourceMappingURL=image.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_image__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__image_modal_image_modal__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_contacts__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_keyboard__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_geolocation__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__user_info_user_info__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var MessagePage = (function () {
    // MessagePage
    // This is the page where the user can chat with a friend.
    function MessagePage(navCtrl, navParams, dataProvider, angularfire, loadingProvider, alertCtrl, imageProvider, modalCtrl, camera, keyboard, actionSheet, contacts, geolocation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataProvider = dataProvider;
        this.angularfire = angularfire;
        this.loadingProvider = loadingProvider;
        this.alertCtrl = alertCtrl;
        this.imageProvider = imageProvider;
        this.modalCtrl = modalCtrl;
        this.camera = camera;
        this.keyboard = keyboard;
        this.actionSheet = actionSheet;
        this.contacts = contacts;
        this.geolocation = geolocation;
        this.startIndex = -1;
        // Set number of messages to show.
        this.numberOfMessages = 10;
    }
    MessagePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.userId = this.navParams.get('userId');
        console.log("userId: " + this.userId);
        this.loggedInUserId = __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid;
        // Get friend details.
        this.dataProvider.getUser(this.userId).snapshotChanges().subscribe(function (user) {
            _this.title = user.payload.val().name;
        });
        // Get conversationInfo with friend.
        this.angularfire.object('/accounts/' + this.loggedInUserId + '/conversations/' + this.userId).snapshotChanges().subscribe(function (conversation) {
            if (conversation.payload.exists()) {
                // User already have conversation with this friend, get conversation
                _this.conversationId = conversation.payload.val().conversationId;
                // Get conversation
                _this.dataProvider.getConversationMessages(_this.conversationId).snapshotChanges().subscribe(function (messagesRes) {
                    var messages = messagesRes.payload.val();
                    console.log(messages);
                    if (messages == null)
                        messages = [];
                    if (_this.messages) {
                        // Just append newly added messages to the bottom of the view.
                        if (messages.length > _this.messages.length) {
                            var message_1 = messages[messages.length - 1];
                            _this.dataProvider.getUser(message_1.sender).snapshotChanges().subscribe(function (user) {
                                message_1.avatar = user.payload.val().img;
                            });
                            _this.messages.push(message_1);
                            _this.messagesToShow.push(message_1);
                        }
                    }
                    else {
                        // Get all messages, this will be used as reference object for messagesToShow.
                        _this.messages = [];
                        messages.forEach(function (message) {
                            _this.dataProvider.getUser(message.sender).snapshotChanges().subscribe(function (user) {
                                message.avatar = user.payload.val().img;
                            });
                            _this.messages.push(message);
                        });
                        // Load messages in relation to numOfMessages.
                        if (_this.startIndex == -1) {
                            // Get initial index for numberOfMessages to show.
                            if ((_this.messages.length - _this.numberOfMessages) > 0) {
                                _this.startIndex = _this.messages.length - _this.numberOfMessages;
                            }
                            else {
                                _this.startIndex = 0;
                            }
                        }
                        if (!_this.messagesToShow) {
                            _this.messagesToShow = [];
                        }
                        // Set messagesToShow
                        for (var i = _this.startIndex; i < _this.messages.length; i++) {
                            _this.messagesToShow.push(_this.messages[i]);
                        }
                        // this.loadingProvider.hide();
                    }
                });
            }
        });
        // Update messages' date time elapsed every minute based on Moment.js.
        var that = this;
        if (!that.updateDateTime) {
            that.updateDateTime = setInterval(function () {
                if (that.messages) {
                    that.messages.forEach(function (message) {
                        var date = message.date;
                        message.date = new Date(date);
                    });
                }
            }, 60000);
        }
    };
    MessagePage.prototype.ionViewDidEnter = function () {
        this.scrollBottom();
    };
    // Load previous messages in relation to numberOfMessages.
    MessagePage.prototype.loadPreviousMessages = function () {
        var that = this;
        // Show loading.
        // this.loadingProvider.show();
        setTimeout(function () {
            // Set startIndex to load more messages.
            if ((that.startIndex - that.numberOfMessages) > -1) {
                that.startIndex -= that.numberOfMessages;
            }
            else {
                that.startIndex = 0;
            }
            // Refresh our messages list.
            that.messages = null;
            that.messagesToShow = null;
            that.scrollTop();
            // Populate list again.
            that.ionViewDidLoad();
        }, 1000);
    };
    // Update messagesRead when user lefts this page.
    MessagePage.prototype.ionViewWillLeave = function () {
        this.setMessagesRead();
    };
    // Check if currentPage is active, then update user's messagesRead.
    MessagePage.prototype.setMessagesRead = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_6_firebase__["database"]().ref('/conversations/' + this.conversationId + '/messages').once('value', function (snap) {
            console.log(snap.val());
            if (snap.val() != null) {
                _this.angularfire.object('/accounts/' + _this.loggedInUserId + '/conversations/' + _this.userId).update({
                    messagesRead: snap.val().length
                });
            }
        });
    };
    // Scroll to bottom of page after a short delay.
    MessagePage.prototype.scrollBottom = function () {
        var that = this;
        setTimeout(function () {
            that.content.scrollToBottom();
        }, 300);
        this.setMessagesRead();
    };
    // Scroll to top of the page after a short delay.
    MessagePage.prototype.scrollTop = function () {
        var that = this;
        setTimeout(function () {
            that.content.scrollToTop();
        }, 300);
    };
    // Check if the user is the sender of the message.
    MessagePage.prototype.isSender = function (message) {
        if (message.sender == this.loggedInUserId) {
            return true;
        }
        else {
            return false;
        }
    };
    // Send message, if there's no conversation yet, create a new conversation.
    MessagePage.prototype.send = function (type) {
        var _this = this;
        if (this.message) {
            // User entered a text on messagebox
            if (this.conversationId) {
                var messages_1 = JSON.parse(JSON.stringify(this.messages));
                messages_1.push({
                    date: new Date().toString(),
                    sender: this.loggedInUserId,
                    type: type,
                    message: this.message
                });
                // Update conversation on database.
                this.dataProvider.getConversation(this.conversationId).update({
                    messages: messages_1
                });
                // Clear messagebox.
                this.message = '';
                this.scrollBottom();
            }
            else {
                console.log("else");
                // New Conversation with friend.
                var messages = [];
                messages.push({
                    date: new Date().toString(),
                    sender: this.loggedInUserId,
                    type: type,
                    message: this.message
                });
                var users = [];
                users.push(this.loggedInUserId);
                users.push(this.userId);
                // Add conversation.
                this.angularfire.list('conversations').push({
                    dateCreated: new Date().toString(),
                    messages: messages,
                    users: users
                }).then(function (success) {
                    var conversationId = success.key;
                    _this.message = '';
                    // Add conversation reference to the users.
                    _this.angularfire.object('/accounts/' + _this.loggedInUserId + '/conversations/' + _this.userId).update({
                        conversationId: conversationId,
                        messagesRead: 1
                    });
                    _this.angularfire.object('/accounts/' + _this.userId + '/conversations/' + _this.loggedInUserId).update({
                        conversationId: conversationId,
                        messagesRead: 0
                    });
                });
                this.scrollBottom();
            }
        }
    };
    MessagePage.prototype.viewUser = function (userId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__user_info_user_info__["a" /* UserInfoPage */], { userId: userId });
    };
    MessagePage.prototype.attach = function () {
        var _this = this;
        var action = this.actionSheet.create({
            title: 'Choose attachments',
            buttons: [{
                    text: 'Camera',
                    handler: function () {
                        _this.imageProvider.uploadPhotoMessage(_this.conversationId, _this.camera.PictureSourceType.CAMERA).then(function (url) {
                            _this.message = url;
                            _this.send("image");
                        });
                    }
                }, {
                    text: 'Photo Library',
                    handler: function () {
                        _this.imageProvider.uploadPhotoMessage(_this.conversationId, _this.camera.PictureSourceType.PHOTOLIBRARY).then(function (url) {
                            _this.message = url;
                            _this.send("image");
                        });
                    }
                },
                {
                    text: 'Video',
                    handler: function () {
                        _this.imageProvider.uploadVideoMessage(_this.conversationId).then(function (url) {
                            _this.message = url;
                            _this.send("video");
                        });
                    }
                },
                {
                    text: 'Location',
                    handler: function () {
                        _this.geolocation.getCurrentPosition({
                            timeout: 5000
                        }).then(function (res) {
                            var locationMessage = "Location:<br> lat:" + res.coords.latitude + "<br> lng:" + res.coords.longitude;
                            var mapUrl = "<a href='https://www.google.com/maps/search/" + res.coords.latitude + "," + res.coords.longitude + "'>View on Map</a>";
                            var confirm = _this.alertCtrl.create({
                                title: 'Your Location',
                                message: locationMessage,
                                buttons: [{
                                        text: 'cancel',
                                        handler: function () {
                                            console.log("canceled");
                                        }
                                    }, {
                                        text: 'Share',
                                        handler: function () {
                                            _this.message = locationMessage + "<br>" + mapUrl;
                                            _this.send("location");
                                        }
                                    }]
                            });
                            confirm.present();
                        }, function (locationErr) {
                            console.log("Location Error" + JSON.stringify(locationErr));
                        });
                    }
                }, {
                    text: 'Contact',
                    handler: function () {
                        _this.contacts.pickContact().then(function (data) {
                            var name;
                            if (data.displayName !== null)
                                name = data.displayName;
                            else
                                name = data.name.givenName + " " + data.name.familyName;
                            _this.message = "<b>Name:</b> " + name + "<br><b>Mobile:</b> <a href='tel:" + data.phoneNumbers[0].value + "'>" + data.phoneNumbers[0].value + "</a>";
                            _this.send("contact");
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }, {
                    text: 'cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log("cancelled");
                    }
                }]
        });
        action.present();
    };
    // Enlarge image messages.
    MessagePage.prototype.enlargeImage = function (img) {
        var imageModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__image_modal_image_modal__["a" /* ImageModalPage */], { img: img });
        imageModal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], MessagePage.prototype, "content", void 0);
    MessagePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-message',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\message\message.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title (click)="viewUser(userId)">{{title}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content has-footer >\n  <!-- Messages -->\n  <div class="messages">\n    <p class="center" *ngIf="startIndex > 0"><span tappable (click)="loadPreviousMessages()">Load previous messages</span></p>\n    \n    <div *ngFor="let message of messagesToShow">\n      <div class="sender" *ngIf="isSender(message)" class="chatbox right">\n        <div *ngIf="message.type == \'text\'">\n          <p>{{message.message}}</p>\n        </div>\n        <div *ngIf="message.type == \'location\'" [innerHtml]="message.message"></div>\n        <div *ngIf="message.type == \'contact\'" [innerHtml]="message.message"></div>\n        <div *ngIf="message.type == \'image\'">\n          <img tappable (click)="enlargeImage(message.message)" src="{{message.message}}" />\n        </div>\n        <div *ngIf="message.type == \'video\'">\n          <video controls width="100%" >\n             <source src="{{message.message}}" type="video/mp4">\n          </video>\n        </div>\n        <span>{{message.date | DateFormat}}</span>\n      </div>\n      <div *ngIf="!isSender(message)" class="chatbox left">\n        <div *ngIf="message.type == \'text\'">\n          <p>{{message.message}}</p>\n        </div>\n        <div *ngIf="message.type == \'location\'" [innerHtml]="message.message"></div>\n        <div *ngIf="message.type == \'contact\'" [innerHtml]="message.message"></div>\n        <div  *ngIf="message.type == \'image\'">\n          <img tappable (click)="enlargeImage(message.message)" src="{{message.message}}" />\n        </div>\n        <div *ngIf="message.type == \'video\'">\n          <video controls width="100%">\n             <source src="{{message.message}}" type="video/mp4">\n          </video>\n        </div>\n        <span>{{message.date | DateFormat}}</span>\n      </div>\n    </div>\n    \n  </div>\n</ion-content>\n<!-- Message Box -->\n<ion-footer>\n  <ion-item class="bottom_bar">\n    <button item-left ion-button clear (click)="attach()"><ion-icon name="md-attach"></ion-icon></button>\n    <ion-textarea type="text" rows="0" placeholder="הקלד את הודעתך " [(ngModel)]="message"></ion-textarea>\n    <button item-right ion-button clear (click)="send(\'text\')" [disabled]="!message"><ion-icon name="md-send"></ion-icon></button>\n    <!-- </ion-buttons> -->\n  </ion-item>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\message\message.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_data__["a" /* DataProvider */], __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__providers_image__["a" /* ImageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_keyboard__["a" /* Keyboard */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_contacts__["a" /* Contacts */], __WEBPACK_IMPORTED_MODULE_11__ionic_native_geolocation__["a" /* Geolocation */]])
    ], MessagePage);
    return MessagePage;
}());

//# sourceMappingURL=message.js.map

/***/ }),

/***/ 658:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_notifications_notifications__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_setting_setting__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_friends_friends__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_test_page_test__ = __webpack_require__(336);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_10__pages_test_page_test__["a" /* TestPage */];
        this.activeitem = 0;
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
            platform.pause.subscribe(function () {
                if (__WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().currentUser)
                    __WEBPACK_IMPORTED_MODULE_4_firebase__["database"]().ref('accounts/' + __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().currentUser.uid).update({ 'online': false });
            });
            platform.resume.subscribe(function () {
                if (__WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().currentUser && localStorage.getItem('showOnline') == 'true')
                    __WEBPACK_IMPORTED_MODULE_4_firebase__["database"]().ref('accounts/' + __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().currentUser.uid).update({ 'online': true });
            });
        });
        this.pages = [{
                title: 'בית',
                icon: "home",
                component: __WEBPACK_IMPORTED_MODULE_10__pages_test_page_test__["a" /* TestPage */]
            },
            {
                title: 'הודעות',
                icon: "notifications",
                component: __WEBPACK_IMPORTED_MODULE_6__pages_notifications_notifications__["a" /* NotificationsPage */]
            },
            {
                title: 'פרופיל',
                icon: "person",
                component: __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]
            },
            {
                title: 'הדילים שלי',
                icon: "heart",
                component: __WEBPACK_IMPORTED_MODULE_9__pages_friends_friends__["a" /* FriendsPage */]
            },
            {
                title: 'הגדרות',
                icon: "settings",
                component: __WEBPACK_IMPORTED_MODULE_8__pages_setting_setting__["a" /* SettingPage */]
            },
            {
                title: 'התחבר',
                icon: "log-in",
                component: __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]
            }
        ];
    }
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        console.log(page);
        console.log(page.component);
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.activeItem = function (index) {
        this.activeitem = index;
    };
    MyApp.prototype.goTo = function (page) {
        var _this = this;
        this.nav.setRoot('LoginPage');
        setTimeout(function () {
            _this.activeitem = 0;
        }, 1000);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\app\app.html"*/'<ion-menu side="right" [content]="content">\n    <ion-header text-center padding-vertical>\n        <img src="assets/img/profile.png" margin-top>\n        <h3>Ori</h3>\n        <p>Arbeson@gmail.com</p>\n    </ion-header>\n\n    <ion-content padding>\n        <ion-list>\n            <ion-item menuClose *ngFor="let p of pages; let i = index" [ngClass]="{\'active_item\':activeitem==i}"\n                (click)="openPage(p);activeItem(i)" no-lines no-padding>\n                <ion-icon name="{{p.icon}}" item-start></ion-icon>\n                {{p.title}}\n            </ion-item>\n        </ion-list>\n    </ion-content>\n    <ion-footer>\n        <ion-item (click)="goTo()" menuClose no-lines>\n            <ion-icon name="log-out" item-start></ion-icon>\n            התנתק\n        </ion-item>\n    </ion-footer>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav class="md" [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 659:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__messages_messages__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__groups_groups__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__friends_friends__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_firebase__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TabsPage = (function () {
    // TabsPage
    // This is the page where we set our tabs.
    function TabsPage(navCtrl, navParams, dataProvider, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataProvider = dataProvider;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.messages = __WEBPACK_IMPORTED_MODULE_3__messages_messages__["a" /* MessagesPage */];
        this.groups = __WEBPACK_IMPORTED_MODULE_4__groups_groups__["a" /* GroupsPage */];
        this.friends = __WEBPACK_IMPORTED_MODULE_5__friends_friends__["a" /* FriendsPage */];
        this.profile = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.friendRequestCount = 0;
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // Get friend requests count.
        this.dataProvider.getRequests(__WEBPACK_IMPORTED_MODULE_7_firebase__["auth"]().currentUser.uid).snapshotChanges().subscribe(function (requestsRes) {
            var requests = requestsRes.payload.val();
            if (requests != null) {
                if (requests.friendRequests != null)
                    _this.friendRequestCount = requests.friendRequests.length;
                else
                    _this.friendRequestCount = null;
            }
            else
                _this.friendRequestCount = null;
        });
        // Get conversations and add/update if the conversation exists, otherwise delete from list.
        this.dataProvider.getConversations().snapshotChanges().subscribe(function (conversationsInfoRes) {
            var conversationsInfo = [];
            conversationsInfo = conversationsInfoRes.map(function (c) { return (__assign({ $key: c.key }, c.payload.val())); });
            _this.conversationsInfo = null;
            _this.conversationList = null;
            if (conversationsInfo.length > 0) {
                _this.conversationsInfo = conversationsInfo;
                conversationsInfo.forEach(function (conversationInfo) {
                    if (conversationInfo.blocked != true) {
                        _this.dataProvider.getConversation(conversationInfo.conversationId).snapshotChanges().subscribe(function (conversationRes) {
                            if (conversationRes.payload.exists()) {
                                var conversation = __assign({ $key: conversationRes.key }, conversationRes.payload.val());
                                if (conversation.blocked != true)
                                    _this.addOrUpdateConversation(conversation);
                            }
                        });
                    }
                });
            }
        });
        this.dataProvider.getGroups().snapshotChanges().subscribe(function (groupIdsRes) {
            var groupIds = [];
            groupIds = groupIdsRes.map(function (c) { return (__assign({ $key: c.key }, c.payload.val())); });
            if (groupIds.length > 0) {
                _this.groupsInfo = groupIds;
                if (_this.groupList && _this.groupList.length > groupIds.length) {
                    // User left/deleted a group, clear the list and add or update each group again.
                    _this.groupList = null;
                }
                groupIds.forEach(function (groupId) {
                    _this.dataProvider.getGroup(groupId.$key).snapshotChanges().subscribe(function (groupRes) {
                        var group = __assign({ $key: groupRes.key }, groupRes.payload.val());
                        if (group.$key != null) {
                            _this.addOrUpdateGroup(group);
                        }
                    });
                });
            }
            else {
                _this.unreadGroupMessagesCount = null;
                _this.groupsInfo = null;
                _this.groupList = null;
            }
        });
    };
    // Add or update conversaion for real-time sync of unreadMessagesCount.
    TabsPage.prototype.addOrUpdateConversation = function (conversation) {
        if (!this.conversationList) {
            this.conversationList = [conversation];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.conversationList.length; i++) {
                if (this.conversationList[i].$key == conversation.$key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.conversationList[index] = conversation;
            }
            else {
                this.conversationList.push(conversation);
            }
        }
        this.computeUnreadMessagesCount();
    };
    // Compute all conversation's unreadMessages.
    TabsPage.prototype.computeUnreadMessagesCount = function () {
        this.unreadMessagesCount = 0;
        if (this.conversationList) {
            for (var i = 0; i < this.conversationList.length; i++) {
                this.unreadMessagesCount += this.conversationList[i].messages.length - this.conversationsInfo[i].messagesRead;
                if (this.unreadMessagesCount == 0) {
                    this.unreadMessagesCount = null;
                }
            }
        }
    };
    TabsPage.prototype.getUnreadMessagesCount = function () {
        if (this.unreadMessagesCount) {
            if (this.unreadMessagesCount > 0) {
                return this.unreadMessagesCount;
            }
        }
        return null;
    };
    // Add or update group
    TabsPage.prototype.addOrUpdateGroup = function (group) {
        if (!this.groupList) {
            this.groupList = [group];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.groupList.length; i++) {
                if (this.groupList[i].$key == group.$key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.groupList[index] = group;
            }
            else {
                this.groupList.push(group);
            }
        }
        this.computeUnreadGroupMessagesCount();
    };
    // Remove group from list if group is already deleted.
    TabsPage.prototype.removeGroup = function (groupId) {
        if (this.groupList) {
            var index = -1;
            for (var i = 0; i < this.groupList.length; i++) {
                if (this.groupList[i].$key == groupId) {
                    index = i;
                }
            }
            if (index > -1) {
                this.groupList.splice(index, 1);
            }
            index = -1;
            for (var j = 0; j < this.groupsInfo.length; j++) {
                if (this.groupsInfo[i].$key == groupId) {
                    index = j;
                }
            }
            if (index > -1) {
                this.groupsInfo.splice(index, 1);
            }
            this.computeUnreadGroupMessagesCount();
        }
    };
    // Compute all group's unreadMessages.
    TabsPage.prototype.computeUnreadGroupMessagesCount = function () {
        this.unreadGroupMessagesCount = 0;
        if (this.groupList) {
            for (var i = 0; i < this.groupList.length; i++) {
                if (this.groupList[i].messages) {
                    this.unreadGroupMessagesCount += this.groupList[i].messages.length - this.groupsInfo[i].messagesRead;
                }
                if (this.unreadGroupMessagesCount == 0) {
                    this.unreadGroupMessagesCount = null;
                }
            }
        }
    };
    TabsPage.prototype.getUnreadGroupMessagesCount = function () {
        if (this.unreadGroupMessagesCount) {
            if (this.unreadGroupMessagesCount > 0) {
                return this.unreadGroupMessagesCount;
            }
        }
        return null;
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\tabs\tabs.html"*/'<ion-tabs selectedIndex="0">\n  <ion-tab [root]="messages" tabIcon="chatbubbles" tabBadgeStyle="danger" tabBadge="{{getUnreadMessagesCount()}}"></ion-tab>\n  <ion-tab [root]="groups" tabIcon="people" tabBadgeStyle="danger" tabBadge="{{getUnreadGroupMessagesCount()}}"></ion-tab>\n  <ion-tab [root]="friends" tabIcon="list" tabBadgeStyle="danger" tabBadge="{{friendRequestCount}}"></ion-tab>\n  <ion-tab [root]="profile" tabIcon="contact" ></ion-tab>\n</ion-tabs>'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_6__providers_data__["a" /* DataProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 660:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FriendPipe = (function () {
    function FriendPipe() {
    }
    // FriendPipe
    // Filter friend by name or username.
    FriendPipe.prototype.transform = function (friends, search) {
        if (!friends) {
            return;
        }
        else if (!search) {
            return friends;
        }
        else {
            var term_1 = search.toLowerCase();
            return friends.filter(function (friend) { return friend.name.toLowerCase().indexOf(term_1) > -1 || friend.username.toLowerCase().indexOf(term_1) > -1; });
        }
    };
    FriendPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'friendFilter'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], FriendPipe);
    return FriendPipe;
}());

//# sourceMappingURL=friend.js.map

/***/ }),

/***/ 661:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SearchPipe = (function () {
    function SearchPipe() {
    }
    // SearchPipe
    // Filter user search results for name or username excluding the excludedIds.
    SearchPipe.prototype.transform = function (accounts, data) {
        var excludedIds = data[0];
        var term = data[1];
        if (!accounts) {
            return;
        }
        else if (!excludedIds) {
            return accounts;
        }
        else if (excludedIds && !term) {
            return accounts.filter(function (account) { return excludedIds.indexOf(account.userId) == -1; });
        }
        else {
            term = term.toLowerCase();
            return accounts.filter(function (account) { return excludedIds.indexOf(account.userId) == -1 && (account.name.toLowerCase().indexOf(term) > -1 || account.username.toLowerCase().indexOf(term) > -1); });
        }
    };
    SearchPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'searchFilter'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], SearchPipe);
    return SearchPipe;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 662:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConversationPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ConversationPipe = (function () {
    function ConversationPipe() {
    }
    // ConversationPipe
    // Filter conversation based on friend's name or username.
    ConversationPipe.prototype.transform = function (conversations, search) {
        if (!conversations) {
            return;
        }
        else if (!search) {
            return conversations;
        }
        else {
            var term_1 = search.toLowerCase();
            return conversations.filter(function (conversation) { return conversation.friend.name.toLowerCase().indexOf(term_1) > -1 || conversation.friend.username.toLowerCase().indexOf(term_1) > -1; });
        }
    };
    ConversationPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'conversationFilter'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], ConversationPipe);
    return ConversationPipe;
}());

//# sourceMappingURL=conversation.js.map

/***/ }),

/***/ 663:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateFormatPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DateFormatPipe = (function () {
    function DateFormatPipe() {
    }
    DateFormatPipe.prototype.transform = function (date, args) {
        return __WEBPACK_IMPORTED_MODULE_1_moment__(new Date(date)).fromNow();
    };
    DateFormatPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'DateFormat'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], DateFormatPipe);
    return DateFormatPipe;
}());

//# sourceMappingURL=date.js.map

/***/ }),

/***/ 665:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 342,
	"./af.js": 342,
	"./ar": 343,
	"./ar-dz": 344,
	"./ar-dz.js": 344,
	"./ar-kw": 345,
	"./ar-kw.js": 345,
	"./ar-ly": 346,
	"./ar-ly.js": 346,
	"./ar-ma": 347,
	"./ar-ma.js": 347,
	"./ar-sa": 348,
	"./ar-sa.js": 348,
	"./ar-tn": 349,
	"./ar-tn.js": 349,
	"./ar.js": 343,
	"./az": 350,
	"./az.js": 350,
	"./be": 351,
	"./be.js": 351,
	"./bg": 352,
	"./bg.js": 352,
	"./bm": 353,
	"./bm.js": 353,
	"./bn": 354,
	"./bn.js": 354,
	"./bo": 355,
	"./bo.js": 355,
	"./br": 356,
	"./br.js": 356,
	"./bs": 357,
	"./bs.js": 357,
	"./ca": 358,
	"./ca.js": 358,
	"./cs": 359,
	"./cs.js": 359,
	"./cv": 360,
	"./cv.js": 360,
	"./cy": 361,
	"./cy.js": 361,
	"./da": 362,
	"./da.js": 362,
	"./de": 363,
	"./de-at": 364,
	"./de-at.js": 364,
	"./de-ch": 365,
	"./de-ch.js": 365,
	"./de.js": 363,
	"./dv": 366,
	"./dv.js": 366,
	"./el": 367,
	"./el.js": 367,
	"./en-au": 368,
	"./en-au.js": 368,
	"./en-ca": 369,
	"./en-ca.js": 369,
	"./en-gb": 370,
	"./en-gb.js": 370,
	"./en-ie": 371,
	"./en-ie.js": 371,
	"./en-il": 372,
	"./en-il.js": 372,
	"./en-nz": 373,
	"./en-nz.js": 373,
	"./eo": 374,
	"./eo.js": 374,
	"./es": 375,
	"./es-do": 376,
	"./es-do.js": 376,
	"./es-us": 377,
	"./es-us.js": 377,
	"./es.js": 375,
	"./et": 378,
	"./et.js": 378,
	"./eu": 379,
	"./eu.js": 379,
	"./fa": 380,
	"./fa.js": 380,
	"./fi": 381,
	"./fi.js": 381,
	"./fo": 382,
	"./fo.js": 382,
	"./fr": 383,
	"./fr-ca": 384,
	"./fr-ca.js": 384,
	"./fr-ch": 385,
	"./fr-ch.js": 385,
	"./fr.js": 383,
	"./fy": 386,
	"./fy.js": 386,
	"./gd": 387,
	"./gd.js": 387,
	"./gl": 388,
	"./gl.js": 388,
	"./gom-latn": 389,
	"./gom-latn.js": 389,
	"./gu": 390,
	"./gu.js": 390,
	"./he": 391,
	"./he.js": 391,
	"./hi": 392,
	"./hi.js": 392,
	"./hr": 393,
	"./hr.js": 393,
	"./hu": 394,
	"./hu.js": 394,
	"./hy-am": 395,
	"./hy-am.js": 395,
	"./id": 396,
	"./id.js": 396,
	"./is": 397,
	"./is.js": 397,
	"./it": 398,
	"./it.js": 398,
	"./ja": 399,
	"./ja.js": 399,
	"./jv": 400,
	"./jv.js": 400,
	"./ka": 401,
	"./ka.js": 401,
	"./kk": 402,
	"./kk.js": 402,
	"./km": 403,
	"./km.js": 403,
	"./kn": 404,
	"./kn.js": 404,
	"./ko": 405,
	"./ko.js": 405,
	"./ky": 406,
	"./ky.js": 406,
	"./lb": 407,
	"./lb.js": 407,
	"./lo": 408,
	"./lo.js": 408,
	"./lt": 409,
	"./lt.js": 409,
	"./lv": 410,
	"./lv.js": 410,
	"./me": 411,
	"./me.js": 411,
	"./mi": 412,
	"./mi.js": 412,
	"./mk": 413,
	"./mk.js": 413,
	"./ml": 414,
	"./ml.js": 414,
	"./mn": 415,
	"./mn.js": 415,
	"./mr": 416,
	"./mr.js": 416,
	"./ms": 417,
	"./ms-my": 418,
	"./ms-my.js": 418,
	"./ms.js": 417,
	"./mt": 419,
	"./mt.js": 419,
	"./my": 420,
	"./my.js": 420,
	"./nb": 421,
	"./nb.js": 421,
	"./ne": 422,
	"./ne.js": 422,
	"./nl": 423,
	"./nl-be": 424,
	"./nl-be.js": 424,
	"./nl.js": 423,
	"./nn": 425,
	"./nn.js": 425,
	"./pa-in": 426,
	"./pa-in.js": 426,
	"./pl": 427,
	"./pl.js": 427,
	"./pt": 428,
	"./pt-br": 429,
	"./pt-br.js": 429,
	"./pt.js": 428,
	"./ro": 430,
	"./ro.js": 430,
	"./ru": 431,
	"./ru.js": 431,
	"./sd": 432,
	"./sd.js": 432,
	"./se": 433,
	"./se.js": 433,
	"./si": 434,
	"./si.js": 434,
	"./sk": 435,
	"./sk.js": 435,
	"./sl": 436,
	"./sl.js": 436,
	"./sq": 437,
	"./sq.js": 437,
	"./sr": 438,
	"./sr-cyrl": 439,
	"./sr-cyrl.js": 439,
	"./sr.js": 438,
	"./ss": 440,
	"./ss.js": 440,
	"./sv": 441,
	"./sv.js": 441,
	"./sw": 442,
	"./sw.js": 442,
	"./ta": 443,
	"./ta.js": 443,
	"./te": 444,
	"./te.js": 444,
	"./tet": 445,
	"./tet.js": 445,
	"./tg": 446,
	"./tg.js": 446,
	"./th": 447,
	"./th.js": 447,
	"./tl-ph": 448,
	"./tl-ph.js": 448,
	"./tlh": 449,
	"./tlh.js": 449,
	"./tr": 450,
	"./tr.js": 450,
	"./tzl": 451,
	"./tzl.js": 451,
	"./tzm": 452,
	"./tzm-latn": 453,
	"./tzm-latn.js": 453,
	"./tzm.js": 452,
	"./ug-cn": 454,
	"./ug-cn.js": 454,
	"./uk": 455,
	"./uk.js": 455,
	"./ur": 456,
	"./ur.js": 456,
	"./uz": 457,
	"./uz-latn": 458,
	"./uz-latn.js": 458,
	"./uz.js": 457,
	"./vi": 459,
	"./vi.js": 459,
	"./x-pseudo": 460,
	"./x-pseudo.js": 460,
	"./yo": 461,
	"./yo.js": 461,
	"./zh-cn": 462,
	"./zh-cn.js": 462,
	"./zh-hk": 463,
	"./zh-hk.js": 463,
	"./zh-tw": 464,
	"./zh-tw.js": 464
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 665;

/***/ }),

/***/ 666:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var GroupPipe = (function () {
    function GroupPipe() {
    }
    // GroupPipe
    // Filter group by name
    GroupPipe.prototype.transform = function (groups, search) {
        if (!groups) {
            return;
        }
        else if (!search) {
            return groups;
        }
        else {
            var term_1 = search.toLowerCase();
            return groups.filter(function (group) { return group.name.toLowerCase().indexOf(term_1) > -1; });
        }
    };
    GroupPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'groupFilter'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], GroupPipe);
    return GroupPipe;
}());

//# sourceMappingURL=group.js.map

/***/ }),

/***/ 667:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CouponsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CouponsPage = (function () {
    function CouponsPage(navCtrl, navParams, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.global = global;
        this.coupons = [
            {
                img: "assets/img/01.png",
                title: "SAVE $0.55",
                type: "offer"
            },
            {
                img: "assets/img/02.png",
                title: "In store or online: $10 off $25 or more",
                type: "coupon"
            },
            {
                img: "assets/img/03.png",
                title: "SAVE $100.50",
                type: "offer"
            }, {
                img: "assets/img/01.png",
                title: "SAVE $0.55",
                type: "offer"
            }
        ];
    }
    CouponsPage.prototype.deleteCoupon = function (item) {
        this.coupons.splice(this.coupons.indexOf(item), 1);
    };
    CouponsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-coupons',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\coupons\coupons.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>הדילים שלי</ion-title>\n    <ion-buttons end>\n        <button ion-button icon-only (click)="global.presentfilterModal()">\n            <ion-icon name="funnel"></ion-icon>\n        </button>\n        <button ion-button icon-only (click)="searchInput=!searchInput">\n          <ion-icon name="search"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n  <!-- <ion-toolbar *ngIf="searchInput"> -->\n  <ion-toolbar>\n      <ion-searchbar placeholder=\'\' no-margin></ion-searchbar>\n  </ion-toolbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-list class="main_list" no-margin>\n      <ion-item *ngFor="let item of coupons" [navPush]="\'DetailsPage\'" no-lines>\n        <ion-thumbnail item-start>\n          <img src="{{item.img}}">\n        </ion-thumbnail>\n\n        <div *ngIf="item.type==\'offer\'">\n          <h2 class="main_title" text-uppercase>{{item.title}}</h2>\n          <h2 text-uppercase>ESThe &reg;</h2>\n          <p class="short_note">on any one (1) ESThe Iced Tea 18.5oz, 23oz or multipack</p>\n          <p><span>00.10$</span>free shipping at amazon</p>\n        </div>\n\n        <div *ngIf="item.type==\'coupon\'">\n          <h2 class="short_note">{{item.title}}</h2>\n          <p>From jcpenney</p>\n          <p class="date" text-uppercase>Ends 10.09.2017</p>\n          <button ion-button color="primary" (click)="global.getCode($event)" text-capitalize no-margin>\n            <span *ngIf="!global.codenumber">Coupon Code</span>\n            <span *ngIf="global.codenumber">0486Fd25</span>\n          </button>\n        </div>\n        \n        <button class="float_btn" color="primary" (click)="deleteCoupon(item)" ion-button icon-only no-margin>\n            <ion-icon name="remove"></ion-icon>\n        </button>\n      </ion-item>\n      <ion-item class="no_results" *ngIf="coupons?.length == 0" text-center>No coupons found</ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\coupons\coupons.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_global__["a" /* global */]])
    ], CouponsPage);
    return CouponsPage;
}());

//# sourceMappingURL=coupons.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return global; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var global = (function () {
    function global(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.items = [{
                logo: "assets/img/pinat_aohel_logo.png",
                img: "assets/img/salad.png",
                title: "פינת האוכל",
                subtitle: " 50 % הנחה על כל הסלטים  ",
                content: " המבצע אינו תקף על סלטי דגים",
                categorty: ["50%", "Salad"],
                location: "Tel Aviv",
                duration: "18:30",
                quantity: "10",
                userId: "0naZnm1Tj8Qh9T7GOJfpbli74sS2",
                type: "offer",
            },
            {
                logo: "assets/img/wafelbar.jpg",
                img: "assets/img/vafel_bar_vafel_belgi_free.png",
                title: "וואפל בר",
                subtitle: "1+1 על כל הקינוחים",
                content: "הקניה ממבחר הקינוחים של וואפל בר",
                categorty: ["1+1"],
                location: "Tel Aviv",
                duration: "12:30",
                quantity: "5",
                userId: "TlWMO3Zip2e6yIt2PILdv1ePq0E3",
                type: "coupon"
            },
            {
                logo: "assets/img/stekialogo.png",
                img: "assets/img/stkiya_deal.png",
                title: "הסטקיה",
                subtitle: "מנה חינם לבחירה",
                content: "בקנית עיסקית",
                categorty: ["free dose"],
                location: "Jerusalem",
                duration: "13:00",
                quantity: "8",
                userId: "cuNoIza53EZYkE0wKrfRwVzncwa2",
                type: "offer"
            }
        ];
        this.filter =
            [{
                    category: "לפי  מבצע",
                    filter: [{
                            title: "1+1 מבצע",
                            check: false
                        },
                        {
                            title: "50% מבצע",
                            check: false
                        },
                        {
                            title: "קינוח חינם",
                            check: false
                        },
                        {
                            title: "מנה חינם לבחירה",
                            check: false
                        }
                    ]
                },
                {
                    category: "סוג עסק",
                    filter: [{
                            title: "מסעדה בשרית",
                            check: false
                        },
                        {
                            title: "מסעדה חלבית",
                            check: false
                        },
                        {
                            title: "מזון מהיר",
                            check: false
                        },
                        {
                            title: "פיצוציה",
                            check: false
                        },
                        {
                            title: "גלידריה",
                            check: false
                        }
                    ]
                }
            ];
        this.filterresults = [];
    }
    //filter modal function
    global.prototype.filterData = function (item) {
        if (this.filterresults.indexOf(item) == -1)
            this.filterresults.push(item);
        else
            this.filterresults.splice(this.filterresults.indexOf(item), 1);
    };
    global.prototype.deleteFilter = function (item) {
        this.filterresults.splice(this.filterresults.indexOf(item), 1);
        item.check = false;
    };
    //present filter modal
    global.prototype.presentfilterModal = function () {
        var modal = this.modalCtrl.create('FiltermodalPage', {}, {
            cssClass: 'filter-modal'
        });
        modal.present();
    };
    //get coupon code
    global.prototype.getCode = function ($event) {
        $event.stopPropagation();
        this.codenumber = !this.codenumber;
    };
    global = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], global);
    return global;
}());

;
//# sourceMappingURL=global.js.map

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Validator; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(22);
// Validators
// This file contains all your validators for the formGroups and for inputPrompts.
// Patterns can be tested by using a RegEx validator such as http://www.regexpal.com, https://regex101.com, among others.

var Validator;
(function (Validator) {
    // Set your validators here, don't forget to import and use them in the appropriate class that uses formGroups.
    // In this example, they are used on LoginPage where a formGroup for email and passwords is used.
    Validator.emailValidator = ['', [
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].minLength(5),
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required,
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')
        ]
    ];
    Validator.passwordValidator = ['', [
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].minLength(5),
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required,
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].pattern('^[a-zA-Z0-9!@#$%^&*()_+-=]*$')
        ]
    ];
    Validator.fullnameValidator = ['', [
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].minLength(1),
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required
        ]
    ];
    Validator.usernameValidator = ['', [
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].minLength(5),
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required,
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].pattern('^[a-zA-Z0-9!@#$%^&*()_+-=]*$')
        ]
    ];
    // Set your prompt input validators here, don't forget to import and use them on the AlertController prompt.
    // In this example they are used by home.ts where the user are allowed to change their profile.
    // errorMessages are used by the AlertProvider class and is imported inside AlertProvider.errorMessages which is used by showErrorMessage().
    Validator.profileNameValidator = {
        minLength: 5,
        lengthError: { title: 'Name Too Short!', subTitle: 'Sorry, but name must be more than 4 characters.' },
        pattern: /^[a-zA-Z0-9\s]*$/g,
        patternError: { title: 'Invalid Name!', subTitle: 'Sorry, but the name you entered contains special characters.' }
    };
    Validator.profileuserNameValidator = {
        minLength: 5,
        lengthError: { title: 'Userame Too Short!', subTitle: 'Sorry, but name must be more than 4 characters.' },
        pattern: /^[a-zA-Z0-9\s]*$/g,
        patternError: { title: 'Invalid Username!', subTitle: 'Sorry, but the name you entered contains special characters.' }
    };
    Validator.profileEmailValidator = {
        pattern: /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/g,
        patternError: { title: 'Invalid Email Address!', subTitle: 'Sorry, but the email you have entered is invalid.' }
    };
    Validator.profilePasswordValidator = {
        minLength: 5,
        lengthError: { title: 'Password Too Short!', subTitle: 'Sorry, but password must be more than 4 characters.' },
        pattern: /^[a-zA-Z0-9!@#$%^&*()_+-=]*$/g,
        patternError: { title: 'Invalid Password!', subTitle: 'Sorry, but the password you have entered contains special characters.' }
    };
    // Group Form Validators
    Validator.groupNameValidator = ['', [__WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].minLength(1)]];
    Validator.groupDescriptionValidator = ['', [__WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].minLength(1)]];
})(Validator || (Validator = {}));
//# sourceMappingURL=validator.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Settings; });
var Settings;
(function (Settings) {
    Settings.firebaseConfig = {
        // apiKey: "AIzaSyDN6WmBnJGfN64BnR-r4TW9V8N1IHgps1w",
        // authDomain: "chatapp-3f829.firebaseapp.com",
        // databaseURL: "https://chatapp-3f829.firebaseio.com",
        // projectId: "chatapp-3f829",
        // storageBucket: "chatapp-3f829.appspot.com",
        // messagingSenderId: "845839389008"
        apiKey: "AIzaSyARbZXkhgZraYqcEQRlNUwa9CKLUYwq0-U",
        authDomain: "instantdealchat.firebaseapp.com",
        databaseURL: "https://instantdealchat.firebaseio.com",
        projectId: "instantdealchat",
        storageBucket: "instantdealchat.appspot.com",
        messagingSenderId: "906116738801"
    };
    Settings.facebookLoginEnabled = true;
    Settings.googleLoginEnabled = true;
    Settings.phoneLoginEnabled = true;
    Settings.facebookAppId = "1989655487796025";
    Settings.googleClientId = "906116738801-crsdbbjlbfogtt37fuie17v8g48lrft9.apps.googleusercontent.com‏";
    Settings.customTokenUrl = "https://us-central1-chatapp-3f829.cloudfunctions.net/getCustomToken";
    // export const homePage = TabsPage;
})(Settings || (Settings = {}));
//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_firebase__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__message_message__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UserInfoPage = (function () {
    // UserInfoPage
    // This is the page where the user can view user information, and do appropriate actions based on their relation to the current logged in user.
    function UserInfoPage(navCtrl, navParams, modalCtrl, dataProvider, loadingProvider, alertCtrl, firebaseProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.dataProvider = dataProvider;
        this.loadingProvider = loadingProvider;
        this.alertCtrl = alertCtrl;
        this.firebaseProvider = firebaseProvider;
    }
    UserInfoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.userId = this.navParams.get('userId');
        console.log(this.userId);
        this.loadingProvider.show();
        // Get user info.
        this.dataProvider.getUser(this.userId).snapshotChanges().subscribe(function (user) {
            _this.user = __assign({ $key: user.key }, user.payload.val());
            console.log(_this.user);
            _this.loadingProvider.hide();
        });
        // Get friends of current logged in user.
        this.dataProvider.getUser(__WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid).snapshotChanges().subscribe(function (user) {
            if (user.payload.val() != null)
                _this.friends = user.payload.val().friends;
        });
        // Get requests of current logged in user.
        this.dataProvider.getRequests(__WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid).snapshotChanges().subscribe(function (requests) {
            console.log(requests.payload.val());
            if (requests.payload.val() != null) {
                _this.friendRequests = requests.payload.val().friendRequests;
                _this.requestsSent = requests.payload.val().requestsSent;
            }
        });
    };
    UserInfoPage.prototype.block = function () {
        console.log("block function");
        __WEBPACK_IMPORTED_MODULE_6_firebase__["database"]().ref('accounts/' + __WEBPACK_IMPORTED_MODULE_6_firebase__["auth"]().currentUser.uid + '/conversations/' + this.userId).update({
            blocked: true
        });
    };
    // Enlarge user's profile image.
    UserInfoPage.prototype.enlargeImage = function (img) {
        var imageModal = this.modalCtrl.create("ImageModalPage", { img: img });
        imageModal.present();
    };
    // Accept friend request.
    UserInfoPage.prototype.acceptFriendRequest = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Confirm Friend Request',
            message: 'Do you want to accept <b>' + this.user.name + '</b> as your friend?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Accept',
                    handler: function () {
                        _this.firebaseProvider.acceptFriendRequest(_this.userId);
                    }
                }
            ]
        }).present();
    };
    // Deny friend request.
    UserInfoPage.prototype.rejectFriendRequest = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Reject Friend Request',
            message: 'Do you want to reject <b>' + this.user.name + '</b> as your friend?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Reject',
                    handler: function () {
                        _this.firebaseProvider.deleteFriendRequest(_this.userId);
                    }
                }
            ]
        }).present();
    };
    // Cancel friend request sent.
    UserInfoPage.prototype.cancelFriendRequest = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Friend Request Pending',
            message: 'Do you want to delete your friend request to <b>' + this.user.name + '</b>?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Delete',
                    handler: function () {
                        _this.firebaseProvider.cancelFriendRequest(_this.userId);
                    }
                }
            ]
        }).present();
    };
    // Send friend request.
    UserInfoPage.prototype.sendFriendRequest = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Send Friend Request',
            message: 'Do you want to send friend request to <b>' + this.user.name + '</b>?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Send',
                    handler: function () {
                        _this.firebaseProvider.sendFriendRequest(_this.userId);
                    }
                }
            ]
        }).present();
    };
    // Open chat with this user.
    UserInfoPage.prototype.sendMessage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__message_message__["a" /* MessagePage */], { userId: this.userId });
    };
    // Check if user can be added, meaning user is not yet friends nor has sent/received any friend requests.
    UserInfoPage.prototype.canAdd = function () {
        if (this.friendRequests) {
            if (this.friendRequests.indexOf(this.userId) > -1) {
                return false;
            }
        }
        if (this.requestsSent) {
            if (this.requestsSent.indexOf(this.userId) > -1) {
                return false;
            }
        }
        if (this.friends) {
            if (this.friends.indexOf(this.userId) > -1) {
                return false;
            }
        }
        return true;
    };
    UserInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-user-info',template:/*ion-inline-start:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\user-info\user-info.html"*/'<ion-header>\n  <ion-navbar color="white">\n    <ion-title *ngIf="user"></ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <!-- User Info -->\n  <div *ngIf="user">\n    <ion-row style="background:#f3f3f3" padding>\n      <ion-col col-8>\n        <h4 style="margin:0">{{user.name}}</h4>\n        <p style="color:#aaa;margin:0">@{{user.username}}</p>\n        <p style="color:#444">{{user.description}}</p>\n        <div style="border-top: 1px solid #ececec; padding: 10px;">\n          <!-- Show actions based on the status of the user in relation to the current logged in user. -->\n          <div *ngIf="friendRequests && friendRequests.indexOf(user.$key) > -1">\n            <p class="info">Sent you a friend request.</p>\n            <button ion-button small color="cancelrequest" tappable (click)="rejectFriendRequest()">\n              בטל\n            </button>\n            <button ion-button small color="acceptrequest" tappable (click)="acceptFriendRequest()">\n              אשר\n            </button>\n          </div>\n          <div *ngIf="requestsSent && requestsSent.indexOf(user.$key) > -1">\n            <button ion-button small color="cancelrequest" tappable (click)="cancelFriendRequest()">בטל הזמנה</button>\n          </div>\n          <div *ngIf="canAdd()">\n            <button ion-button small color="sendrequest" tappable (click)="sendFriendRequest()">הוסף חבר</button>\n          </div>\n          <div *ngIf="friends && friends.indexOf(user.$key) > -1">\n            <button ion-button small color="sendrequest" tappable (click)="sendMessage()">התחל צ\'אט</button>\n            <button ion-button small color="cancelrequest" tappable (click)="block()">חסום משתמש</button>\n          </div>\n        </div>\n      </ion-col>\n      <ion-col col-4 class="center">\n        <img src="{{user.img}}" tappable (click)="enlargeImage(user.img)" onError="this.src=\'./assets/images/default-dp.png\'" style="border-radius: 100%">\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\mylaptop30\Desktop\ArbesTech-Projects\Andrian\InstantDealNew\src\pages\user-info\user-info.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2__providers_data__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__providers_firebase__["a" /* FirebaseProvider */]])
    ], UserInfoPage);
    return UserInfoPage;
}());

//# sourceMappingURL=user-info.js.map

/***/ })

},[467]);
//# sourceMappingURL=main.js.map