import { MessagePage } from './../pages/message/message';
import { MessagesPage } from './../pages/messages/messages';

import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';
import { LoginPage } from '../pages/login/login';
import { NotificationsPage } from '../pages/notifications/notifications';
import { ProfilePage } from '../pages/profile/profile';
import { HomePage } from '../pages/home/home';
import { SettingPage } from '../pages/setting/setting';
import { UserInfoPage } from '../pages/user-info/user-info';
import { FriendsPage } from '../pages/friends/friends';
import { TestPage } from '../pages/test-page/test';


@Component({
  templateUrl: 'app.html'
})



export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage = TestPage;
  pages: Array<any>;
  activeitem = 0;



  
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {

 


    platform.ready().then(() => {

      statusBar.styleDefault();
      splashScreen.hide();
      platform.pause.subscribe(()=>{
        if(firebase.auth().currentUser)
          firebase.database().ref('accounts/'+firebase.auth().currentUser.uid).update({'online': false});
      });
      platform.resume.subscribe(()=>{
        if(firebase.auth().currentUser && localStorage.getItem('showOnline') == 'true')
          firebase.database().ref('accounts/'+firebase.auth().currentUser.uid).update({'online': true});
      })
    });


    this.pages = [{
      title: 'בית',
      icon: "home",
      component: TestPage
    },
    {
      title: 'הודעות',
      icon: "notifications",
      component: NotificationsPage
    },
    {
      title: 'פרופיל',
      icon: "person",
      component: HomePage
    },
    {
      title: 'הדילים שלי',
      icon: "heart",
      component: FriendsPage
    },
    {
      title: 'הגדרות',
      icon: "settings",
      component: SettingPage
    }
      ,
    {
      title: 'התחבר',
      icon: "log-in",
      component: LoginPage
    }
    ];

    
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    console.log(page);
    console.log(page.component);
    

    this.nav.setRoot(page.component);
  }
  activeItem(index) {
    this.activeitem = index;
  }
  goTo(page) {
    this.nav.setRoot('LoginPage');
    setTimeout(() => {
      this.activeitem = 0;
    }, 1000)
  }
}
