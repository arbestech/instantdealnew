import {
  Injectable
} from '@angular/core'
import {
  ModalController
} from 'ionic-angular';

@Injectable()
export class global {
  filter: Array < any > ;
  filterresults: Array < any > ;
  codenumber;

  public constructor(public modalCtrl: ModalController) {
    this.filter =
     [{
        category: "לפי  מבצע",
        filter: [{
            title: "1+1 מבצע",
            check: false
          },
          {
            title: "50% מבצע",
            check: false
          },
          {
            title: "קינוח חינם",
            check: false
          },
          {
            title: "מנה חינם לבחירה",
            check: false
          }
        ]
      },
      {
        category: "סוג עסק",
        filter: [{
            title: "מסעדה בשרית",
            check: false
          },
          {
            title: "מסעדה חלבית",
            check: false
          },
          {
            title: "מזון מהיר",
            check: false
          },
          {
            title: "פיצוציה",
            check: false
          },
          {
            title: "גלידריה",
            check: false
          }
        ]
      }
    ];
    this.filterresults = [];
  }
  //filter modal function
  filterData(item) {
    if (this.filterresults.indexOf(item) == -1) this.filterresults.push(item);
    else this.filterresults.splice(this.filterresults.indexOf(item), 1)
  }
  deleteFilter(item) {
    this.filterresults.splice(this.filterresults.indexOf(item), 1);
    item.check = false;
  }

  //present filter modal
  presentfilterModal() {
    let modal = this.modalCtrl.create('FiltermodalPage', {}, {
      cssClass: 'filter-modal'
    });
    modal.present();
  }

  //get coupon code
  getCode($event) {
    $event.stopPropagation();
    this.codenumber = !this.codenumber;
  }

  items = [{
      logo: "assets/img/pinat_aohel_logo.png",
      img: "assets/img/salad.png",
      title: "פינת האוכל",
      subtitle: " 50 % הנחה על כל הסלטים  ",
      content: " המבצע אינו תקף על סלטי דגים",
      categorty: ["50%","Salad"],
      location: "Tel Aviv",
      duration: "18:30",
      quantity: "10",
      userId:"0naZnm1Tj8Qh9T7GOJfpbli74sS2",
      type: "offer",
     

    },
    {
      logo: "assets/img/wafelbar.jpg",

      img: "assets/img/vafel_bar_vafel_belgi_free.png",
      title: "וואפל בר",
      subtitle: "1+1 על כל הקינוחים",
      content: "הקניה ממבחר הקינוחים של וואפל בר",
      categorty: ["1+1"],
      location: "Tel Aviv",
      duration: "12:30",
      quantity: "5",
      userId:"TlWMO3Zip2e6yIt2PILdv1ePq0E3",
      type: "coupon"
    },
    {
      logo: "assets/img/stekialogo.png",
      img: "assets/img/stkiya_deal.png",
      title: "הסטקיה",
      subtitle: "מנה חינם לבחירה",
      content: "בקנית עיסקית",
      categorty: ["free dose"],
      location: "Jerusalem",
      duration: "13:00",
      quantity: "8",
      userId:"cuNoIza53EZYkE0wKrfRwVzncwa2",

      type: "offer"
    }
  ]
};
