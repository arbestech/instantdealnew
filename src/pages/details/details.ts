import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { global } from '../../providers/global'; 



@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})   
export class DetailsPage {
comments:Array<any>;  

  constructor(public navCtrl: NavController, public navParams: NavParams, public global:global, public socialSharing: SocialSharing) {

    this.comments=[{img:"assets/img/person.png",name:"WendyVerdades",date:"3 days ago"},
    {img:"assets/img/person2.png",name:"john wick",date:"1 hour"}]

  }
  shareSheetShare() {
    this.socialSharing.share("Share message", "Share subject", "URL to file or image", "A URL to share").then(() => {
      console.log("shareSheetShare: Success");
    }).catch(() => {
      console.error("shareSheetShare: failed");
    });
  }

}
