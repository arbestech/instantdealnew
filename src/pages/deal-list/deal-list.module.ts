import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DealsListPage } from './deal-list';



@NgModule({
  declarations: [  
    DealsListPage,
  ],
  imports: [
    IonicPageModule.forChild(DealsListPage),
  ],
})
export class HomePageModule {}
