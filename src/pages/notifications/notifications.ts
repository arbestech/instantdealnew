import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
notifications:Array<any>; 

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.notifications=[{img:"assets/img/01.png",title:"SAVE $0.55",date:"1h"},
    {img:"assets/img/02.png",title:"SAVE $0.10",date:"3h"},
    {img:"assets/img/03.png",title:"SAVE $0.55",date:"1d"}]

  }

}
