import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { global } from '../../providers/global'; 

@IonicPage()
@Component({
  selector: 'page-filtermodal',
  templateUrl: 'filtermodal.html',
})
export class FiltermodalPage {
  filter:Array<any>;
  filterresults:Array<any>;

  constructor(public navCtrl: NavController,
  public navParams: NavParams,
  public viewCtrl: ViewController,
  public global:global) {
  }

  dismiss() {    
    this.viewCtrl.dismiss();
  }   
  
}  
 