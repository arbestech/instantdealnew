import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { global } from '../../providers/global'; 


@Component({
  selector: 'page-coupons',
  templateUrl: 'coupons.html',
})
export class CouponsPage {
coupons:Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public global:global) {
    this.coupons=[
      {
      img:"assets/img/01.png",
      title:"SAVE $0.55",
      type:"offer"
    },
    {
      img:"assets/img/02.png",
      title:"In store or online: $10 off $25 or more",
      type:"coupon"
    },
    {
      img:"assets/img/03.png",
      title:"SAVE $100.50",
      type:"offer"
    },{
      img:"assets/img/01.png",
      title:"SAVE $0.55",
      type:"offer"}]
  }

  deleteCoupon(item){
     this.coupons.splice(this.coupons.indexOf(item), 1);
  } 

}
